<?php

namespace common\view;

class PageView {

	/**
	 * @param Page $page   
	 * @return String Final HTML
	 */
	public function getHTMLPage(Page $page){

		return "<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv='content-type' content='text/html; charset=utf-8' >
		<title>$page->title</title> 
		<link href='css/bootstrap-3.0.0/dist/css/bootstrap.min.css' rel='stylesheet' media='screen'>

		<link href='css/bootstrap-3.0.0/dist/css/starter-template.css' rel='stylesheet'>
		<link href='css/default.css' type='text/css' rel='stylesheet'  >
	</head>
	<body>
		<div class='container'>
			<div class='starter-template'>
        		$page->body
        	</div>
      	</div>

	</body>
</html>

    <script src='css/bootstrap-3.0.0/assets/js/jquery.js'></script>
    <script src='css/bootstrap-3.0.0/dist/js/bootstrap.min.js'></script>
	<script src='http://code.jquery.com/jquery-1.10.1.min.js'></script>
	<script src='http://code.jquery.com/jquery-migrate-1.2.1.min.js'></script>
	<script type='text/javascript' src='javascript/vendor/parsley.min.js'></script>";
	}

}
