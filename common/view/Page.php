<?php

namespace common\view;

class Page {

	/**
	 * @var String
	 */ 
	public $title = "";

	/**
	 * @var String
	 */ 	
	public $body = "";

	/**
	 * @param  String $title 
	 * @param  String $body
	 */ 
	public function __construct($title, $body) {
		$this->title = $title;
		$this->body = $body;
	}
}
