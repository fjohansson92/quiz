<?php

namespace common\model;

class LengthException extends \Exception {

	/** 
	 * @var integer
	 */
	protected $length;

	/** 
	 * @param integer $length 
	 */
	public function __construct($length) {

		$this->length = $length;
	}

	/** 
	 * @return integer
	 */
	public function getLength() {
		return $this->length;
	}

}