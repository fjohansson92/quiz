<?php

namespace common\model;

class Database {

    /** 
     * @var \mysqli
     */
	private $mysqli;

    /** 
     * @param mysqli $mysqli 
     */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;
	}

    /** 
     * Select result as matrix.
     * @param  string $sql 
     * @param  array $values 
     * @return array Result from database.        
     */
    public function select($sql, $values) {

        $stmt = $this->prepare($sql, $values);
        $result = $stmt->get_result();
        return $result;
    }

    /** 
     * Insert in database
     * @param  string $sql 
     * @param  array $values 
     * @return integer Insert id
     */
    public function insert($sql, $values) {

        $stmt = $this->prepare($sql, $values);
        return $this->mysqli->insert_id;
    }    

    /** 
     * Bind params and execute.
     * @param  string $sql
     * @param  array $values 
     * @return Statment object
     * @throws If prepare or execute failed.       
     */
    private function prepare($sql, $values) {

        $stmt = $this->mysqli->prepare($sql);
        if ($stmt === FALSE) {
            throw new \Exception();
        }

        if (!empty($values)) {
            $types = "";
            foreach($values as $value) {
                $types .= "s";
            }   
            $values = array_merge(array($types),$values);
            
            $refArray = array();
            foreach($values as $key => $value) {
                $refArray[$key] = &$values[$key];
            }    
            call_user_func_array(array($stmt, 'bind_param'), $refArray);
        }

        if ($stmt->execute() === FALSE) {
            throw new \Exception();
        }
        return $stmt;
    }

    /** 
     * Insert in two tables 
     * @param  string $firstSql 
     * @param  string $secondSql  
     * @param  array $firstValues 
     * @param  array $secondValues 
     * @throws If one insert failed.
     */
    public function insertTwoTables($firstSql, $secondSql, $firstValues, $secondValues) {

        mysqli_autocommit($this->mysqli, FALSE);
        $succesful = true;
        try{

            $this->insert($firstSql, $firstValues);
            $this->insert($secondSql, $secondValues);

        } catch(\Exception $exception) {
            $succesful = false;
        }

        if($succesful) {

            $this->mysqli->commit();
        } else {
            $this->mysqli->rollback();
            throw new \Exception();
        }

    }
}