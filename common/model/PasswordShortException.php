<?php

namespace common\model;

class PasswordShortException extends LengthException {}