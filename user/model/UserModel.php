<?php

namespace user\model;

class UserModel {

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @param \user\model\UserCredentials $userCredentials 
	 */
	public function __construct(\user\model\UserCredentials $userCredentials) {

		$this->userCredentials = $userCredentials;
	}

	/** 
	 * @return boolean 
	 */
	public function isAdmin() {

		return $this->userCredentials->getUserType() == \user\model\UserType::ADMIN;
	}

	/** 
	 * @return boolean 
	 */
	public function isTeacher() {

		return $this->userCredentials->getUserType() == \user\model\UserType::TEACHER;
	}
}