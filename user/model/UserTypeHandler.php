<?php

namespace user\model;

class UserTypeHandler {

	/** 
	 * @var \user\model\Users
	 */
	private $users;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {

		$this->users = new \user\model\Users($mysqli);
	}

	/** 
	 * @return array of \user\model\UserCredentials
	 */
	public function getTeachers() {

		return $this->users->getEveryUserFromType(\user\model\UserType::TEACHER);		
	}

	/** 
	 * @return array of \user\model\UserCredentials
	 */
	public function getStudents() {

		return $this->users->getEveryUserFromType(\user\model\UserType::STUDENT);		
	}

	/** 
	 * @param  integer $userId
	 */
	public function changeType($userId) {

		$user = $this->users->getFullName($userId);
		$newUserType;
		if ($user->getUserType() == \user\model\UserType::STUDENT) {

			$newUserType = \user\model\UserType::TEACHER;
		} else {

			$newUserType = \user\model\UserType::STUDENT;
		}

		$this->users->updateUserType($user, $newUserType);
	}
}