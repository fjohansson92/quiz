<?php

namespace user\model;

final class UserType {

	const STUDENT = 0;
	const TEACHER = 1;
	const ADMIN = 2;
}