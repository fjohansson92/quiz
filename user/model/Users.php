<?php

namespace user\model;

class Users {
	
	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {
		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  \login\model\ClientInfo $clientInfo 
	 * @return \user\model\UserCredentials                           
	 */
	public function getUserCredentials(\login\model\ClientInfo $clientInfo) {

		$loginCredentials = $clientInfo->getLoginCredentials();

		$sql = "SELECT pk, userName, fName, lName, userType FROM userdata WHERE userName = ?";
		$values = array($loginCredentials->getUserName());

		return $this->sortUserCredentials($sql, $values);
	}

	/** 
	 * @param  integer $userType 
	 * @return array of \user\model\UserCredentials          
	 */
	public function getEveryUserFromType($userType) {

		$sql = "SELECT pk, userName, fName, lName, userType FROM userdata WHERE userType = ?";
		$values = array($userType);

		return $this->sortMultipleCredentials($sql, $values);
	}

	/** 
	 * @return array of \user\model\UserCredentials  
	 */
	public function getAllUsers() {

		$sql = "SELECT pk, userName, fName, lName, userType FROM userdata";
		$values = array();

		return $this->sortMultipleCredentials($sql, $values);
	}

	/** 
	 * @param  integer $pk 
	 * @return \user\model\UserCredentials       
	 */
	public function getFullName($pk) {

		$sql = "SELECT pk, userName, fName, lName, userType FROM userdata WHERE pk = ?";
		$values = array($pk);

		return $this->sortUserCredentials($sql, $values);
	}

	/** 
	 * @param  string $sql    
	 * @param  array $values 
	 * @return \user\model\UserCredentials         
	 * @throws If user wasn't found
	 */
	private function sortUserCredentials($sql, $values) {

		$result = $this->database->select($sql, $values);

		if ($row = $result->fetch_assoc()) {  

			return new \user\model\UserCredentials($row["pk"], $row["userName"], $row["fName"], $row["lName"], $row["userType"]);           
		}	
		throw new \Exception();
	}

	/** 
	 * @param  string $sql    
	 * @param  array $values 
	 * @return array of \user\model\UserCredentials          
	 */
	private function sortMultipleCredentials($sql, $values) {

		$result = $this->database->select($sql, $values);

		$users = array();

		while ($row = $result->fetch_assoc()) {   

			$users[] = new \user\model\UserCredentials($row["pk"], $row["userName"], $row["fName"], $row["lName"], $row["userType"]);

		}	
		return $users;
	}

	/** 
	 * @param  \user\model\UserCredentials $userCredentials 
	 * @param  integer $userType  
	 */
	public function updateUserType(\user\model\UserCredentials $userCredentials, $userType) {

		$sql = "UPDATE userdata SET userType = ? WHERE pk = ?";
		$values = array($userType, $userCredentials->getPk());
		$this->database->insert($sql, $values);
	}

}

