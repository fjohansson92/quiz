<?php

namespace user\model;

require_once("./user/model/UserType.php");

class UserCredentials {

	/** 
	 * @var integer
	 */
	private static $fNameMaxLength = 30;

	/** 
	 * @var integer
	 */
	private static $lNameMaxLength = 30;

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var string
	 */
	private $userName;

	/** 
	 * @var string
	 */
	private $fName;

	/** 
	 * @var string
	 */	
	private $lName;

	/** 
	 * @var integer
	 */	
	private $userType;

	/** 
	 * @param integer $pk       
	 * @param string  $userName 
	 * @param string  $fName    
	 * @param string  $lName    
	 * @param integer $userType 
	 * @throws If invalid input for new user
	 */
	public function __construct($pk = 0, $userName, $fName, $lName, $userType) {
		
		$this->pk = $pk;
		$this->userName = $userName;

		if(empty($fName)) {
			throw new \common\model\FNameEmptyException();
		} else if (strlen($fName) > self::$fNameMaxLength) {
			throw new \common\model\FNameLengthException(self::$fNameMaxLength);
		} else if (empty($lName)) {
			throw new \common\model\LNameEmptyException();
		} else if (strlen($lName) > self::$lNameMaxLength) {
			throw new \common\model\LNameLengthException(self::$lNameMaxLength);
		}

		assert(is_numeric($userType));

		$this->fName = $fName;
		$this->lName = $lName;
		$this->userType = $userType;
	}

	/**
	 * @param  integer $pk       
	 * @param  string $userName 
	 * @param  string $fName    
	 * @param  string $lName    
	 * @return \user\model\UserCredentials          
	 */
	public static function createStudent($pk = NULL, $userName, $fName, $lName) {

		return new \user\model\UserCredentials($pk, $userName, $fName, $lName, \user\model\UserType::STUDENT);
	}

	/** 
	 * @return integer
	 */
	public function getPk() {
		return $this->pk;
	}

	/** 
	 * @return string
	 */
	public function getUserName() {

		return $this->userName;
	}

	/** 
	 * @return string
	 */
	public function getFName() {

		return $this->fName;
	}

	/** 
	 * @return string
	 */
	public function getLName() {

		return $this->lName;
	}

	/** 
	 * @return integer
	 */
	public function getUserType() {

		return $this->userType;
	}


}