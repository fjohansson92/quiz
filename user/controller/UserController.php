<?php

namespace user\controller;

require_once("./user/model/UserModel.php");
require_once("./user/model/Users.php");
require_once("./user/controller/StudentController.php");
require_once("./user/controller/TeacherController.php");
require_once("./user/controller/AdminController.php");
require_once("./quiz/view/Navigation.php");

class UserController {

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @var \user\model\UserModel
	 */
	private $userModel;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @param \mysqli $mysqli     
	 * @param \login\model\ClientInfo $clientInfo 
	 */
	public function __construct(\mysqli $mysqli, \login\model\ClientInfo $clientInfo) {

		$this->mysqli = $mysqli;

		$users = new \user\model\Users($this->mysqli);
		$this->userCredentials = $users->getUserCredentials($clientInfo);
		$this->userModel = new \user\model\UserModel($this->userCredentials);
	}

	/** 
	 * @return \common\view\Page
	 */
	public function startUserApplication() {

		if($this->userModel->isAdmin()) {

			$adminController = new \user\controller\AdminController($this->mysqli, $this->userCredentials);
			return $adminController->startAdminApplication();
		} else if($this->userModel->isTeacher()) {

			$teacherController = new \user\controller\TeacherController($this->mysqli, $this->userCredentials);
			return $teacherController->startTeacherApplication();
		} else {

			$studentController = new \user\controller\StudentController($this->mysqli, $this->userCredentials);
			return $studentController->startStudentApplication();
		}
	}
}