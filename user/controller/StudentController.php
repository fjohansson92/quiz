<?php

namespace user\controller;

require_once("./user/view/StudentView.php");
require_once("./user/view/StudentNavigation.php");
require_once("./quiz/controller/GroupController.php");
require_once("./quiz/controller/OwnResultController.php");

class StudentController {

	/** 
	 * @var \user\view\StudentView
	 */
	private $studentView;

	/** 
	 * @var \user\view\StudentNavigation
	 */
	private $navigation;

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var \quiz\controller\GroupController
	 */
	private $groupController;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @param mysqli  $mysqli 
	 * @param \user\model\UserCredentials $userCredentials
	 */
	public function __construct(\mysqli $mysqli, \user\model\UserCredentials $userCredentials) {

		$this->mysqli = $mysqli;
		$this->userCredentials = $userCredentials;
		$this->navigation = new \user\view\StudentNavigation();
		$this->studentView = new \user\view\StudentView($this->navigation);
		$this->quizNavigation = new \quiz\view\Navigation();
		$this->groupController = new \quiz\controller\GroupController($this->mysqli, 
																	  $this->navigation, 
																	  $this->quizNavigation, 
																	  $this->userCredentials);
	}

	/** 
	 * @return string HTML
	 */
	public function startStudentApplication() {

		if ($this->navigation->userChecksGroup() || $this->quizNavigation->studentChecksGroups()) {
			$content = $this->groupController->doStudentGroup();
			return $this->studentView->getGroupPage($content);
		} else if ($this->navigation->userChecksQuizzes() || $this->quizNavigation->studentChecksQuizzes()) {

			$studentQuizController = new \quiz\controller\QuizController($this->mysqli, $this->quizNavigation, $this->userCredentials);
			$content = $studentQuizController->doStudentQuiz();
			return $this->studentView->getQuizPage($content);
		} else if ($this->navigation->userChecksResults() ||$this->quizNavigation->quizDone()) {

			$ownResultController = new \quiz\controller\OwnResultController($this->mysqli, $this->quizNavigation, $this->userCredentials->getPk());
			$content = $ownResultController->doOwnResults();
			return $this->studentView->getResultPage($content);
		} else if ($this->navigation->userChecksOpenResult() || $this->quizNavigation->checksResults()) {

			$resultsController = new \quiz\controller\ResultController($this->mysqli, $this->quizNavigation);
			$content = $resultsController->doStudentResults();
			return $this->studentView->getOpenResultPage($content);
		} else {

			$messageController = new \quiz\controller\MessageController($this->mysqli, 
																		$this->quizNavigation, 
																		$this->userCredentials, 
																		$this->navigation);
			$content = $messageController->doMessages();
			return $this->studentView->getStartPage($content); 
		}
	}
}

