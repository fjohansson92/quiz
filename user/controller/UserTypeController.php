<?php

namespace user\controller;

require_once("./user/model/UserTypeHandler.php");
require_once("./user/view/UserHandlingView.php");

class UserTypeController {

	/** 
	 * @var \user\model\UserTypeHandler
	 */
	private $userTypeHandler;

	/** 
	 * @var \user\view\UserHandlingView
	 */
	private $userHandlingView;

	/** 
	 * @var \user\view\AdminNavigation
	 */
	private $adminNavigation;

	/** 
	 * @param \mysqli $mysqli          
	 * @param \user\view\AdminNavigation $adminNavigation 
	 */
	public function __construct(\mysqli $mysqli, \user\view\AdminNavigation $adminNavigation) {

		$this->userTypeHandler = new \user\model\UserTypeHandler($mysqli);
		$this->userHandlingView = new \user\view\UserHandlingView($adminNavigation);
		$this->adminNavigation = $adminNavigation;
	}

	/** 
	 * @return string
	 */
	public function handleUserTypes() {

		if ($this->adminNavigation->userChange()) {

			try {
				$userId = $this->userHandlingView->getUserId();
				$this->userTypeHandler->changeType($userId);
			} catch (\Exception $exception) {
				$this->userHandlingView->changeFailed();
			}	
		}
		$teachers = $this->userTypeHandler->getTeachers();
		$students = $this->userTypeHandler->getStudents();

		return $this->userHandlingView->displayUsers($teachers, $students);
	}
}