<?php

namespace user\controller;

require_once("./user/view/TeacherView.php");
require_once("./user/view/TeacherNavigation.php");
require_once("./quiz/controller/QuizController.php");
require_once("./quiz/controller/ResultController.php");
require_once("./quiz/controller/MessageController.php");

class TeacherController {

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @var \user\view\TeacherView
	 */
	private $teacherView;

	/** 
	 * @var \user\view\TeacherNavigation
	 */
	private $navigation;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @param \mysqli  $mysqli          
	 * @param \user\model\UserCredentials $userCredentials 
	 */
	public function __construct(\mysqli $mysqli, \user\model\UserCredentials $userCredentials) {

		$this->mysqli = $mysqli;
		$this->userCredentials = $userCredentials;
		$this->navigation = new \user\view\TeacherNavigation();
		$this->teacherView = new \user\view\TeacherView($this->navigation);
		$this->quizNavigation = new \quiz\view\Navigation();
	}

	/** 
	 * @return \common\view\Page
	 */
	public function startTeacherApplication() {
		$groupController = new \quiz\controller\GroupController($this->mysqli, $this->navigation, $this->quizNavigation, $this->userCredentials);

		if ($this->navigation->userChecksGroups() || $this->quizNavigation->userChecksGroup()) {
			
			$content = $groupController->doTeacherGroup();
			return $this->teacherView->getGroupPage($content);
		} else if ($this->navigation->userChecksQuizzes() || $this->quizNavigation->userChecksQuiz()) {
			
			$quizController = new \quiz\controller\QuizController($this->mysqli, $this->quizNavigation, $this->userCredentials);
			$content = $quizController->doTeacherQuiz();
			return $this->teacherView->getQuizPage($content);
		} else if ($this->navigation->userChecksResults() || $this->quizNavigation->checksResults()) {

			$resultsController = new \quiz\controller\ResultController($this->mysqli, $this->quizNavigation);
			$content = $resultsController->doTeacherResults();
			return $this->teacherView->getResultsPage($content);
		} else {

			$messageController = new \quiz\controller\MessageController($this->mysqli, $this->quizNavigation, $this->userCredentials);
			$content = $messageController->doTeacherMessage($this->navigation);
			return $this->teacherView->getStartPage($content); 
		}
	}
}