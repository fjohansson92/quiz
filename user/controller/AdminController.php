<?php

namespace user\controller;

require_once("./user/view/AdminView.php");
require_once("./user/view/AdminNavigation.php");
require_once("./user/controller/UserTypeController.php");

class AdminController {
	
	/**
	 * @var \mysqli
	 */
	private $mysqli;

	/**
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/**
	 * @var \user\view\AdminView
	 */
	private $adminView;

	/**
	 * @var \user\view\AdminNavigation
	 */
	private $navigation;

	/** 
	 * @param \mysqli $mysqli          
	 * @param \user\model\UserCredentials $userCredentials 
	 */
	public function __construct(\mysqli $mysqli, \user\model\UserCredentials $userCredentials) {

		$this->mysqli = $mysqli;
		$this->userCredentials = $userCredentials;
		$this->adminView = new \user\view\AdminView();
		$this->navigation = new \user\view\AdminNavigation();
	}

	/** 
	 * @return \common\view\Page
	 */
	public function startAdminApplication() {

		$userTypeController = new \user\controller\UserTypeController($this->mysqli, $this->navigation);
		$content = $userTypeController->handleUserTypes();
		return $this->adminView->getStartPage($content); 
	}	
}