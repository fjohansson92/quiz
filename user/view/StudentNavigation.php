<?php

namespace user\view;

class StudentNavigation extends Navigation{

	/** 
	 * @var string
	 */
	private static $Start = "start";

	/** 
	 * @var string
	 */
	private static $Groups = "groups";

	/** 
	 * @var string
	 */
	private static $Quizzes = "quizzes";

	/** 
	 * @var string
	 */
	private static $MyResults = "myResults";

	/** 
	 * @var string
	 */
	private static $OpenResult = "results";

	/** 
	 * @var boolean
	 */
	public function userChecksGroup() {
		return isset($_GET[self::$Groups]);
	}

	public function reloadAfterGroupEdit() {

		header("Location: index.php?" . self::$Groups);
	}

	/** 
	 * @var boolean
	 */
	public function userChecksQuizzes() {
		return isset($_GET[self::$Quizzes]);
	}

	/** 
	 * @var boolean
	 */
	public function userChecksResults() {
		return isset($_GET[self::$MyResults]);
	}

	/** 
	 * @var boolean
	 */
	public function userChecksOpenResult() {
		return isset($_GET[self::$OpenResult]);
	}

	/** 
	 * @var string
	 */
	public function startPageLink() {
		return self::$Start;
	}

	/** 
	 * @var string
	 */
	public function groupPageLink() {
		return self::$Groups;
	}

	/** 
	 * @var string
	 */
	public function quizPageLink() {
		return self::$Quizzes;
	}

	/** 
	 * @var string
	 */
	public function myResultsPageLink() {
		return self::$MyResults;
	}

	/** 
	 * @var string
	 */
	public function openResultsPageLink() {
		return self::$OpenResult;
	}

	/** 
	 * @param  string $active 
	 * @return string         
	 */
	public function getMenu($active) {
		$links = array();
		$links[] = self::$Start;
		$links[] = self::$Groups;
		$links[] = self::$Quizzes;
		$links[] = self::$MyResults;
		$links[] = self::$OpenResult;
		$links[] = self::$Logout;

		$menu = "";
		$span = "";
		foreach ($links as $link) {
			$span .=  "<span class='icon-bar'></span>";
			$menu .= "<li";
			$menu .= $active == $link ? " class='active'>" : ">";
			
			switch ($link) {
				case self::$Groups :
					$menu .= "<a href='?" . self::$Groups . "'>Grupper</a>";
					break;
				case self::$Quizzes :
					$menu .= "<a href='?" . self::$Quizzes . "'>Prov</a>";
					break;
				case self::$MyResults :
					$menu .= "<a href='?" . self::$MyResults . "'>Mina resultat</a>";
					break;	
				case self::$OpenResult :
					$menu .= "<a href='?" . self::$OpenResult . "'>Öppna resultat</a>";
					break;		
				case self::$Start : 
					$menu .= "<a href='?" . self::$Start . "'>Start</a>";
					break;	
				case self::$Logout :
					$menu .= $this->logoutLink();
					break;	
			}
			$menu .= "</li>";
		}	
		return $this->getHeader($menu, $span);
	}		
}