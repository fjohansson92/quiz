<?php

namespace user\view;

class UserHandlingView {

	/** 
	 * @var \user\view\AdminNavigation
	 */
	private $adminNavigation;

	/** 
	 * @var string
	 */
	private $message = "";

	/** 
	 * @param \user\view\AdminNavigation $adminNavigation 
	 */
	public function __construct(\user\view\AdminNavigation $adminNavigation) {

		$this->adminNavigation = $adminNavigation;
	}

	/** 
	 * @param  array of \quiz\model\UserCrdentials $teachers 
	 * @param  array of \quiz\model\UserCrdentials $students 
	 * @return string
	 */
	public function displayUsers($teachers, $students) {

		$html = "<h2>Lärare</h2>$this->message";
		$html .= $this->listUsers($teachers, true);

		$html .= "<h2>Studenter</h2>";
		$html .= $this->listUsers($students, false);

		return $html;
	}

	/**
	 * @param  array of \quiz\model\UserCrdentials $users     
	 * @param  boolean $isTeacher 
	 * @return string           
	 */
	private function listUsers($users, $isTeacher) {

		$html = "<div class='shadow'><div class='users'><table class='table'>
					<thead><tr>
					<th>Användarnamn</th>
					<th>Namn</th>
					<th>Ändra rättigheter</th>
					</tr></thead><tbody>";

		foreach ($users as $user) {
			$html .= "<tr>";

			$userName = $user->getUsername();
			$html .= "<td>$userName</td>";

			$fName = $user->getFName();
			$lName = $user->getLName();
			$html .= "<td>$fName $lName</td>";

			$changeLink = $this->adminNavigation->getChangeUserLink();
			$linkText = $isTeacher ? "Ge studenträttigheter" : "Ge lärarrättigheter";
			$userPk = $user->getPk();
			$link = "<a href='?$changeLink=$userPk'>$linkText</a>";
			$html .= "<td>$link</td></tr>";
		}
		$html .= "</tbody></table></div></div>";

		return $html;
	}

	/** 
	 * @return string
	 */
	public function getUserId() {

		if ($this->adminNavigation->userChange()) {

			return $_GET[$this->adminNavigation->getChangeUserLink()];
		} 
		return "";
	}

	public function changeFailed() {
		$this->message = "<p class='error'>Det gick inte att ändra rättigheter för användaren</p>";
	}
}