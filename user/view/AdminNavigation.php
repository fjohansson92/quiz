<?php

namespace user\view;

class AdminNavigation extends Navigation{

	/** 
	 * @var string
	 */
	private static $ChangeUser = "change";

	/** 
	 * @return boolean
	 */
	public function getChangeUserLink() {
		return self::$ChangeUser;
	}

	/** 
	 * @return boolean
	 */
	public function userChange() {
		return isset($_GET[self::$ChangeUser]);
	}

	/** 
	 * @return string
	 */
	public function getMenu() {

		$logout = $this->logoutLink();
		$menu = "<li>$logout</li>";
		$span =  "<span class='icon-bar'></span>";
		return $this->getHeader($menu, $span);
	}
}