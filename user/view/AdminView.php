<?php

namespace user\view;

class AdminView {

	/** 
	 * @var \user\view\AdminNavigation
	 */
	private $navigation;

	public function __construct() {

		$this->navigation = new \user\view\AdminNavigation();
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page         
	 */
	public function getStartPage($content) {

		$html = $this->navigation->getMenu();
		$html .= $content;

		return new \common\view\Page("Admin-Användarrättigheter", $html);
	}
}