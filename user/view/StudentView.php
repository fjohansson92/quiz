<?php

namespace user\view;

class StudentView {

	/** 
	 * @var \user\view\StudentNavigation
	 */
	private $navigation;

	public function __construct() {

		$this->navigation = new \user\view\StudentNavigation();
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page          
	 */
	public function getStartPage($content) {

		$active = $this->navigation->startPageLink();
		$html = $this->navigation->getMenu($active);
		$html .= $content;
		return new \common\view\Page("Förstasidan - Quiz", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page          
	 */
	public function getGroupPage($content) {

		$active = $this->navigation->groupPageLink();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Grupper</h1>";
		$html .= $content;
		return new \common\view\Page("Grupper - Quiz", $html);

	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page          
	 */
	public function getQuizPage($content) {

		$active = $this->navigation->quizPageLink();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Prov</h1>";
		$html .= $content;
		return new \common\view\Page("Prov - Quiz", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page          
	 */
	public function getResultPage($content) {

		$active = $this->navigation->myResultsPageLink();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Mina Resultat</h1>";
		$html .= $content;
		return new \common\view\Page("Mina resultat - Quiz", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page          
	 */
	public function getOpenResultPage($content) {

		$active = $this->navigation->openResultsPageLink();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Öppna Resultat</h1>";
		$html .= $content;
		return new \common\view\Page("Öppna resultat - Quiz", $html);
	}
}