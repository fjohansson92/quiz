<?php

namespace user\view;

require_once("./user/view/Navigation.php");

class TeacherNavigation extends Navigation{

	/** 
	 * @var string
	 */
	private static $Start = "start";

	/** 
	 * @var string
	 */
	private static $Groups = "groups";

	/** 
	 * @var string
	 */
	private static $Quizzes = "quizzes";

	/** 
	 * @var string
	 */
	private static $Results = "results";

	/** 
	 * @var boolean
	 */
	public function userChecksGroups() {
		return isset($_GET[self::$Groups]);
	}

	/** 
	 * @var boolean
	 */
	public function userChecksQuizzes() {
		return isset($_GET[self::$Quizzes]);
	}

	/** 
	 * @var boolean
	 */
	public function userChecksResults() {
		return isset($_GET[self::$Results]);
	}

	/** 
	 * @var string
	 */
	public function getStart() {
		return self::$Start;
	}

	/** 
	 * @var string
	 */
	public function getGroups() {
		return self::$Groups;
	}

	/** 
	 * @var string
	 */
	public function getQuizzes() {
		return self::$Quizzes;
	}

	/** 
	 * @var string
	 */
	public function getResults() {
		return self::$Results; 
	}

	public function reloadToGroupPage() {

		header("Location: index.php?" . self::$Groups);
	}


	/** 
	 * @param  string $active 
	 * @return string         
	 */
	public function getMenu($active) {

		$links = array();
		$links[] = self::$Start;
		$links[] = self::$Groups;
		$links[] = self::$Quizzes;
		$links[] = self::$Results;
		$links[] = self::$Logout;

		$menu = "";
		$span = "";
		foreach ($links as $link) {
			$span .=  "<span class='icon-bar'></span>";

			$menu .= "<li";
			$menu .= $active == $link ? " class='active'>" : ">";

			switch ($link) {

				case self::$Groups :

					$menu .= "<a href='?" . self::$Groups . "'>Grupper</a>";
					break;
				case self::$Quizzes :

					$menu .= "<a href='?" . self::$Quizzes . "'>Prov</a>";
					break;
				case self::$Results :

					$menu .= "<a href='?" . self::$Results . "'>Resultat</a>";
					break;		
				case self::$Start : 
					$menu .= "<a href='?" . self::$Start . "'>Start</a>";
					break;	
				case self::$Logout :
					$menu .= $this->logoutLink();
					break;		
			}
			$menu .= "</li>";
		}	
		return $this->getHeader($menu, $span);
	}	
}