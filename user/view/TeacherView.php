<?php

namespace user\view;

require_once("./user/view/TeacherNavigation.php");

class TeacherView {

	/** 
	 * @var \user\view\TeacherNavigation
	 */
	private $navigation;

	public function __construct() {

		$this->navigation = new \user\view\TeacherNavigation();
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page       
	 */
	public function getStartPage($content) {

		$active = $this->navigation->getStart();
		$html = $this->navigation->getMenu($active);
		$html .= $content;
		return new \common\view\Page("Förstasidan - Quiz", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page       
	 */
	public function getGroupPage($content) {

		$active = $this->navigation->getGroups();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Grupper</h1>";
		$html .= $content;
		return new \common\view\Page("Grupper - Quiz", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page       
	 */
	public function getQuizPage($content) {

		$active = $this->navigation->getQuizzes();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Prov</h1>";
		$html .= $content;
		return new \common\view\Page("Prov - Quiz", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page       
	 */
	public function getResultsPage($content) {

		$active = $this->navigation->getResults();
		$html = $this->navigation->getMenu($active);
		$html .= "<h1>Resultat</h1>";
		$html .= $content;
		return new \common\view\Page("Resultat - Quiz", $html);		
	}
}
