<?php

namespace user\view;

class Navigation {

	/** 
	 * @var string
	 */
	protected static $Logout = "logout";

	/** 
	 * @return string
	 */
	public function startPageLink() {
		return "<a href=?>Start</a>";
	}

	/** 
	 * @return string
	 */
	protected function logoutLink() {
		return "<a href='?" . self::$Logout . "'>Logga ut</a>";
	}

	public function reloadToStartPage() {
		header("Location: index.php?");
	}

	/** 
	 * @param  string $menu 
	 * @param  string $span 
	 * @return string       
	 */
	public function getHeader($menu, $span) {

		return "<div class='navbar navbar-inverse navbar-fixed-top'>
			      <div class='container'>
			        <div class='navbar-header'>
			          <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
			          		$span
			          </button>
			          <a class='navbar-brand' href='#''>Quiz</a>
			        </div>
			        <div class='collapse navbar-collapse'>
			          <ul class='nav navbar-nav'>
						$menu
			          </ul>
			        </div>
			      </div>
			    </div>";
	}	
}
