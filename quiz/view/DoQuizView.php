<?php

namespace quiz\view;

class DoQuizView {


	/** 
	 * @var string
	 */
	private static $Guesses = "DoQuizView::Guesses";

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @param \quiz\view\Navigation $quizNavigation
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation) {

		$this->quizNavigation = $quizNavigation;
	}

	/** 
	 * @param \quiz\model\QuizCredentials  $quizCredentials 
	 * @param array of quiz\model\QuestionCredentials $questions       
	 * @return string HTML                 
	 */
	public function getQuizForm(\quiz\model\QuizCredentials $quizCredentials, $questions) {

		$html = "<div class='row shadow'><div class='col-md-6 col-md-offset-3'>";

		$titel = $quizCredentials->getTitel();
		$html .= "<h2>$titel</h2>";

		$submit = $this->quizNavigation->getSubmitQuiz();
		$html .= "<form method='post' action='?$submit'
					 enctype='multipart/form-data' >
					<fieldset>";
		$html .= $this->listQuestions($questions);
		$html .= "<input type='submit' value='Skicka'type='button' class='btn btn-primary ' />
					</fieldset>
					</form>
				</div></div>";
		return $html;
	}

	/** 
	 * @param  array of quiz\model\QuestionCredentials $questions
	 * @return string HTML
	 */
	private function listQuestions($questions) {

		$html = "<dl>";
		foreach ($questions as $question) {

			$html .= "<dt><div>";

			$titel = $question->getQuestion();
			$html .= "<h3 class='question'>$titel</h3>";

			$description = $question->getDescription();
			$html .= "<p>$description</p>";

			$questionPk = $question->getPk();
			foreach ($question->getAnswers() as $answer) {

				$answerTitel = $answer->getAnswer();
				$answerPk = $answer->getPk();

				$html .= "<div class='checkbox'><label>
						<input type='checkbox' value='$answerPk' name='" . self::$Guesses . "[]' />$answerTitel
						</label></div>";
			}
			$html .= "</div></dt>";
		}
		$html .= "</dl>";
		return $html;
	}

	/** 
	 * @return array integer
	 */
	public function getGuesses() {

		$checked = array();
		if (!empty($_POST[self::$Guesses])) {
			foreach ($_POST[self::$Guesses] as $guess) {
				$checked[] = $guess;
			} 
		}
		return $checked;
	}
}