<?php

namespace quiz\view;

require_once("./common/model/DateTimeException.php");

class NewQuizView extends GroupPicker {

	/** 
	 * @var string
	 */
	private static $Titel = "NewQuizView::Titel";

	/** 
	 * @var string
	 */
	private static $Year = "NewQuizView::Year";

	/** 
	 * @var string
	 */
	private static $Month = "NewQuizView::Month";

	/** 
	 * @var string
	 */
	private static $Day = "NewQuizView::Day";

	/** 
	 * @var string
	 */
	private static $Hour = "NewQuizView::Hour";

	/** 
	 * @var string
	 */
	private static $Minute = "NewQuizView::Minute";

	/** 
	 * @var string
	 */
	private $message = "";

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var array with GroupCredentials
	 */
	private $groups;

	/** 
	 * @var integer
	 */
	private $userDataPk;

	/** 
	 * @param \quiz\view\Navigation $quizNavigation
	 * @param array with GroupCredentials $groups  
	 * @param integer $userDataPk 
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation, $groups, $userDataPk) {

		$this->quizNavigation = $quizNavigation;
		$this->groups = $groups;
		$this->userDataPk = $userDataPk;
	}

	/** 
	 * @param \quiz\model\QuizCredentials $quizCredentials
	 * @param string $editQuestionForm
	 * @param string $addQuestionForm
	 * @return string HTML
	 */
	public function getEditQuizForm(\quiz\model\QuizCredentials $quizCredentials, $editQuestionForm, $addQuestionForm) {

		$editQuiz = $this->quizNavigation->getEditQuizCredentials();
		$legend = "Redigera provets uppgifter";
		$btnText = "Spara";

		$html = $this->quizForm($editQuiz, $legend, $quizCredentials, $btnText);
		$html .= $editQuestionForm;
		$html .= $addQuestionForm;

		return $html;
	}

	/** 
	 * @return string HTML
	 */
	public function getQuizForm() {

		$addQuiz = $this->quizNavigation->getAddQuiz();
		$legend = "Skapa nytt prov";
		$btnText = "Gå vidare";

		return $this->quizForm($addQuiz, $legend, NULL, $btnText);
	}

	/** 
	 * @param  string $action          
	 * @param  string $legend          
	 * @param  \quiz\model\QuizCredentials $quizCredentials 
	 * @param  string $btnText         
	 * @return string HTML                  
	 */
	private function quizForm($action, $legend, \quiz\model\QuizCredentials $quizCredentials = NULL, $btnText) {

		if(empty($this->groups)) {
			return "<p>Du måste ha minst en grupp innan du skapar ett prov</p>";
		}

		$html = "<div class='row shadow'><div class='col-md-6 col-md-offset-3'>
				<form method='post' action='?$action' data-validate='parsley'
					 enctype='multipart/form-data' class='form-horizontal' role='form'>
					<fieldset>
						<legend>
							$legend
						</legend>";

		$html .= $this->getFormContent($quizCredentials);

		$html .= "<input type='submit' value='$btnText' type='button' class='btn btn-primary pull-right'/> 
					</fieldset>
				</form></div></div>";	

		return $html;
	}

	/** 
	 * @param  \quiz\model\QuizCredentials $quizCredentials 
	 * @return string HTML                
	 */
	private function getFormContent(\quiz\model\QuizCredentials $quizCredentials = NULL) {

		$titel = $this->getTitle();
		$dateTime = NULL;
		$groupName = "";
		if (isset($quizCredentials)) {
			$titel = $quizCredentials->getTitel();
			$dateTime = $quizCredentials->getEndDateObject();
			$groupName = $quizCredentials->getGroupNameFk();
		}

		$html =  "<div class='error'>$this->message</div>
				<div class='form-group'>
					<label for='title'  class='col-lg-2 control-label'>Titel:</label>
					<div class='col-lg-10'>
					<input type='text' name='". self::$Titel .
						"' id='titelID'  maxlength='50'  value='$titel'type='text' id='title' class='form-control' placeholder='Prov titel' 
						data-validation-minlength='0' data-required-message='Du måste ange ett namn på provet' 
						maxlength='16' data-trigger='focusout' data-required='true'	/>
					</div>
				</div>";

		$html .= $this->getDateForm($dateTime);		
		$html .= "<table class='table'>";			
		$html .= $this->getGroupList($this->groups, false, false, true, true, $groupName);
		$html .= "</table>";
		return $html;		
	}

	/** 
	 * @param  \DateTime $dateTime 
	 * @return string HTML           
	 */
	private function getDateForm($dateTime) {

		$year = "";
		$month = "";
		$day = "";
		$hour = "";
		$minute = "";

		if (isset($dateTime)) {

			$date = $dateTime->getDateObject();	
			$year = $date->format('Y');
			$month = $date->format('n');
			$day = $date->format('j');
			$hour = $date->format('d');
			$minute = intval($date->format('i'));
		}

		$html = "<div class='form-group'>
					<label>Utgångsdatum: YYYY-MM-DD</label>
					<div class='row'>";

		$html .= $this->selectLoop(self::$Year, date('Y'), 5, $year);
		$html .= $this->selectLoop(self::$Month, 1, 12, $month);
		$html .= $this->selectLoop(self::$Day, 1, 31, $day);
	
		$html .= "	</div>
				</div>
				<div class='form-group'>
					<label>Utgångstid: HH:ii</label>
					<div class='row'>";

		$html .= $this->selectLoop(self::$Hour, 0, 24, $hour);
		$html .= $this->selectLoop(self::$Minute, 0, 60, $minute);

		$html .= "	</div>
				</div>";

		return $html;
	}

	/** 
	 * @param  string $selectName   
	 * @param  integer $startValue   
	 * @param  integer $incrementSum 
	 * @param  integer $defaultTime  
	 * @return string HTML             
	 */
	private function selectLoop($selectName, $startValue, $incrementSum, $defaultTime) {

		$html = "<div class='col-md-3'>
				<select name='$selectName' class='form-control'>";	
		for ($i = $startValue; $i < $startValue + $incrementSum; $i++) {

			$selected = $i == $defaultTime ? "selected='selected'" : "";

			$html .= "<option value='$i' $selected >$i</option>"; 
		}
		$html .= "</select>
				</div>";
		return $html;
	}

	/** 
	 * @return \quiz\model\QuizCredentials
	 * @throws If invalid input for new quiz
	 */
	public function getNewQuiz() {

		try {
			return new \quiz\model\QuizCredentials(NULL, 
												  $this->getTitle(), 
												  $this->getEndDate(), 
												  $this->getGroupPk($this->groups), 
												  $this->userDataPk, 
												  \quiz\model\PublishType::CREATOR);

		} catch (\common\model\TitleEmptyException $exception) {
			$this->message .= "<p>Titel på provet saknas</p>";
		} catch (\common\model\TitleLengthException $exception) {			
			$length = $exception->getLength();
			$this->message .= "<p>Titeln får max vara " . $length . " tecken</p>"; 
		} catch (\common\model\DateTimeException $exception) {
			$this->message .= "<p>Ogiltligt datum</p>"; 
		} catch (\Exception $exception) {

			$this->message .= "<p>Ingen grupp vald</p>"; 
		}
		throw new \Exception();
	}

	/** 
	 * @return \quiz\model\DateTime
	 * @throws If DateTime couldn't be created
	 */
	private function getEndDate() {

		if (isset($_POST[self::$Year]) && isset($_POST[self::$Month]) && isset($_POST[self::$Day])
			&& isset($_POST[self::$Hour]) && isset($_POST[self::$Minute])){

			return \quiz\model\DateTime::createDateTime($_POST[self::$Year], 
														$_POST[self::$Month], 
														$_POST[self::$Day], 
														$_POST[self::$Hour], 
														$_POST[self::$Minute]);
		} 
		throw new \common\model\DateTimeException();
	}

	/** 
	 * @return string
	 */
	private function getTitle() {

		if (isset($_POST[self::$Titel])){	
			return $this->sanitize($_POST[self::$Titel]);
		}else {
			return "";
		}	
	}

	/** 
	 * @return string
	 */
	private function sanitize($input) {
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}
	

}

