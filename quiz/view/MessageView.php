<?php

namespace quiz\view;

require_once("./common/model/LengthException.php");
require_once("./common/model/TitleEmptyException.php");
require_once("./common/model/TitleLengthException.php");
require_once("./common/model/TextEmptyException.php");
require_once("./common/model/TextLengthException.php");

class MessageView {

	/** 
	 * @var \quiz\view\Navigation $quizNavigation
	 */
	private $quizNavigation;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/**
	 * @var string
	 */
	private static $Title = "MessageView::Titel";

	/**
	 * @var string
	 */
	private static $Text = "MessageView::Text";

	/**
	 * @var string
	 */
	private $message;

	/** 
	 * @param \quiz\view\Navigation       $quizNavigation  
	 * @param \user\model\UserCredentials $userCredentials 
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation, \user\model\UserCredentials $userCredentials) {

		$this->quizNavigation = $quizNavigation;
		$this->userCredentials = $userCredentials;
	}

	/** 
	 * @param  array if \quiz\model\MessageCredentials $messages 
	 * @return string
	 */
	public function getMessageForm($messages) {

		$sendMessage = $this->quizNavigation->getSendMessage();

		$title = $this->getTitle();
		$text = $this->getText();

		$html = "<div class='row'><div class='col-md-8 col-md-offset-2 shadow' >
				<form class='form-horizontal' role='form' method='post' action='?$sendMessage' enctype='multipart/form-data' data-validate='parsley' >
					<fieldset>
						<legend>
							Lägg upp ett meddelande
						</legend>
						<div class='error col-md-offset-2'>$this->message</div>
						<div class='form-group'>
							<label for='title' class='col-lg-2 control-label'>Titel:</label>
							<div class='col-lg-10'>
								<input type='title' name='" . self::$Title . "'
									 id='title' maxlength='100' type='text' class='form-control' placeholder='Titel' value='$title' 
									data-validation-minlength='0' data-required-message='Du måste ange en titel' maxlength='16' data-trigger='focusout' data-required='true' />
							</div>
						</div>
						<div class='form-group'>
							<label for='text' class='col-lg-2 control-label' >Meddelande:</label>
							<div class='col-lg-10'>
								<textarea class='form-control' rows='3' id='title' maxlength='400' placeholder='Meddelande' name='" . self::$Text . "' 
									data-validation-minlength='0' data-required-message='Du måste ange ett meddelande' maxlength='16' data-trigger='focusout' data-required='true' >$text</textarea>
							</div>
						</div>
						<div class='form-group'>
						    <div class='col-lg-offset-2 col-lg-10'>
						      <button type='submit' value='send' type='button' class='btn btn-primary pull-right'>Skicka</button>
						    </div>
						  </div>
					</fieldset>
				</form>
			</div></div>";			

		$html .= $this->getMessageList($messages);
		return $html;
	}

	/** 
	 * @param  array if \quiz\model\MessageCredentials $messages 
	 * @return string
	 */
	public function getMessageList($messages) {

		$html = "<dl><div class='row'><div class='col-md-8 col-md-offset-2 shadow'>";

		if (empty($messages)) {
			$html .= "<h3>Det finns inga meddelanden.</h3>";
		} else {

			foreach ($messages as $message) {

				$html .= "<dt><div class='message'>";

				$title = $message->getTitle();
				$html .= "<h3>$title</h3>";

				$fName = $message->getFName();
				$lName = $message->getLName();
				$html .= "<small>av $fName $lName</small>";

				$messageText = $message->getText();
				$html .= "<p>$messageText</p></div></dt>";
			}
		}
		$html .= "</div></div></dl>";
		return $html;
	}

	/** 
	 * @return \quiz\model\MessageCredentials
	 * @throws If invalid input values for message
	 */
	public function getMessage() {

		try {
			return new \quiz\model\MessageCredentials(NULL, $this->userCredentials , $this->getTitle(), $this->getText());
		} catch (\common\model\TitleEmptyException $exception) {
			$this->message .= "<p>Titel saknas</p>";
		} catch (\common\model\TitleLengthException $exception) {
			$length = $exception->getLength();
			$this->message .= "<p>Titeln får max vara " . $length . " tecken</p>"; 
		} catch (\common\model\TextEmptyException $exception) {
			$this->message .= "<p>Meddelande saknas</p>";
		} catch (\common\model\TextLengthException $exception) {
			$length = $exception->getLength();
			$this->message .= "<p>Meddelandet får max vara " . $length  . " tecken</p>"; 
		}

		throw new \Exception();
	}

	/** 
	 * @return string 
	 */
	private function getTitle() {

		if (isset($_POST[self::$Title])){	
			return $this->sanitize($_POST[self::$Title]);
		}else {
			return "";
		}	
	}

	/** 
	 * @return string 
	 */
	private function getText() {

		if (isset($_POST[self::$Text])){	
			return $this->sanitize($_POST[self::$Text]);
		}else {
			return "";
		}	
	}

	/** 
	 * @return string 
	 */	
	private function sanitize($input) {
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}
}