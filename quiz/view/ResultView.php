<?php

namespace quiz\view;

class ResultView {

	/** 
	 * @var \quiz\view\Navigation 
	 */
	private $quizNavigation;

	/** 
	 * @var boolean
	 */
	private $isTeacher = false;

	/** 
	 * @var boolean
	 */
	private $published = false;

	/**
	 * @var string
	 */
	private $message;

	/** 
	 * @var string
	 */
	private $removeMessage;

	/** 
	 * @param \quiz\view\Navigation $quizNavigation
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation) {

		$this->quizNavigation = $quizNavigation;
	}

	/** 
	 * @param  array of quiz\moodel\ResultCompilation $privateResults 
	 * @param  array of quiz\moodel\ResultCompilation $openResults    
	 * @return string HTML               
	 */
	public function listTeacherResults($privateResults, $openResults) {

		$this->isTeacher = true;

		$html = "<h3>Privata resultat</h3>$this->message";
		$html .= $this->getResultTable($privateResults);

		$this->published = true;

		$html .= "<h3>Publicerade resultat</h3>$this->removeMessage";
		$html .= $this->getResultTable($openResults);

		return $html;
	}

	/** 
	 * @param  array of quiz\moodel\ResultCompilation $openResults 
	 * @return string HTML
	 */
	public function listStudentResults($openResults) {

		$html = "<h3>Publicerade resultat</h3><div class='error'>$this->message</div>";
		$html .= "<div class='shadow'>";
		$html .= $this->getResultTable($openResults);
		$html .= "</div>";

		return $html;
	}

	/** 
	 * @param  array of quiz\moodel\ResultCompilation $resultCompilations 
	 * @return string HTML                   
	 */
	private function getResultTable($resultCompilations) {

		if (empty($resultCompilations)) {
			return "<p>Resultat saknas.</p>";
		}


		$html = "<table class='table shadow'><thead><tr>";
		$html .= "<th>Provtitel</th>";
		$html .= "<th>Deadline</th>";
		$html .= "<th>Medel</th>";
		$html .= "<th>Maxpoäng</th>";
		$html .= "<th>Deltagare</th>";
		$html .= "<th>Provskapare</th>";

		if ($this->isTeacher) {
			$html .= "<th>Ändra status</th>";
		}	
		$html .= "</tr></thead><tbody>";


		foreach ($resultCompilations as $resultCompilation) {

			$html .= "<tr><div>";
			$html .= $this->getResultRow($resultCompilation);
			$html .= "</tr></div>";
		}
		$html .= "</tbody><table>";

		return $html;
	}

	/** 
	 * @param  \quiz\model\ResultCompilation $resultComp 
	 * @return string HTML                               
	 */
	private function getResultRow(\quiz\model\ResultCompilation $resultComp) {

		$checkResult = $this->quizNavigation->getQuizResult();

		$quizId = $resultComp->getQuizId();
		$titel = $resultComp->getQuizTitel();
		$html = "<td><a href='?$checkResult=$quizId'>$titel</a></td>";

		$endDate = $resultComp->getEndDate();
		$html .= "<td>$endDate</td>";

		$average = round($resultComp->getAverage(), 2);
		$html .= "<td>$average</td>";

		$max = $resultComp->getMaxCorrectAnswers();
		$html .= "<td>$max</td>";

		$participants = $resultComp->getParticipants();
		$html .= "<td>$participants</td>";

		$fName = $resultComp->getFName();
		$LName = $resultComp->getLName();
		$html .= "<td>$fName $LName</td>";

		if ($this->isTeacher) {
			
			$resultStatusPk = $resultComp->getResultStatus();
			if (!$this->published) {
				$publishResult = $this->quizNavigation->getPublishResult();
				$html .= "<td><a href='?$publishResult=$resultStatusPk'>Publicera</a></td>";
			} else {
				$unpublishResult = $this->quizNavigation->getUnpublishResultResult();
				$html .= "<td><a href='?$unpublishResult=$resultStatusPk'>Dölj</a></td>";
			}
		}

		return $html;
	}


	public function publishSuccess() {
		$this->message = "<p>Resultatet publicerades</p>";
	}

	public function publishFailed() {
		$this->message = "<p class='error'>Det gick inte att publicera provet</p>";
	}

	public function removeSuccess() {
		$this->message = "<p>Resultatet har döljts</p>";	
	}

	public function unPublishFailed() {
		$this->message = "<p class='error'>Det gick inte att dölja provet</p>";
	}

	public function quizResultFailed() {
		$this->message = "<p class='error'>Det gick inte att visa resultatet för provet</p>";
	}

	/** 
	 * @return string
	 */
	public function getQuizId() {

		if ($this->quizNavigation->checksQuizResult()) {

			return $this->quizNavigation->getQuizResultId();
		}
		return "";
	}

	/** 
	 * @return string
	 */
	public function getResultStatusId() {

		if ($this->quizNavigation->userPublishResult()) {

			return $this->quizNavigation->getPublishId();
		}
		return "";
	}

	/** 
	 * @return string
	 */
	public function getUnPublishId() {

		if ($this->quizNavigation->userUnPublishResult()) {

			return $this->quizNavigation->getUnpublishResultId();
		}
		return "";
	}


	/** 
	 * @param  \quiz\model\ResultCredentials $resultCredentials 
	 * @param  array of quiz\model\QuestionCredential $questions         
	 * @return string                    
	 */
	public function showResult(\quiz\model\ResultCredentials $resultCredentials, $questions) {

		$correctAnswers = $resultCredentials->getCorrectAnswers();
		$maxCorrectAnswers = $resultCredentials->getMaxCorrectAnswers();

		$html = "<div class='row shadow'><div class='col-md-6 col-md-offset-3'>
					<h2>Du hade $correctAnswers rätt av $maxCorrectAnswers möjliga!</h2>";

		if (!empty($questions)) {

			$html .= "<h3>Frågor du svarade fel på:</h3>";
			foreach ($questions as $question) {
				$questionTitel = $question->getQuestion();
				$html .= "<p>$questionTitel</p>";
			}
		}
		$html .= "</div></div>";

		return $html;
	}

	/** 
	 * @param  array of quiz\model\ResultCredentials $ownResults [description]
	 * @param array of quiz\model\QuizCredentials $quizzes  
	 * @return string           
	 */
	public function listOwnResults($ownResults, $quizzes) {

		$html = "<div class='row shadow'><div class='col-md-8 col-md-offset-2'>";
		$html .= "<table class='table'><thead><tr>";
		$html .= "<th>Prov titel</th>";
		$html .= "<th>Poäng</th>";
		$html .= "<th>Maxpoäng</th></tr></thead><tbody>";

		foreach ($ownResults as $result) {

			$html .= "<tr><div>";

			$quizTitel = "";
			foreach ($quizzes as $quiz) {
				if ($quiz->getPk() == $result->getQuizFk()) {
					$quizTitel = $quiz->getTitel();
				}
			}
			$html .= "<td>$quizTitel</td>";

			$correct = $result->getCorrectAnswers();
			$html .= "<td>$correct</td>";

			$possible = $result->getMaxCorrectAnswers();
			$html .= "<td>$possible</td>";

			$html .= "</tr></div>";
		}

		$html .= "</tbody></table></div></div>";
		return $html;
	}
}