<?php

namespace quiz\view;

require_once("./quiz/model/GroupObserver.php");
require_once("./quiz/view/GroupPicker.php");

class GroupView extends GroupPicker implements \quiz\model\GroupObserver{

	/** 
	 * @var string
	 */
	private static $GroupName = "GroupView::GroupName";

	/** 
	 * @var \quiz\view\Navigation 
	 */
	private $quizNavigation;

	/** 
	 * @var array with GroupCredentials
	 */	
	private $myGroups;

	/** 
	 * @var array with GroupCredentials
	 */	
	private $otherGroups;

	/** 
	 * @var string
	 */
	private $addMessage = "";

	/** 
	 * @var string
	 */	
	private $removeMessage = "";

	/** 
	 * @param \quizview\Navigation $quizNavigation 
	 * @param array with GroupCredentials $myGroups       
	 * @param array with GroupCredentials $otherGroups    
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation, $myGroups, $otherGroups) {

		$this->quizNavigation = $quizNavigation;
		$this->myGroups = $myGroups;
		$this->otherGroups = $otherGroups;
	}

	/** 
	 * @return string HTML
	 */
	public function getTeacherGroupPage() {

		$html = "<div class='row col-md-12'>";
		$html .= $this->getGroupForm();
		$html .= $this->getMyGroups();
		$html .= $this->getOtherGroups();
		$html .= "</div>";

		return $html;
	}

	/** 
	 * @return String HTML
	 */
	private function getMyGroups() {

		if (empty($this->myGroups)) {
			return "<h3>Du har inga grupper</h3>";
		}

		$action = $this->quizNavigation->getRemoveGroups();

		$html = "<form method='post' action='?$action'
				 enctype='multipart/form-data' class='groups shadow'>
				<fieldset>
					<legend>
						Välj grupper som ska tas bort
					</legend>
					<div class='error'>$this->removeMessage</div>";	
		$html .= "<table class='table'>";

		$html .= $this->getGroupList($this->myGroups, true);

		$html .= "</table><input type='submit' value='Ta bort' type='button' class='btn btn-primary pull-right'/>
					</fieldset>
					</form>";
		return $html;		
	}

	/** 
	 * @return String HTML
	 */
	private function getOtherGroups() {

		if (empty($this->otherGroups)) {
			return "<h3>Det finns inga övriga grupper</h3>";
		}

		$html = "<h3>Övriga grupper</h3>";
		$html .= "<div class='groups shadow'><table class='table'>";
		$html .= $this->getGroupList($this->otherGroups, false);
		return $html .= "</table></div>";
	}

	/** 
	 * @return string HTML
	 */
	public function getStudentGroupPage() {

		$action = $this->quizNavigation->getFollowGroup();

		$html = "<form method='post' action='?$action'
					 enctype='multipart/form-data' class='groups shadow'>
					<fieldset>
						<legend>
							Välj grupper som du vill följa
						</legend>";


		$html .= "<table class='table'>";
		if (empty($this->myGroups) && empty($this->otherGroups)) {
			$html .= "Du finns inga grupper just nu.";
		} else {

			$html .= $this->getGroupList($this->myGroups, true, true);	
			$html .= $this->getGroupList($this->otherGroups, true, false, false);
		}
		$html .= "</table>";
		
		$html .= "<input type='submit' value='Spara' type='button' class='btn btn-primary  pull-right'/>
					</fieldset>
					</form>";

		return $html;
	}

	/** 
	 * @return String HTML
	 */
	private function getGroupForm() {

		$createGroup = $this->quizNavigation->getCreateGroup();

		return "<form method='post' action='?$createGroup' class='shadow groups groupForm'
					 enctype='multipart/form-data' data-validate='parsley'>
					<fieldset>
						<legend>
							Lägg till ny grupp
						</legend>
						<div class='error'>$this->addMessage</div>	
						<div class='form-group'>
							<label for='group' class='col-lg-2 control-label' >Gruppnamn:</label>
							<div class='col-lg-3'>
								<input type='text' name='". self::$GroupName . "' id='group'  maxlength='100' 
									data-validation-minlength='0' data-required-message='Du måste ange en titel' 
										maxlength='16' data-trigger='focusout' data-required='true' />
							</div>			
							<input type='submit' value='Lägg till' type='button' class='btn btn-primary '/>	
						</div>	
				</fieldset>
				</form>";
	}

	/** 
	 * @return String HTML
	 */
	private function sanitize($input) {
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}

	/** 
	 * @return String HTML
	 */
	public function getGroupName() {

		if (isset($_POST[self::$GroupName])){	
			return $this->sanitize($_POST[self::$GroupName]);
		}else {
			return "";
		}	
	}

	public function groupNameEmptyFail() {
		$this->addMessage .= "<p>Gruppnamn saknas.</p>";
	}

	public function groupNameLengthFail($length) {
		$this->addMessage .= "<p>Gruppnamnet får max vara " . $length . " tecken långt</p>";	
	}

	public function uniqNameFail() {
		$this->addMessage = "<p>Det gick tyvärr inte att lägga till gruppen. Kontrollera så att namnet är unikt.</p>";
	}

	public function removeFailed() {
		$this->removeMessage = "<p>Välj en grupp att ta bort.</p>";
	}

	/** 
	 * @return array integer
	 */
	public function getPickedGroups() {

		$allGroups = array_merge($this->myGroups, $this->otherGroups);
		return $this->getGroups($allGroups);
	}	
}