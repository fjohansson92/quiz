<?php

namespace quiz\view;

class QuizResultView {

	/** 
	 * @param  \quiz\model\ResultCompilation $resultCompilation    
	 * @param  array of quiz\model\QuestionCompilation  $questionCompilations 
	 * @return string                                           
	 */
	public function showQuizResult(\quiz\model\ResultCompilation $resultCompilation, $questionCompilations) {

		$quizCredentials = $resultCompilation->getQuizCredentials();
		$html = "<div class='row sideShadow'><div class='col-md-8 col-md-offset-2 shadow' ><div class='col-md-6 col-md-offset-3'>";

		$html .= "<dl>";
		foreach ($questionCompilations as $quesitonComp) {
			$html .= "<dt><div>";
			$html .= $this->getQuestionResult($quesitonComp);
			$html .= "</div></dt>";
		}
		$html .= "</dl>";
		$html .= "</div></div></div>";

		return $html;
	}

	/** 
	 * @param  \quiz\model\QuesitonCompilation $questionCompilation 
	 * @return string                                          
	 */
	private function getQuestionResult(\quiz\model\QuesitonCompilation $questionCompilation) {

		$questionCredentials = $questionCompilation->getQuestion();
		
		$question = $questionCredentials->getQuestion();
		$html = "<h3 class='question'>$question</h3>";

		$description = $questionCredentials->getDescription();
		$html .= "<p>$description</p>";

		$html .= "<ul>";
		$answers = $questionCredentials->getAnswers();
		foreach ($answers as $answer) {
			$answerText = $answer->getAnswer();
			$html .= "<li>$answerText</li>";
		}
		$html .= "</ul>";

		$average = $questionCompilation->getAverageProcent();
		$rounded = round($average, 2);
		$html .= "<h4>$rounded % svarade rätt på frågan<h4>";

		return $html;				
	}

}