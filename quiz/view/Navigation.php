<?php

namespace quiz\view;

class Navigation {

	/** 
	 * @var string
	 */
	private static $createGroup = "createGroup";

	/** 
	 * @var string
	 */
	private static $remove = "remove";

	/** 
	 * @var string
	 */
	private static $newQuiz = "newQuiz";

	/** 
	 * @var string
	 */
	private static $addQuiz = "addQuiz";

	/** 
	 * @var string
	 */
	private static $Question = "question";

	/** 
	 * @var string
	 */
	private static $AddQuestion = "addQuestion";

	/** 
	 * @var string
	 */
	private static $Publish = "publish";

	/** 
	 * @var string
	 */
	private static $Edit = "edit";

	/** 
	 * @var string
	 */
	private static $EditQuizCredentials = "editQuiz";

	/** 
	 * @var string
	 */
	private static $RemoveQuestion = "removeQuestion";

	/** 
	 * @var string
	 */
	private static $AddEditQuestion = "addEditQuestion";

	/** 
	 * @var string
	 */
	private static $EditQuestion = "editQuestion";

	/** 
	 * @var string
	 */
	private static $SaveEdditedQuestion = "saveEdditedQuestion";

	/** 
	 * @var string
	 */
	private static $FollowGroup = "followGroups";

	/** 
	 * @var string
	 */
	private static $QuizResult = "quizResult";

	/** 
	 * @var string
	 */
	private static $PublishResult = "publishResults";

	/** 
	 * @var string
	 */
	private static $UnpublishResultResult = "unpublishResult";

	/** 
	 * @var string
	 */
	private static $Quiz = "quiz";

	/** 
	 * @var string
	 */
	private static $SubmitQuiz = "submit";

	/** 
	 * @var string
	 */
	private static $SendMessage = "sendMessage";

	/** 
	 * @var string
	 */
	private static $RemoveQuiz = "removeQuiz";

	/** 
	 * @var string
	 */
	private static $QuizDone = "quizDone";

	/** 
	 * @return boolean
	 */
	public function userChecksGroup() {
		return $this->userGoesToCreateGroup() || $this->userRemovesGroups();

	}

	/** 
	 * @return boolean
	 */
	public function userChecksQuiz() {
		return $this->userCreateQuiz() || 
			   $this->userAddQuiz() || 
			   $this->userPublishQuiz() || 
			   $this->userEditQuiz() || 
			   $this->userRemoveQuiz();

	}

	/** 
	 * @return boolean
	 */
	public function userRemoveQuiz() {
		return isset($_GET[self::$RemoveQuiz]);
	}

	/** 
	 * @var string
	 */
	public function getRemoveQuiz() {
		return self::$RemoveQuiz;
	}

	/** 
	 * @return  string
	 */
	public function getRemoveQuizId() {
		return $_GET[self::$RemoveQuiz];
	}


	/** 
	 * @return boolean
	 */
	public function userGoesToCreateGroup() {
		return isset($_GET[self::$createGroup]);
	}

	/** 
	 * @return string
	 */
	public function getCreateGroup() {
		return self::$createGroup;
	}

	/** 
	 * @return boolean
	 */
	public function userRemovesGroups() {
		return isset($_GET[self::$remove]);
	}

	/** 
	 * @return string
	 */
	public function getRemoveGroups() {
		return self::$remove;
	}

	/** 
	 * @return boolean
	 */
	public function userAddQuiz() {
		return isset($_GET[self::$addQuiz]);
	}

	/** 
	 * @return string
	 */	
	public function getAddQuiz() {
		return self::$addQuiz;
	}

	/** 
	 * @return boolean
	 */
	public function userAddQuestion() {
		return isset($_GET[self::$Question]);
	}

	/** 
	 * @return string
	 */
	public function getAddQuestion() {
		return self::$AddQuestion;
	}

	/** 
	 * @return boolean
	 */
	public function addQuestion() {
		return isset($_GET[self::$AddQuestion]);
	}

	/** 
	 * @return string
	 */
	public function getPublishQuiz() {
		return self::$Publish;
	}

	/** 
	 * @return string
	 */
	public function getPublishQuizId() {
		return $_GET[self::$Publish];
	}

	/** 
	 * @return string
	 */
	public function getEditQuiz() {
		return self::$Edit;
	}

	/** 
	 * @return string
	 */
	public function getEditQuizId() {
		return $_GET[self::$Edit];
	}

	/** 
	 * @var string
	 */
	public function getRemoveQuestions() {
		return self::$RemoveQuestion;
	}

	/** 
	 * @var boolean
	 */
	public function removeQuestion() {
		return isset($_GET[self::$RemoveQuestion]);
	}

	/** 
	 * @return boolean
	 */
	public function publishQuiz() {
		return isset($_GET[self::$Publish]);
	}

	/** 
	 * @return boolean
	 */
	public function editQuiz() {
		return isset($_GET[self::$Edit]);
	}

	/** 
	 * @var string
	 */
	public function getEditQuizCredentials() {
		return self::$EditQuizCredentials;	
	}

	/** 
	 * @var boolean
	 */
	public function editQuizCredentials() {
		return isset($_GET[self::$EditQuizCredentials]);
	}

	/** 
	 * @return boolean
	 */
	public function addEditQuestion() {
		return isset($_GET[self::$AddEditQuestion]);
	}

	/** 
	 * @var string
	 */
	public function getAddEditQuestion() {
		return self::$AddEditQuestion;
	}

	/** 
	 * @var string
	 */
	public function getEditQuestion() {
		return self::$EditQuestion;
	}

	public function getEditQuestionId() {
		return $_GET[self::$EditQuestion];
	}

	/** 
	 * @var boolean
	 */
	public function editQuestion() {
		return isset($_GET[self::$EditQuestion]);
	}

	/** 
	 * @param  integer $questionPk 
	 */
	public function redirectToEditQuestion($questionPk) {
		header("Location: index.php?" . self::$EditQuestion . "=$questionPk");
	}

	/** 
	 * @var boolean
	 */
	public function checksResults() {
		return $this->checksQuizResult() || $this->userPublishResult() || $this->userUnPublishResult();
	}

	/** 
	 * @var boolean
	 */
	public function checksQuizResult() {
		return isset($_GET[self::$QuizResult]);
	}

	/** 
	 * @var string
	 */
	public function getQuizResult() {
		return self::$QuizResult;
	}

	/** 
	 * @return string
	 */
	public function getQuizResultId() {
		return $_GET[self::$QuizResult];
	}

	/** 
	 * @var boolean
	 */
	public function userPublishResult() {
		return isset($_GET[self::$PublishResult]);
	}

	/** 
	 * @var string
	 */
	public function getPublishResult() {
		return self::$PublishResult;
	}

	public function getPublishId() {
		return $_GET[self::$PublishResult];
	}

	/** 
	 * @var boolean
	 */
	public function userUnPublishResult() {
		return isset($_GET[self::$UnpublishResultResult]);
	}

	/** 
	 * @var string
	 */
	public function getUnpublishResultResult() {
		return self::$UnpublishResultResult;
	}

	/** 
	 * @return string
	 */
	public function getUnpublishResultId() {
		return $_GET[self::$UnpublishResultResult];
	}

	/** 
	 * @var boolean
	 */
	public function sendMessage() {
		return isset($_GET[self::$SendMessage]);
	}

	/** 
	 * @var string
	 */
	public function getSendMessage() {
		return self::$SendMessage;
	}

	/** 
	 * @var boolean
	 */
	public function quizDone() {
		return isset($_GET[self::$QuizDone]);
	}

	public function quizDoneRedirect() {
		header("Location: index.php?" . self::$QuizDone);
	}

	/** 
	 * @return boolean
	 */
	public function userCreateQuiz() {
		return isset($_GET[self::$newQuiz]) || 
			   $this->userAddQuiz() || 
			   $this->userAddQuestion() || 
			   $this->addQuestion();
	}

	/** 
	 * @var boolean
	 */
	public function userPublishQuiz() {	
		return $this->publishQuiz();
	}

	/** 
	 * @var string
	 */	
	public function getSaveEdditedQuestion() {
		return self::$SaveEdditedQuestion;
	}

	/** 
	 * @return string
	 */
	public function getSavedEdditedQuestionId() {
		return $_GET[self::$SaveEdditedQuestion];
	}

	/** 
	 * @var boolean
	 */
	public function saveEdditedQuestion() {

		return isset($_GET[self::$SaveEdditedQuestion]);
	}

	/** 
	 * @var boolean
	 */
	public function userEditQuiz() {
		return $this->editQuiz() || 
			   $this->editQuizCredentials() || 
			   $this->removeQuestion() ||
			   $this->addEditQuestion() || 
			   $this->editQuestion() || 
			   $this->saveEdditedQuestion();
	}

	/** 
	 * @return string
	 */
	public function getCreateQuizLink() {
		return "<a href='?" . self::$newQuiz. " '>Skapa ny quiz</a>";
	}

	public function reloadToQuestion() {
		header("Location: index.php?" . self::$Question);
	}

	public function redirectToEditQuiz() {
		header("Location: index.php?" . self::$Edit);
	}

	/** 
	 * @var string
	 */
	public function getFollowGroup() {
		return self::$FollowGroup;
	}

	/** 
	 * @var boolean
	 */
	public function FollowGroup() {
		return isset($_GET[self::$FollowGroup]);
	}

	/** 
	 * @var boolean
	 */
	public function studentChecksGroups() {
		return $this->FollowGroup();
	}

	/** 
	 * @var string
	 */
	public function getDoQuiz() {
		return self::$Quiz;
	}

	/** 
	 * @var boolean
	 */
	public function doQuiz() {
		return isset($_GET[self::$Quiz]);
	}

	public function getDoQuizId() {
		return $_GET[self::$Quiz];
	}

	/** 
	 * @var boolean
	 */
	public function studentChecksQuizzes() {
		return $this->doQuiz() || $this->submitQuiz();
	}

	/** 
	 * @var string
	 */
	public function getSubmitQuiz() {
		return self::$SubmitQuiz;
	}

	/** 
	 * @var boolean
	 */
	public function submitQuiz() {
		return isset($_GET[self::$SubmitQuiz]);
	}
}
