<?php

namespace quiz\view;

require_once("./common/model/LengthException.php");
require_once("./common/model/QuestionEmptyException.php");
require_once("./common/model/QuestionLengthException.php");
require_once("./common/model/AnswerCountException.php");
require_once("./common/model/AnswerLengthException.php");

class NewQuestionView {

	/** 
	 * @var string
	 */
	private static $Answer = "NewQuestionView::Answer";

	/** 
	 * @var string
	 */
	private static $Question = "NewQuestionView::Question";

	/** 
	 * @var string
	 */
	private static $IsCorrect = "NewQuestionView::IsCorrect";

	/** 
	 * @var string
	 */
	private static $Description = "NewQuestionView::Description";

	/** 
	 * @var string
	 */
	private static $MoreQuestions = "NewQuestionView::MoreQuestions";

	private $message = "";

	/** 
	 * @var integer
	 */
	private static $MaxQuestion = 4;

	/** 
	 * @param quiz\view\Navigation $quizNavigaiton
	 */
	public function __construct(Navigation $quizNavigaiton) {

		$this->quizNavigation = $quizNavigaiton;
	}

	/** 
	 * @param  quizmodelQuestionCredentials $question 
	 * @return string HTML                                 
	 */
	public function getEditQuestionForm(\quiz\model\QuestionCredentials $question) {

		$saveEdditedQuestion= $this->quizNavigation->getSaveEdditedQuestion();
		$questionPk = $question->getPk();
		$action = "$saveEdditedQuestion=$questionPk";
		$legend = "Redigera fråga";
		$btnText = "Ändra";

		return $this->questionForm($action, $legend, $question, $btnText, NULL);
	}

	/** 
	 * @return  string HTML
	 */
	public function getAddQuestionForm() {

		$addEditQuestion = $this->quizNavigation->getAddEditQuestion();
		$legend = "Lägg till ny en ny fråga";
		$btnText = "Lägg till";

		return $this->questionForm($addEditQuestion, $legend, NULL, $btnText, NULL);
	}

	/** 
	 * @return string HTML
	 */
	public function getQuestionForm() {

		$addQuestion = $this->quizNavigation->getAddQuestion();
		$legend = "Lägg till ny en ny fråga";
		$btnText = "Fler frågor";
		$extraBtn = "<input type='submit' value='Klar' type='button' class='btn btn-primary' />";

		return $this->questionForm($addQuestion, $legend, NULL, $btnText, $extraBtn);
	}

	/** 
	 * @param  string $action   
	 * @param  string $legend   
	 * @param  \quiz\model\QuestionCredentials $question 
	 * @param  string $btnText  
	 * @param  string $extraBtn 
	 * @return string HTML         
	 */
	private function questionForm($action, $legend, \quiz\model\QuestionCredentials $question = NULL, $btnText, $extraBtn = NULL) {

		$html = "<div class='row shadow'>
					<div class='col-md-6 col-md-offset-3'>
						<form method='post' action='?$action' enctype='multipart/form-data' data-validate='parsley' >
					<fieldset>
						<legend>
							$legend
						</legend>";

		$html .= $this->getFormContent($question);	

		$html .= "<div class='pull-right'><input name='" . self::$MoreQuestions . "' 
						type='submit' value='$btnText' type='button' class='btn btn-primary'/>
					$extraBtn</div>
				</fieldset>
						</form>
					</div>
				</div>";

		return $html;
	}

	/** 
	 * @param  \quiz\model\QuestionCredentials $question 
	 * @return string HTML
	 */
	private function getFormContent(\quiz\model\QuestionCredentials $question = NULL) {

		$questionValue = $this->getQuestion();
		$descriptionValue = $this->getDescription();
		$answers = NULL;
		try {
			$answers = $this->getAnswers();
		} catch (\Exception $exception) {}
		if (isset($question)) {
			$questionValue = $question->getQuestion();
			$descriptionValue = $question->getDescription();
			$answers = $question->getAnswers();
		}

		$html = "<div class='error'>$this->message</div>
				<div class='form-group'>
					<label for='question' class='col-lg-2 control-label' >Fråga:</label>
					<div class='col-lg-10'>
						<input type='text' name='" . self::$Question .
							"' id=''  maxlength='255' value='$questionValue' 
							type='text' class='form-control' id='question' placeholder='Fråga'
							data-validation-minlength='0' data-required-message='Du måste ange en fråga' 
							maxlength='16' data-trigger='focusout' data-required='true' />
					</div>	
				</div>
				<div class='form-group'>		
					<label for='description' class='col-lg-2 control-label' >Förklarande text:</label>
					<div class='col-lg-10'>
						<input type='text' name='" . self::$Description .
							"' id=''  maxlength='255'  value='$descriptionValue'
							type='text' class='form-control' id='description' placeholder='Förklarande text' />
					</div>	
				</div>";

		return $html .= $this->getAnswerForm($answers);
	}

	/** 
	 * @param  array of \quiz\model\AnswerCredentials $answers 
	 * @return string HTML 
	 */
	private function getAnswerForm($answers) {

		$html = "<table class='table table-hover'>";

		$html .= "<tr><th>Svarsalternativ:</th><th>Rätt svar:</th></tr>";

		for ($i = 0; $i < self::$MaxQuestion; $i++) {

			$value = "";
			$checked = "";
			if (isset($answers) && isset($answers[$i])) {

				if ($answers[$i]->getAnswer()) {
					$value =  $answers[$i]->getAnswer();
					$isCorrect = $answers[$i]->getIsCorrect();
					if ($isCorrect) {	
						$checked = "checked='$isCorrect'";
					}
				}
			}

			$html .= "<tr><td><input type='text' name='" . self::$Answer .  
							"$i' id='$i'  maxlength='150'  value='$value'";

			if ($i < 2) {
				$html .= "data-validation-minlength='0' data-required-message='Du måste ange minst två svar' 
							maxlength='16' data-trigger='focusout' data-required='true'";
			}

			$html .= "/></td><td><input type='checkbox' id='' value='1' name='" . self::$IsCorrect . "$i' $checked /></td></tr>";							
		}
		return $html .= "</table>";
	}

	/** 
	 * @return \quiz\model\QuestionCredentials
	 * @throws If invalid input for question
	 */
	public function getQuestionCredentials() {

		$answers = $this->getAnswers();

		try {
			return new \quiz\model\QuestionCredentials(NULL, NULL, $this->getQuestion(), $this->getDescription(), $answers);
		} catch (\common\model\QuestionEmptyException $exception) {
			$this->message .= "<p>Fråga saknas</p>";
		} catch (\common\model\QuestionLengthException $exception) {
			$length = $exception->getLength();
			$this->message .= "<p>Frågan får max vara " . $length . " tecken</p>"; 
		} catch (\common\model\TextLengthException $exception) {
			$length = $exception->getLength();
			$this->message .= "<p>Texten får max vara " . $length . " tecken</p>";
		} catch (\common\model\AnswerCountException $exception) {
			$length = $exception->getLength();
			$this->message .= "<p>Det behövs minst  " . $length  . " svar</p>"; 
		}
		throw new \Exception();
	}

	/** 
	 * @return string 
	 */
	private function getQuestion() {

		if (isset($_POST[self::$Question])) {
			return $this->sanitize($_POST[self::$Question]);
		}
		return "";
	}

	/** 
	 * @return string 
	 */
	private function getDescription() {

		if (isset($_POST[self::$Description])) {
			return $this->sanitize($_POST[self::$Description]);
		}
		return "";
	}

	/** 
	 * @return array of \quiz\model\AnswerCredentials
	 * @throws  If invalid answer format
	 */
	private function getAnswers() {

		$answers = array();

		for ($i = 0; $i < self::$MaxQuestion; $i++) {

			if (isset($_POST[self::$Answer . $i])) {
				$answer = $this->getAnswer($i);
				
				$isCorrect = $this->getIsCorrect($i);
				
				try {
					$answers[] = new \quiz\model\AnswerCredentials(NULL, NULL, $answer, $isCorrect);
				} catch (\common\model\AnswerLengthException $exception) {
					$maxLength = $exception->getLength();
					$this->message = "Ett svar får max vara $maxLength tecken";
					throw new \common\model\AnswerLengthException($maxLength);
				} catch (\Exception $exception) {}
			}
		}	
		return $answers;
	}

	/** 
	 * @param  integer $id
	 * @return string   
	 */
	private function getAnswer($id) {

		if (isset($_POST[self::$Answer . $id])) {
			return $this->sanitize($_POST[self::$Answer . $id]);
		}
		return "";
	}

	/** 
	 * @param  integer $id 
	 * @return boolean     
	 */
	private function getIsCorrect($id) {

		return isset($_POST[self::$IsCorrect . $id]);
	}

	/** 
	 * @param  string $input 
	 * @return string        
	 */
	private function sanitize($input) {
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}

	/** 
	 * @return boolean
	 */
	public function addMoreQuestions() {
		
		return isset($_POST[self::$MoreQuestions]);
	}

	/** 
	 * @return string
	 */
	public function getQuestionPk() {

		if ($this->quizNavigation->saveEdditedQuestion()) {
			return $this->quizNavigation->getSavedEdditedQuestionId();
		}
		return "";
	}
}

