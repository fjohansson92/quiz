<?php

namespace quiz\view;

class GroupPicker {

	/** 
	 * @var string
	 */
	private static $PickedGroups = "GroupView::PickedGroups";

	/** 
	 * @var string
	 */
	private static $GroupName = "NewQuizView::GroupName";

	/** 
	 * @param  array of \quiz\model\GroupCredentials $groupList        
	 * @param  boolean  $editable         
	 * @param  boolean $checkedBox       
	 * @param  boolean $tableHead        
	 * @param  boolean $radio            
	 * @param  integer  $defaultGroupName 
	 * @return String HTML                  
	 */
	protected function getGroupList($groupList, $editable, $checkedBox = false, $tableHead = true, $radio = false, $defaultGroupName = null) {

		$html = "";
		if ($tableHead){
			$html .= "<thead><tr>
						<th>Gruppnamn</th>
						<th>Gruppskapare</th>";
			$html .= $editable ? "<th>Välj grupp</th>" : "";			
			$html .= "</tr></thead>";
		}

		foreach($groupList as $group) {

			$groupName = $group->getGroupName();
			$teacher = $group->getTeacher();
			$fName = $teacher->getFName();
			$lName = $teacher->getLName();
			$groupId = $group->getPk();

			$html .= "<tr><div>
							<td>$groupName</td>
							<td>$fName $lName</td>";
			if($editable) {

				$checked = $checkedBox ? "checked='checked'" : "";
				$html .= "<td><input type='checkbox' id='$groupName' $checked value='$groupId'  name='" . self::$PickedGroups ."[]' /></td>";
			}else if ($radio) {

				$checked = $groupId == $defaultGroupName ? "checked='checked'" : "";
				$html .= "<td><input type='radio' id='$groupName' value='$groupId' data-required='true'  
							$checked name='". self::$PickedGroups ."[]' /></td>";
			}
			$html .= "</div></tr>";		
		}

		return $html;
	}

	/** 
	 * @param array of \quiz\model\GroupCredentials $paramname 
	 * @return string
	 * @throws If no group is picked
	 */
	protected function getGroupPk($groups) {

		$selectedGroups = $this->getGroups($groups);
		if (!empty($selectedGroups)){	
			return $selectedGroups[0];
		}
		throw new \Exception();
	}

	/** 
	 * @param  array of \quiz\model\GroupCredentials $groups 
	 * @return array integer
	 */
	public function getGroups($groups) {

		$checked = array();
		if (!empty($_POST[self::$PickedGroups])) {
			foreach ($_POST[self::$PickedGroups] as $group) {

				foreach ($groups as $savedGroup) {
					if ($group == $savedGroup->getPk()) {
						$checked[] = $group;
					}
				}
			} 
		}
		return $checked;
	}

}