<?php

namespace quiz\view;

class QuestionListView {

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var array of \quiz\model\QuestionCredentials
	 */
	private $questions;

	/** 
	 * @var string
	 */
	private $editErrorMessage = "";

	/** 
	 * @var string
	 */
	private static $CheckedQuestion = "QuestionListView::QuestionToRemove";

	/** 
	 * @param \quiz\view\Navigation $quizNavigation
	 * @param array of \quiz\model\QuestionCredentials $questions     
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation, $questions) {

		$this->quizNavigation = $quizNavigation;
		$this->questions = $questions;
	}

	/** 
	 * @return string HTMl
	 */
	public function getEditQuestions() {

		$removeQuestions = $this->quizNavigation->getRemoveQuestions();

		$html = "<div class='row shadow questionList'><div class='col-md-6 col-md-offset-3'>";
		$html .= "<form method='post' action='?$removeQuestions'
					 enctype='multipart/form-data' >
					<fieldset>
						<legend>
							Redigera provets frågor
						</legend>
						<div class='error'>$this->editErrorMessage</div>";

		$html .= $this->listQuestions();			

		$html .= "<input type='submit' value='Ta bort' type='button' class='btn btn-primary pull-right'/> 
					</fieldset>
				</form>";	
		$html .= "</div></div>";

		return $html;
	}

	/** 
	 * @return string
	 */
	private function listQuestions() {

		if (empty($this->questions)) {
			return "<p>Provet har inga frågor.</p>";
		}


		$editQuestion = $this->quizNavigation->getEditQuestion();

		$html = "<table class='table'><thead>
				<tr>
					<th>Fråga</th>
					<th>Ta bort</th>
				</tr>
				</thead><tbody>";

		foreach ($this->questions as $question) {

			$html .="<tr>";
	
			$questionTitel = $question->getQuestion();
			$questionPk = $question->getPk();

			$html .= "<td><a href='?editQuestion=$questionPk' >$questionTitel</a></td>
						<td><input type='checkbox' id='$questionPk' value='1' name='" . self::$CheckedQuestion . "$questionPk' /></td>
					</tr>";	

		}
		$html .= "</tbody></table>";
		return $html;
	}

	public function editQuesitonFailed() {
		$this->editErrorMessage = "<p>Det gick inte att hitta vald fråga.</p>";
	}

	/** 
	 * @return string
	 * @throws If no quesiton selected to be removed
	 */
	public function getQuestionsToRemove() {

		$questionsToRemove = array();

		foreach($this->questions as $question){
			
			if(isset($_POST[self::$CheckedQuestion . $question->getPk()])) {
				$questionsToRemove[] = $question;
			}
		}

		if (empty($questionsToRemove)) {
			$this->editErrorMessage = "<p>Kryssa i rutorna för de grupper du vill ta bort";
			throw new \Exception();
		}
		return $questionsToRemove;
	}

	/** 
	 * @return \quiz\model\QuestionCredentials
	 * @throws If question wasn't found
	 */
	public function getQuestion() {

		$questionPk = $this->quizNavigation->getEditQuestionId();

		foreach ($this->questions as $question) {

			if ($question->getPk() == $questionPk) {
				return $question;
			}
		}
		throw new \Exception();
	}

}