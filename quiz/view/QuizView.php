<?php

namespace quiz\view;

class QuizView {

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var string
	 */
	private $message = "";

	/** 
	 * @var string
	 */
	private $errorMessage = "";

	/**
	 * @var string
	 */
	private $removeMessage = "";

	/** 
	 * @var string
	 */
	private $removeErrorMessage = "";

	/** 
	 * @var string
	 */
	private $doQuizMessage = "";

	/** 
	 * @param \quiz\view\Navigation $quizNavigation
	 */
	public function __construct(\quiz\view\Navigation $quizNavigation) {

		$this->quizNavigation = $quizNavigation;
	}


	/** 
	 * @param  array of \quiz\model\QuizCredentials $quizzes          
	 * @param  array of \quiz\model\QuizCredentials $publishedQuizzes 
	 * @return string
	 */
	public function getQuizPage($quizzes, $publishedQuizzes) {

		$html = "<h2>";
		$html .= $this->quizNavigation->getCreateQuizLink();
		$html .= "</h2>";

		$html .= "<div class=''>";
		$html .= "<h3>Sparade prov</h3><div class='success'>$this->message</div><div class='error'>$this->errorMessage</div>";
		$html .= $this->displayPublished($quizzes, false);

		$html .= "<h3>Publicerade prov</h3><div class='success'>$this->removeMessage</div><div class='error'>$this->removeErrorMessage</div>";
		$html .= $this->displayPublished($publishedQuizzes, true);
		$html .= "</div>";

		return $html;
	}

	/** 
	 * @param array of \quiz\model\QuizCredentials $quizzes   
	 * @param  boolean $published 
	 * @return string           
	 */
	private function displayPublished($quizzes, $published) {

		if (empty($quizzes)) {
			return "<p>Det finns inga prov</p>";
		}

		$html = "<div class='quizzes shadow'><table class='table'><thead><tr>";
		$html .= "<th>Provtitel</th>";
		$html .= $published ? "<th>Ta bort för studenter</th>" : "<th>Publicera för studenter</th>";
		$html .= "</tr></thead>";

		$html .= "<tbody";
		foreach ($quizzes as $quiz) {
			$html .= "<tr>";
	
			$id = $quiz->getPk();
			$titel = $quiz->getTitel();

			if ($published) {

				$html .= "<td>$titel</td>";

				$link  = $this->quizNavigation->getRemoveQuiz();
				$html .= "<td><a href='?$link=$id'>Ta bort</a></td>";
			} else {
				$publish = $this->quizNavigation->getPublishQuiz();
				$edit = $this->quizNavigation->getEditQuiz();

				$html .= "<td><a href='?$edit=$id'>$titel</a></td>
						  <td><a href='?$publish=$id'>Publicera</a></td>";	
			}

			$html .= "</tr>";
		}
		$html .= "<tbody></table></div>";

		return $html;
	}

	/** 
	 * @param  array of \quiz\model\QuizCredentials $quizzes  
	 * @param  array of \user\model\UserCredentials $teachers 
	 * @param  array of \quiz\model\GroupCredentials $groups   
	 * @return string         
	 */
	public function getStudentQuizPage($quizzes, $teachers, $groups) {

		$html = "<div class='row shadow'><div class='quizzes'><div class='error'>$this->doQuizMessage</div>";
		$html .= $this->displayQuizzes($quizzes, $teachers, $groups);
		$html .= "</div></div>";
		return $html;

	} 

	/** 
	 * @param  array of \quiz\model\QuizCredentials $quizzes  
	 * @param  array of \user\model\UserCredentials $teachers 
	 * @param  array of \quiz\model\GroupCredentials $groups   
	 * @return string         
	 */
	private function displayQuizzes($quizzes, $teachers, $groups) {
		if (empty($quizzes)) {
			return "<p>Det finns inga prov</p>";
		}
		$html = "<table class='table quizzes'>
					<th>Provtitel</th>
					<th>Grupp</th>
					<th>Deadline</th>
					<th>Prov skapare</th>";
		foreach ($quizzes as $quiz) {
			$html .= "<tr><div>";

			$titel = $quiz->getTitel();
			$id = $quiz->getPk();
			$quizCreatorFk = $quiz->getQuizCreatorFk();
			$groupNameFk = $quiz->getGroupNameFk();
			$endDate = $quiz->getEndDate();

			$quiz = $this->quizNavigation->getDoQuiz();
			$html .= "<td><a href='?$quiz=$id'>$titel</a></td>";
			$groupName = "Gruppnamn saknas";
			
			foreach ($groups as $group) {
				if ($group->getPk() == $groupNameFk) {
					$groupName = $group->getGroupName();
				}
			}
			$html .= "<td><p>$groupName</p></td>
						<td><p>$endDate</p></td>";
			$creator = "Skapare saknas";
			foreach ($teachers as $teacher) {
				if ($teacher->getPk() == $quizCreatorFk) {
					$creator = $teacher->getFName() . " " . $teacher->getLName();
				}
			}
			$html .= "<td><p>$creator</p></td>
						</tr></div>";
		}
		$html .= "</table>";
		return $html;
	}

	/** 
	 * @return integer
	 */
	public function getRemoveId() {

		if ($this->quizNavigation->userRemoveQuiz()) {

			return $this->quizNavigation->getRemoveQuizId();
		}
		return "";
	}

	public function removeFailed() {
		$this->removeErrorMessage = "<p>Det gick inte att ta bort provet</p>";
	}

	/** 
	 * @return integer
	 * @throws If no quiz was selected
	 */
	public function getPublishId() {

		if ($this->quizNavigation->publishQuiz()) {

			return $this->quizNavigation->getPublishQuizId();
		}
		throw new \Exception();
	}

	public function publishFailed() {
		$this->errorMessage = "<p>Det gick inte att publicera provet. Kontrollera provets uppgifter.</p>";
	}

	/** 
	 * @return integer
	 */
	public function getEditId() {

		if ($this->quizNavigation->editQuiz()) {

			return $this->quizNavigation->getEditQuizId();
		}
		return "";
	}

	public function editFailed() {
		$this->errorMessage = "<p>Det gick inte att redigera provet.</p>";
	}

	/** 
	 * @return integer
	 */
	public function getDoQuizId() {

		if ($this->quizNavigation->doQuiz()) {

			return $this->quizNavigation->getDoQuizId();
		} 
		return "";
	}

	public function doQuizFailed() {
		$this->doQuizMessage = "<p>Det gick inte att hitta provet</p>";
	}


	public function publishSuccess() {
		$this->message = "<p>Provet publicerades.</p>";
	}

	public function removeSuccess() {
		$this->removeMessage = "<p>Provet togs bort.</p>";
	}

}