<?php

namespace quiz\controller;

require_once("./quiz/model/OwnResultModel.php");

class OwnResultController {

	/** 
	 * @var \quiz\model\OwnResultModel
	 */
	private $ownResultModel;

	/** 
	 * @var \quiz\view\ResultView
	 */
	private $resultView;

	/** 
	 * @param mysqli  $mysqli         
	 * @param \quiz\view\Navigation $quizNavigation 
	 * @param integer $userDataPk     
	 */
	public function __construct(\mysqli $mysqli, \quiz\view\Navigation $quizNavigation, $userDataPk) {

		$this->ownResultModel = new \quiz\model\OwnResultModel($mysqli, $userDataPk);
		$this->resultView = new \quiz\view\ResultView($quizNavigation);
	}
 
 	/** 
 	 * @return string HTML
 	 */
	public function doOwnResults() {

		$result = $this->ownResultModel->getResults();
		$quizzes = $this->ownResultModel->getQuizzes($result);

		return $this->resultView->listOwnResults($result, $quizzes);
	}
}