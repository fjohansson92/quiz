<?php

namespace quiz\controller;

require_once("./quiz/view/QuizView.php");
require_once("./quiz/controller/NewQuizController.php");
require_once("./quiz/model/QuizCredentials.php");
require_once("./quiz/model/QuizModel.php");
require_once("./quiz/controller/EditQuizController.php");
require_once("./quiz/controller/DoQuizController.php");

class QuizController {

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @var \qiuz\view\Navgigation
	 */
	private $quizNavigation;

	/** 
	 * @var integer
	 */
	private $userDataPk;

	/** 
	 * @var \quiz\view\QuizView
	 */
	private $quizView;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @var \quiz\model\QuizModel
	 */
	private $quizModel;

	/** 
	 * @param \mysqli  $mysqli 
	 * @param \quiz\view\Navigation $quizNavigation 
	 * @param \user\model\UserCredentials $userCredentials
	 */
	public function __construct(\mysqli $mysqli, 
								\quiz\view\Navigation $quizNavigation, 
								\user\model\UserCredentials $userCredentials) {

		$this->mysqli = $mysqli;
		$this->quizNavigation = $quizNavigation;
		$this->userDataPk = $userCredentials->getPk();
		$this->userCredentials = $userCredentials;

		$this->quizModel = new \quiz\model\QuizModel($mysqli, $this->userDataPk);
		$this->quizView = new \quiz\view\QuizView($this->quizNavigation);
	}

	/** 
	 * @return string HTML
	 */
	public function doTeacherQuiz() {

		if ($this->quizNavigation->userCreateQuiz()) {
			try {
				$newQuizController = new \quiz\controller\NewQuizController($this->mysqli, $this->quizNavigation, $this->userCredentials);
				return $newQuizController->doNewQuiz();
			} catch (\Exception $exception) {}	
		} else if ($this->quizNavigation->userPublishQuiz()) {
			try {
				$quizId = $this->quizView->getPublishId(); 
				$this->quizModel->publishQuiz($quizId);
				$this->quizView->publishSuccess();
			} catch (\Exception $exception) {
				$this->quizView->publishFailed();
			}
		} else if ($this->quizNavigation->userRemoveQuiz()) {
			try {
				$quizId = $this->quizView->getRemoveId();
				$this->quizModel->removePublishedQuiz($quizId);
				$this->quizView->removeSuccess();
			} catch (\Exception $exception) {
				$this->quizView->removeFailed();
			}
		} else if ($this->quizNavigation->userEditQuiz()) {
			try {
				$quizId = $this->quizView->getEditId();
				$this->quizModel->saveEditId($quizId);
				$quizId = $this->quizModel->getEditId();

				$editQuizController = new \quiz\controller\EditQuizController($this->mysqli, $this->quizNavigation, 
																			  $this->userCredentials, $quizId); 
				return $editQuizController->editQuiz();		
			} catch (\Exception $exception) { 
				$this->quizView->editFailed();	
			}
		}
		$myQuizzes = $this->quizModel->getQuizzes(); 
		$publishedQuizzes = $this->quizModel->getPublishedQuizzes();
		return $this->quizView->getQuizPage($myQuizzes, $publishedQuizzes);
	}

	/** 
	 * @return string HTML
	 */
	public function doStudentQuiz() {
		
		$doQuizController = new \quiz\model\DoQuizController($this->mysqli, $this->quizNavigation, $this->userDataPk);

		if ($this->quizNavigation->doQuiz()) {
			try {
				$quizId = $this->quizView->getDoQuizId();
				return $doQuizController->doQuiz($quizId);
			} catch (\Exception $exception) {
				$this->quizView->doQuizFailed();
			}	
		} else if ($this->quizNavigation->submitQuiz()) {

			return $doQuizController->doResult();
		}

		$users = new \user\model\Users($this->mysqli);
		$teachers = $users->getEveryUserFromType(\user\model\UserType::TEACHER);

		$groupModel = new \quiz\model\GroupModel($this->mysqli, $this->userCredentials);	
		$groups = $groupModel->getAllGroups();

		$availableQuizzes = $this->quizModel->getStudentQuizzes();
		return $this->quizView->getStudentQuizPage($availableQuizzes, $teachers, $groups);
	}
}