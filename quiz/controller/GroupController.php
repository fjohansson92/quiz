<?php

namespace quiz\controller;

require_once("./quiz/model/GroupModel.php");
require_once("./quiz/view/GroupView.php");
require_once("./quiz/model/GroupCredentials.php");;
require_once("./quiz/model/GroupFollowCred.php");

class GroupController {

	/** 
	 * @var \quiz\model\GroupModel
	 */
	private $groupModel;

	/**
	 * @var \quiz\view\GroupView
	 */
	private $groupView;

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var \user\view\Navigation
	 */
	private $navigation;

	/** 
	 * @param \mysqli $mysqli
	 * @param \user\view\TeacherNavigation $navigation
	 * @param \quiz\view\Navigation $quizNavigation
	 * @param \user\model\UserCredentials $userCredentials
	 */
	public function __construct(\mysqli $mysqli, 
								\user\view\Navigation $navigation, 
								\quiz\view\Navigation $quizNavigation, 
								\user\model\UserCredentials $userCredentials) {

		$this->navigation = $navigation;
		$this->groupModel = new \quiz\model\GroupModel($mysqli, $userCredentials);	
		$this->quizNavigation = $quizNavigation;
	}

	/** 
	 * @return string HTML
	 */
	public function doTeacherGroup() {

		$myGroups = $this->groupModel->getMyGroups();
		$otherGroups = $this->groupModel->getOtherGroups();
		$this->groupView = new \quiz\view\GroupView($this->quizNavigation, $myGroups, $otherGroups);

		if ($this->quizNavigation->userGoesToCreateGroup()) {
			
			try{

				$groupName = $this->groupView->getGroupName();
				$this->groupModel->addGroup($groupName, $this->groupView);
				$this->navigation->reloadToGroupPage();
			} catch(\common\model\TitleEmptyException $exception) {

				$this->groupView->groupNameEmptyFail();
			} catch(\common\model\TitleLengthException $exception) {

				$length = $exception->getLength();
				$this->groupView->groupNameLengthFail($length);
			} catch(\Exception $exception) {}
		} else if ($this->quizNavigation->userRemovesGroups()) {
			
			try{
				$groupsToRemove = $this->groupView->getPickedGroups();
				$this->groupModel->removeGroups($groupsToRemove);	
				$this->navigation->reloadToGroupPage();
			} catch(\Exception $exception) {
				$this->groupView->removeFailed();
			}	
		}
		
		return $this->groupView->getTeacherGroupPage();
	}

	/** 
	 * @return string HTML
	 */
	public function doStudentGroup() {

		$myGroups = $this->groupModel->getFollowedGroups();
		$otherGroups = $this->groupModel->getNotFollowedGroups($myGroups);
		$this->groupView = new \quiz\view\GroupView($this->quizNavigation, $myGroups, $otherGroups);

		if ($this->quizNavigation->followGroup()) {

			$followedGroups = $this->groupView->getPickedGroups();
			$this->groupModel->saveFollowedGroups($followedGroups);
			$this->navigation->reloadAfterGroupEdit();
		}

		return $this->groupView->getStudentGroupPage();
	}
}