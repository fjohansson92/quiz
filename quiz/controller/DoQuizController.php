<?php

namespace quiz\model;

require_once("./quiz/model/DoQuizModel.php");
require_once("./quiz/view/DoQuizView.php");
require_once("./quiz/view/ResultView.php");

class DoQuizController {

	/** 
	 * @var \quiz\model\DoQuizModel
	 */
	private $doQuizModel;

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var \quiz\view\ResultView
	 */
	private $resultView;

	/** 
	 * @var \quiz\view\DoQuizView
	 */
	private $doQuizView;

	/** 
	 * @param mysqli $mysqli         
	 * @param \quiz\view\Navigation $quizNavigation 
	 * @param integer $userDataPk     
	 */
	public function __construct(\mysqli $mysqli, \quiz\view\Navigation $quizNavigation, $userDataPk) {

		$this->quizNavigation = $quizNavigation;

		$this->doQuizModel = new \quiz\model\DoQuizModel($mysqli, $userDataPk);
		$this->doQuizView = new \quiz\view\DoQuizView($this->quizNavigation);
		$this->resultView = new \quiz\view\ResultView($quizNavigation);
	}

	/** 
	 * @param integer $quizId
	 * @return string HTML
	 */
	public function doQuiz($quizId) {
		
		$this->doQuizModel->toggleQuiz($quizId);

		$quizCredentials = $this->doQuizModel->getQuizCredentials();
		$questions = $this->doQuizModel->getQuestions();

		return $this->doQuizView->getQuizForm($quizCredentials, $questions);
	}

	/** 
	 * @return string HTML
	 */
	public function doResult() {

		try {
			$guesses = $this->doQuizView->getGuesses();
			$results = $this->doQuizModel->getResult($guesses);
			$this->doQuizModel->saveResult($results);
			$questions = $this->doQuizModel->getQuestions();
		} catch (\Exception $exception) {
			$this->quizNavigation->quizDoneRedirect();
		}
		return $this->resultView->showResult($results, $questions);
	}

}
