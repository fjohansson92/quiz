<?php

namespace quiz\controller;

require_once("./quiz/view/NewQuizView.php");
require_once("./quiz/model/DateTime.php");
require_once("./quiz/model/NewQuizModel.php");
require_once("./quiz/view/NewQuestionView.php");
require_once("./quiz/model/AnswerCredentials.php");
require_once("./quiz/model/QuestionCredentials.php");

class NewQuizController {

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var \quiz\model\NewQuizModel
	 */
	private $newQuizModel;

	/** 
	 * @var \quiz\view\NewQuizView
	 */
	private $newQuizView;

	/** 
	 * @var \quiz\view\NewQuestionView
	 */
	private $newQuestionView;

	/** 
	 * @param \mysqli $mysqli
	 * @param \quiz\view\Navigation $quizNavigation
	 * @param \user\model\UserCredentials $userCredentials
	 */
	public function __construct(\mysqli $mysqli, 
								\quiz\view\Navigation $quizNavigation, 
								\user\model\UserCredentials $userCredentials) {

		$this->quizNavigation = $quizNavigation;

		$this->userDataPk = $userCredentials->getPk();
		$this->groupModel = new \quiz\model\GroupModel($mysqli, $userCredentials);	
		$groups = $this->groupModel->getAllGroups();

		$this->newQuizModel = new \quiz\model\NewQuizModel($mysqli);

		$this->newQuizView = new \quiz\view\NewQuizView($quizNavigation, $groups, $this->userDataPk);
		$this->newQuestionView = new \quiz\view\NewQuestionView($quizNavigation);
	}

	/** 
	 * @return string HTML
	 * @throws If there is no session id.
	 * @throws If quiz create is created
	 */
	public function doNewQuiz() {

		if ($this->quizNavigation->userAddQuiz()) {
			try {
				$newQuiz = $this->newQuizView->getNewQuiz();
				$this->newQuizModel->saveNewQuiz($newQuiz);
				$this->quizNavigation->reloadToQuestion();
			} catch (\Exception $exception){}
		} else if ($this->quizNavigation->userAddQuestion()) {

				if (!$this->newQuizModel->sessionQuizPkIsset()) {
					throw new \Exception();
				}
				return $this->newQuestionView->getQuestionForm();

		} else if ($this->quizNavigation->addQuestion()) {

			if ($this->newQuizModel->sessionQuizPkIsset()) {
				try {
					$newQuestion = $this->newQuestionView->getQuestionCredentials();
					$this->newQuizModel->saveNewQuestion($newQuestion);
				} catch (\Exception $exception) {
					if ($this->newQuestionView->addMoreQuestions()) {
						return $this->newQuestionView->getQuestionForm();
					}
				} 
				if ($this->newQuestionView->addMoreQuestions()) {
					$this->quizNavigation->reloadToQuestion();
				} else {
					$this->newQuizModel->removeSessionQuizPk();
					throw new \Exception();
				}
			} else {
				throw new \Exception();
			}
		}
		return $this->newQuizView->getQuizForm();
	}
}