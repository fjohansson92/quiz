<?php

namespace quiz\controller;

require_once("./quiz/model/ResultModel.php");
require_once("./quiz/view/QuizResultview.php");

class ResultController {

	/** 
	 * @var \quiz\model\ResultModel
	 */
	private $resultModel;

	/** 
	 * @var \quiz\view\ResultView
	 */
	private $resultView;

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @param mysqli $mysqli        
	 * @param \quiz\view\Navigation $quizNavigation
	 */
	public function __construct(\mysqli $mysqli, \quiz\view\Navigation $quizNavigation) {

		$this->resultModel = new \quiz\model\ResultModel($mysqli);
		$this->resultView = new \quiz\view\ResultView($quizNavigation);
		$this->quizNavigation = $quizNavigation;
	}

	/** 
	 * @return string HTML 
	 */
	public function doTeacherResults() {

		if ($this->quizNavigation->checksQuizResult()) {
			try {
				return $this->getResultCompilation(true);
			} catch (\Exception $exception) {
				$this->resultView->quizResultFailed();
			}

		} else if ($this->quizNavigation->userPublishResult()) {
			try {
				$statusId = $this->resultView->getResultStatusId();
				$this->resultModel->publishResult($statusId);		
				$this->resultView->publishSuccess();
			} catch (\Exception $exception) {
				$this->resultView->publishFailed();
			}
		} else if ($this->quizNavigation->userUnPublishResult()) {
			try {
				$statusId = $this->resultView->getUnPublishId();
				$this->resultModel->unPublishResult($statusId);
				$this->resultView->removeSuccess();
			} catch (\Exception $exception) {
				$this->resultView->unPublishFailed();
			}
		}

		$this->resultModel->sortResults();
		$privateResultsComp = $this->resultModel->getPrivateResultsComp();
		$openResultsComp = $this->resultModel->getOpenResultsComp();

		return $this->resultView->listTeacherResults($privateResultsComp, $openResultsComp);
	}

	/** 
	 * @return string HTML
	 */
	public function doStudentResults() {

		if ($this->quizNavigation->checksQuizResult()) {
			try {
				return $this->getResultCompilation(false);
			} catch (\Exception $exception) {
				$this->resultView->quizResultFailed();
			}
		}

		$this->resultModel->sortResults();
		$openResultsComp = $this->resultModel->getOpenResultsComp();
		return $this->resultView->listStudentResults($openResultsComp);
	}

	/** 
	 * @param boolean $isTeacher
	 * @return string HTML
	 */
	private function getResultCompilation($isTeacher) {

		$quizId = $this->resultView->getQuizId();
		$resultComp;
		if ($isTeacher) {
			$resultComp = $this->resultModel->getTeacherCompilation($quizId);
		} else {
			$resultComp = $this->resultModel->getStudentCompilation($quizId);
		}	
		$questionComps = $this->resultModel->getQuestionCompilations($resultComp);
		$quizResultView = new \quiz\view\QuizResultView();
		return $quizResultView->showQuizResult($resultComp, $questionComps);

	}
}				