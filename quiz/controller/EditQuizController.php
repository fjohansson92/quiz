<?php

namespace quiz\controller;

require_once("./quiz/model/EditQuizModel.php");
require_once("./quiz/view/QuestionListView.php");

class EditQuizController {

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var \quiz\model\QuizCredentials
	 */
	private $quizCredentials;

	/** 
	 * @var \quiz\model\EditQuizModel
	 */
	private $editQuizModel;

	/** 
	 * @var \quiz\view\NewQuizView
	 */
	private $newQuizView;

	/** 
	 * @var \quiz\view\QuestionListView
	 */
	private $questionListView;

	/** 
	 * @var \quiz\view\NewQuesitonView
	 */
	private $newQuestionView;

	/** 
	 * @param mysqli  $mysqli          
	 * @param \quiz\view\Navigation $quizNavigation  
	 * @param \user\model\UserCredentials $userCredentials 
	 * @param integer $id              
	 */
	public function __construct(\mysqli $mysqli, 
								\quiz\view\Navigation $quizNavigation,
								\user\model\UserCredentials $userCredentials, 
								$id) {

		$this->quizNavigation =  $quizNavigation;
		$userDataPk = $userCredentials->getPk();

		$this->groupModel = new \quiz\model\GroupModel($mysqli, $userCredentials);	
		$groups = $this->groupModel->getAllGroups();

		$this->editQuizModel = new \quiz\model\editQuizModel($mysqli, $userDataPk, $id);
		$this->quizCredentials = $this->editQuizModel->getQuizCredentials();
		$questions = $this->editQuizModel->getQuizQuestions();

		$users = new \user\model\Users($mysqli);
		$teachers = $users->getEveryUserFromType(\user\model\UserType::TEACHER);

		$this->questionListView = new \quiz\view\QuestionListView($quizNavigation, $questions);
		$this->newQuizView = new \quiz\view\NewQuizView($quizNavigation, $groups, $userDataPk, $teachers);
		$this->newQuestionView  = new \quiz\view\NewQuestionView($quizNavigation);
	}


	/** 
	 * @return string HTML
	 */
	public function editQuiz() {
		if ($this->quizNavigation->editQuizCredentials()) {
			try {
				$quizCredentials = $this->newQuizView->getNewQuiz();
				$this->editQuizModel->saveEditedQuiz($quizCredentials);
				$this->quizNavigation->redirectToEditQuiz();
			} catch (\Exception $exception) {}
		} else if ($this->quizNavigation->removeQuestion()) {
				try {
					$questionsToRemove = $this->questionListView->getQuestionsToRemove();
					$this->editQuizModel->removeQuestions($questionsToRemove);
					$this->quizNavigation->redirectToEditQuiz();
				} catch (\Exception $exception) {}
		} else if ($this->quizNavigation->addEditQuestion()) {
			try {
				$question = $this->newQuestionView->getQuestionCredentials();
				$this->editQuizModel->saveQuestion($question);
				$this->quizNavigation->redirectToEditQuiz();
			} catch (\Exception $exception) {}
		} else if ($this->quizNavigation->editQuestion()) {
			try {
				$question = $this->questionListView->getQuestion();
				return $this->newQuestionView->getEditQuestionForm($question);
			} catch (\Exception $exception) {
				$this->questionListView->editQuesitonFailed();
			}
		} else if ($this->quizNavigation->saveEdditedQuestion()) {
			try {
				$question = $this->newQuestionView->getQuestionCredentials();
				$questionPk = $this->newQuestionView->getQuestionPk();
				$this->editQuizModel->updateQuestion($question, $questionPk);
				$this->quizNavigation->redirectToEditQuiz();
			} catch (\Exception $exception) {
				$questionPk = $this->newQuestionView->getQuestionPk();
				return $this->quizNavigation->redirectToEditQuestion($questionPk);
			}
		}
		$editQuestionForm = $this->questionListView->getEditQuestions();
		$addQuestionForm = $this->newQuestionView->getAddQuestionForm();
		return $this->newQuizView->getEditQuizForm($this->quizCredentials, $editQuestionForm, $addQuestionForm);
	}
}