<?php

namespace quiz\controller;

require_once("./quiz/view/MessageView.php");
require_once("./quiz/model/MessageModel.php");

class MessageController {

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @var  \quiz\model\MessageModel
	 */
	private $messageModel;

	/** 
	 * @var \quiz\view\MessageView
	 */
	private $messageView;

	/** 
	 * @param mysqli $mysqli          
	 * @param \quiz\view\Navigation $quizNavigation  
	 * @param \user\model\UserCredentials $userCredentials 
	 */
	public function __construct(\mysqli $mysqli, 
								\quiz\view\Navigation $quizNavigation,
								\user\model\UserCredentials $userCredentials) {

		$this->quizNavigation = $quizNavigation;
		$this->messageModel = new \quiz\model\MessageModel($mysqli);
		$this->messageView = new \quiz\view\MessageView($this->quizNavigation, $userCredentials);
	}

	/** 
	 * @param  \user\view\TeacherNavigation $teacherNavigation 
	 * @return string HTML 
	 */
	public function doTeacherMessage(\user\view\TeacherNavigation $teacherNavigation) {

		if ($this->quizNavigation->sendMessage()) {
			try{
				$newMessage = $this->messageView->getMessage();
				$this->messageModel->saveMessage($newMessage);
				$teacherNavigation->reloadToStartPage();
			} catch(\Exception $exception) {}	
		}

		$messages = $this->messageModel->getMessages();
		return $this->messageView->getMessageForm($messages);
	}

	/**
	 * @return string HTML
	 */
	public function doMessages() {

		$messages = $this->messageModel->getMessages();
		return $this->messageView->getMessageList($messages);
	}
}