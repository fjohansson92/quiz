<?php

namespace quiz\model;

require_once("./quiz/model/QuizDAL.php");
require_once("./quiz/model/QuestionDAL.php");

class NewQuizModel {

	/** 
	 * @var \quiz\model\QuizDAL
	 */
	private $quizDAL;

	/** 
	 * @var \quiz\model\QuestionDAL
	 */
	private $questionDAL;

	/** 
	 * @var string
	 */
	private static $quizPkSessionLocation = "NewQuizModel::newQuizPk";

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->questionDAL = new \quiz\model\QuestionDAL($mysqli);
		$this->quizDAL = new \quiz\model\QuizDAL($mysqli);
	}

	/** 
	 * @param QuizCredentials $quizCredentials  
	 */
	public function saveNewQuiz(QuizCredentials $quizCredentials) {

		$newPk = $this->quizDAL->saveNewQuiz($quizCredentials);
		$quizCredentials->setPk($newPk);
		$this->saveQuizPk($quizCredentials->getPk());
	}

	/** 
	 * @param  QuestionCredentials $questionCredentials 
	 */
	public function saveNewQuestion(QuestionCredentials $questionCredentials) {
		$quizPk = $this->getSessionQuizPk();
		$questionCredentials->setQuizFk($quizPk);
		$this->questionDAL->saveNewQuestion($questionCredentials);
	}

	/** 
	 * @param integer $pk
	 */
	private function saveQuizPk($pk) {

		$_SESSION[self::$quizPkSessionLocation] = $pk;
	}

	/** 
	 * @return integer
	 * @throws If there is no pk saved in session
	 */
	private function getSessionQuizPk() {

		if ($this->sessionQuizPkIsset()) {

			return $_SESSION[self::$quizPkSessionLocation];
		}
		throw new \Exception();
	}

	/** 
	 * @return boolean
	 */
	public function sessionQuizPkIsset() {

		return isset($_SESSION[self::$quizPkSessionLocation]);
	}

	public function removeSessionQuizPk() {

		unset($_SESSION[self::$quizPkSessionLocation]);
	}
}