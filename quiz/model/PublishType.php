<?php

namespace quiz\model;

final class PublishType {

	const CREATOR = 0;
	const STUDENTS = 1;
	const REMOVED = 2;
}