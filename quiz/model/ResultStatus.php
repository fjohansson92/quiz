<?php

namespace quiz\model;

class ResultStatus {

	/** 
	 * @var integer
	 */
	public $pk;

	/** 
	 * @var integer
	 */
	private $quizFk;

	/** 
	 * @var boolean
	 */
	private $published;

	/** 
	 * @param integer $pk        
	 * @param integer  $quizFk    
	 * @param boolean  $published 
	 */
	public function __construct($pk = 0, $quizFk, $published) {

		$this->pk = $pk;
		$this->quizFk = $quizFk;
		$this->published = $published;
	}

	/** 
	 * @return integer
	 */
	public function getQuizFk() {
		return $this->quizFk;
	}

	/** 
	 * @return boolean
	 */
	public function getPublished() {
		return $this->published;
	}
}