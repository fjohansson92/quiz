<?php

namespace quiz\model;

class ResultCredentials {

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var integer
	 */
	private $quizFk;

	/** 
	 * @var integer
	 */
	private $userDataFk;

	/** 
	 * @var \quiz\model\GuessCredentials
	 */
	private $guessCredentials;

	/** 
	 * @param integer $pk               
	 * @param integer  $quizFk           
	 * @param integer $userDataFk       
	 * @param array of \quiz\model\GuessCredentials $guessCredentials 
	 */
	public function __construct($pk = 0, $quizFk, $userDataFk, $guessCredentials = NULL) {

		$this->pk = $pk;
		$this->quizFk = $quizFk;
		$this->userDataFk = $userDataFk;
		$this->guessCredentials = $guessCredentials;
	}

	/** 
	 * @return integer
	 */
	public function getCorrectAnswers() {

		$correctAnswers = 0;
		foreach ($this->guessCredentials as $guessCredential) {
			if ($guessCredential->isCorrect()) {
				$correctAnswers++;
			}
		}
		return $correctAnswers;
	}

	/** 
	 * @return integer
	 */
	public function getMaxCorrectAnswers() {
		return count($this->guessCredentials);
	}

	/** 
	 * @return array of strings
	 */
	public function getFailedQuestions() {

		$failedQuestions = array();
		foreach ($this->guessCredentials as $guessCredential) {
			if (!$guessCredential->isCorrect()) {
				$failedQuestions[] = $guessCredential->questionFk;
			}
		}
		return $failedQuestions;
	}

	/** 
	 * @return integer
	 */
	public function getQuizFk() {
		return $this->quizFk;
	}

	/** 
	 * @return integer
	 */
	public function getUserDataFk() {
		return $this->userDataFk;
	}

	/** 
	 * @return array of \quiz\model\GuessCredentials
	 */
	public function getGuessCredentials() {
		return $this->guessCredentials;
	}

}