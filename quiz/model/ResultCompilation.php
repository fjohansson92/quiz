<?php

namespace quiz\model;

class ResultCompilation {

	/** 
	 * @var \quiz\model\QuizCredentials 
	 */
	private $quizCredentials;

	/** 
	 * @var array of \quiz\model\ResultCredentials
	 */
	private $results;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @var \quiz\model\ResultStatus
	 */
	private $resultStatusFk;

	/** 
	 * @param \quiz\model\QuizCredentials $quizCredentials 
	 * @param array of \quiz\model\ResultCredentials $results         
	 * @param \user\model\UserCredentials $userCredentials 
	 * @param integer$resultStatusFk    
	 */
	public function __construct(\quiz\model\QuizCredentials $quizCredentials, 
								$results, \user\model\UserCredentials $userCredentials, 
								$resultStatusFk = NULL) {

		$this->quizCredentials = $quizCredentials;
		$this->results = $results;
		$this->userCredentials = $userCredentials;
		$this->resultStatusFk = $resultStatusFk;
	}

	/** 
	 * @return \quiz\model\QuizCredentials
	 */
	public function getQuizCredentials() {
		return $this->quizCredentials;
	}

	/** 
	 * @return array of \quiz\model\ResultCredentials
	 */
	public function getResults() {
		return $this->results;
	}

	/** 
	 * @return integer
	 */
	public function getQuizId() {
		return $this->quizCredentials->getPk();
	}

	/** 
	 * @return string
	 */
	public function getQuizTitel() {

		return $this->quizCredentials->getTitel();
	}

	/** 
	 * @return string
	 */
	public function getEndDate() {

		return $this->quizCredentials->getEndDate();
	}

	/** 
	 * @return double
	 */
	public function getAverage() {

		$correct = 0;
		$total = 0;

		foreach ($this->results as $result) {

			$correct += $result->getCorrectAnswers();
			$total += $result->getMaxCorrectAnswers();
		}

		return $correct / $this->getParticipants(); 
	}

	/** 
	 * @return integer
	 */
	public function getMaxCorrectAnswers() {
		return $this->results[0]->getMaxCorrectAnswers();
	}

	/** 
	 * @return integer
	 */
	public function getParticipants() {

		return count($this->results);
	}

	/** 
	 * @return string
	 */
	public function getFName() {

		return $this->userCredentials->getFName();
	}

	/** 
	 * @return string
	 */
	public function getLName() {

		return $this->userCredentials->getLName();
	}

	/** 
	 * @return \quiz\model\ResultStatus
	 */
	public function getResultStatus() {
		return $this->resultStatusFk;
	}
}
