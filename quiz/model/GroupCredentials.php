<?php

namespace quiz\model;

class GroupCredentials {

	/** 
	 * @var integer
	 */
	private static $groupNameMaxLength = 100;

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var string
	 */
	private $groupName;

	/** 
	 * @var user\model\UserCredentials
	 */
	private $teacher;

	/** 
	 * @param string $groupName 
	 * @param string $creator   
	 * @throws If invalid groupName or creator.
	 */
	public function __construct($pk = 0, $groupName, \user\model\UserCredentials $teacher) {

		if (empty($groupName)) {
			throw new \common\model\TitleEmptyException();
		} else if (strlen($groupName) > self::$groupNameMaxLength) {
			throw new \common\model\TitleLengthException(self::$groupNameMaxLength);
		}		

		$this->pk = $pk;
		$this->groupName = $groupName;
		$this->teacher = $teacher;
	}

	/** 
	 * @return integer
	 */
	public function getPk() {
		return $this->pk;
	}

	/** 
	 * @return string
	 */
	public function getGroupName() {

		return $this->groupName;
	}

	/** 
	 * @return string
	 */
	public function getTeacher() {

		return $this->teacher;
	}
}