<?php

namespace quiz\model;

class ResultDAL {

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;
		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  \quiz\model\ResultCredentials $results 
	 */
	public function saveResult(\quiz\model\ResultCredentials $results) {

		$sql = "INSERT INTO result (quizFk, userDataFk)  VALUES (?, ?)";

		$values = array($results->getQuizFk(), $results->getUserDataFk());

		$resultPk = $this->database->insert($sql, $values);

		$sql = "INSERT INTO guess (resultFk, isCorrect, questionFk) VALUES (?, ?, ?)";
		foreach ($results->getGuessCredentials() as $guessCredentials) {

			$values = array($resultPk , $guessCredentials->isCorrect(),$guessCredentials->questionFk);
			$this->database->insert($sql, $values);
		}
	}

	/** 
	 * @param  integer  $quizFk     
	 * @param  integer  $userDataFk 
	 * @return boolean             
	 */
	public function hasDoneQuiz($quizFk, $userDataFk) {

		$sql = "SELECT pk FROM result WHERE userDataFk = ? AND quizFk = ?";
		$values = array($userDataFk, $quizFk);

		$result = $this->database->select($sql, $values);
		if ($row = $result->fetch_assoc()) {
			return true;
		}
		return false;
	}

	/** 
	 * @param  integer $userDataPk
	 * @return array of  \quiz\model\ResultCredentials            
	 */
	public function getCompletedQuizzes($userDataPk) {

		$sql = "SELECT pk, quizFk, userDataFk FROM result WHERE userDataFk = ? ";

		$values = array($userDataPk);

		$result = $this->database->select($sql, $values);

		$results = array();
		while ($row = $result->fetch_assoc()) {   

	        $results[] = new \quiz\model\ResultCredentials($row["pk"], $row["quizFk"], $row["userDataFk"], NULL);
	    }
	    return $results;

	}

	/** 
	 * @param  integer $userDataPk 
	 * @return array of \quiz\model\ResultCredentials            
	 */
	public function getCompletedResults($userDataPk) {

		$sql = "SELECT pk, quizFk, userDataFk FROM result WHERE userDataFk = ?";
		$values = array($userDataPk);

		return $this->getResults($sql, $values);
	}

	/** 
	 * @return array of \quiz\model\ResultCredentials            
	 */
	public function getAllResults() {

		$sql = "SELECT pk, quizFk, userDataFk FROM result";
		$values = array();

		return $this->getResults($sql, $values);
	}

	/** 
	 * @param  string $sql    
	 * @param  array $values 
	 * @return array of \quiz\model\ResultCredentials          
	 */
	private function getResults($sql, $values) {

		$result = $this->database->select($sql, $values);

		$results = array();
		while ($row = $result->fetch_assoc()) {

			$guessSql = "SELECT pk, resultFk, isCorrect, questionFk FROM guess WHERE resultFk = ?";
			$guessValues = array($row["pk"]);

			$guessResult = $this->database->select($guessSql, $guessValues);

			$guesses = array();
			while ($guessRow = $guessResult->fetch_assoc()) {

				$guesses[] = new \quiz\model\GuessCredentials($guessRow["pk"], 
															  $guessRow["resultFk"], 
															  $guessRow["questionFk"], 
															  $guessRow["isCorrect"]);
			}	
			$results[] = new \quiz\model\ResultCredentials($row["pk"], $row["quizFk"], $row["userDataFk"], $guesses);
		}
		return $results;
	}
}

