<?php

namespace quiz\model;

class EditQuizModel {

	/** 
	 * @var integer
	 */
	private $quizId;

	/** 
	 * @var \quiz\model\QuizDAL
	 */
	private $quizDAL;

	/** 
	 * @var \quiz\model\QuestionDAL
	 */
	private $questionDAL;

	/** 
	 * @param \mysqli $mysqli     
	 * @param integer $userDataPk 
	 * @param integer $quizId         
	 */
	public function __construct(\mysqli $mysqli, $userDataPk, $quizId) {

		$this->userDataPk = $userDataPk;
		$this->quizId = $quizId;
		$this->quizDAL = new \quiz\model\QuizDAL($mysqli);
		$this->questionDAL = new \quiz\model\QuestionDAL($mysqli);
	}

	/** 
	 * @return \quiz\model\QuizCredentials
	 */
	public function getQuizCredentials() {

		$quizCredentials = $this->quizDAL->getQuiz($this->quizId, $this->userDataPk, \quiz\model\PublishType::CREATOR);
		return $quizCredentials;
	}

	/**
	 * @param \quiz\model\QuizCredentials $quizCredentials
	 */
	public function saveEditedQuiz(\quiz\model\QuizCredentials $quizCredentials) {
		
		$quizCredentials->setPk($this->quizId);
		$this->quizDAL->updateQuizCredentials($quizCredentials);
	}

	/** 
	 * @return array of  \quiz\model\QuestionCredentials
	 */
	public function getQuizQuestions() {

		return $this->questionDAL->getQuestionsForQuiz($this->quizId);
	} 

	/** 
	 * @param  \quiz\model\QuestionCredentials
	 */
	public function removeQuestions($questions) {

		$this->questionDAL->removeQuestions($questions);
	}

	/** 
	 * @param  \quiz\model\QuestionCredentials $questionCredentials                                            
	 */
	public function saveQuestion(\quiz\model\QuestionCredentials $questionCredentials) {

		$questionCredentials->setQuizFk($this->quizId);
		$this->questionDAL->saveNewQuestion($questionCredentials);
	}

	/** 
	 * @param  \quiz\model\QuestionCredentials $questionCredentials 
	 * @param  integer  $questionPk        
	 */
	public function updateQuestion(\quiz\model\QuestionCredentials $questionCredentials, $questionPk) {

		$questionCredentials->setQuizFk($this->quizId);
		$questionCredentials->setPk($questionPk);

		$removeQuestions = array($questionCredentials);
		$this->saveQuestion($questionCredentials);
		$this->removeQuestions($removeQuestions);
	}
}