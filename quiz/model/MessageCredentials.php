<?php

namespace quiz\model;

class MessageCredentials {

	/** 
	 * @var integer
	 */
	private static $titleMaxLength = 100;

	/** 
	 * @var integer
	 */
	private static $textMaxLength = 400;

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @var string
	 */
	private $text;

	/** 
	 * @var string
	 */
	private $title;

	/** 
	 * @param integer  $pk              
	 * @param \user\model\UserCredentials $userCredentials 
	 * @param string  $title           
	 * @param string  $text   
	 * @throws If title or text is in invalid format         
	 */
	public function __construct($pk = 0, \user\model\UserCredentials $userCredentials, $title, $text) {

		if (empty($title)) {
			throw new \common\model\TitleEmptyException();
		} else if (strlen($title) > self::$titleMaxLength) {
			throw new \common\model\TitleLengthException(self::$titleMaxLength);
		} else if (empty($text)) {
			throw new \common\model\TextEmptyException();
		} else if (strlen($text) > self::$textMaxLength) {
			throw new \common\model\TextLengthException(self::$textMaxLength);
		}

		$this->pk = $pk;
		$this->userCredentials = $userCredentials;
		$this->text = $text;
		$this->title = $title;
	}

	/** 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/** 
	 * @return string
	 */
	public function getText() {
		return $this->text;
	}

	/** 
	 * @return integer
	 */
	public function getUserDataFk() {
		return $this->userCredentials->getPk();
	}

	/** 
	 * @return string
	 */
	public function getFName() {
		return $this->userCredentials->getFName();
	}

	/** 
	 * @return string
	 */
	public function getLName() {
		return $this->userCredentials->getLName();
	}

}