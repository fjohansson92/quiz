<?php

namespace quiz\model;

class QuestionDAL {

	/** 
	 * @var string
	 */
	private $mysqli;

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;
		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param \quiz\model\QuestionCredentials $questionCredentials 
	 */
	public function saveNewQuestion(QuestionCredentials $questionCredentials) {

		$sql = "INSERT INTO questions (quizFk, question, description)  VALUES (?, ?, ?)";
		$values = array($questionCredentials->getQuizFk(), $questionCredentials->getQuestion(), $questionCredentials->getDescription());
		
		$questionPk = $this->database->insert($sql, $values);

		foreach ($questionCredentials->getAnswers() as $answer) {

			$sql = "INSERT INTO answers (questionFk, answer, isCorrect)  VALUES (?, ?, ?)";
			$values = array($questionPk, $answer->getAnswer(), $answer->getIsCorrect());

			$this->database->insert($sql, $values);
		}
	}

	/** 
	 * @param  integer $id 
	 * @return array of  \quiz\model\QuestionCredentials
	 */
	public function getQuestionsForQuiz($id) {

		$sql = "SELECT pk, quizFk, question, description FROM questions WHERE quizFk = ?";
		$values = array($id);
		$result = $this->database->select($sql, $values);

		$answerSql = "SELECT pk, questionFk, answer, isCorrect FROM answers WHERE questionFk = ?";

		$questions = array();
        while ($row = $result->fetch_assoc()) {   

        	$answers = array();
        	$answerValues = array($row["pk"]);
        	$answersResult = $this->database->select($answerSql, $answerValues);
        	while ($answerRow = $answersResult->fetch_assoc()) {   

        		$answers[] = new \quiz\model\AnswerCredentials($answerRow["pk"], 
        													   $answerRow["questionFk"], 
        													   $answerRow["answer"], 
        													   $answerRow["isCorrect"]); 
        	}

        	$questions[] =  new \quiz\model\QuestionCredentials($row["pk"], $row["quizFk"], $row["question"], $row["description"], $answers);
        } 
        return $questions;
	}

	/** 
	 * @param  array of \quiz\model\QuestionCredentials $questions 
	 */
	public function removeQuestions($questions) {
		
		$questionSql = "DELETE FROM questions WHERE pk = ?";
		$answerSql = "DELETE FROM answers WHERE questionFk = ?";

		foreach ($questions as $question) {

			$values = array($question->getPk());
			$this->database->insertTwoTables($questionSql, $answerSql, $values, $values);
		}
	}

	/** 
	 * @param integer $oldQuizId 
	 * @param integer $newQuizId 
	 */
	public function copyQuestions($oldQuizId, $newQuizId) {

		$questions = $this->getQuestionsForQuiz($oldQuizId);

		foreach ($questions as $question) {

			$question->setQuizFk($newQuizId);

			$this->saveNewQuestion($question);
		}
	}
}