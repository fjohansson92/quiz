<?php

namespace quiz\model;

require_once("./quiz/model/GuessCredentials.php");
require_once("./quiz/model/ResultCredentials.php");
require_once("./quiz/model/ResultDAL.php");

class DoQuizModel {

	/** 
	 * @var \quiz\model\QuizCredentials
	 */
	private $quizCredentials;

	/** 
	 * @var array of \quiz\model\QuestionCredentials
	 */
	private $questions;

	/** 
	 * @var \quiz\model\QuizDAL
	 */
	private $quizDAL;

	/** 
	 * @var \quiz\model\QuestionDAL
	 */
	private $questionDAL;

	/** 
	 * @var integer
	 */
	private $userDataPk;

	/** 
	 * @var \quiz\model\ResultDAL
	 */
	private $resultDAL;

	/** 
	 * @var string
	 */
	private static $QuizLocation = "DoQuizModel::QuizLocation";

	/** 
	 * @param \mysqli $mysqli     
	 * @param integer $userDataPk 
	 */
	public function __construct(\mysqli $mysqli, $userDataPk) {

		$this->quizDAL = new \quiz\model\QuizDAL($mysqli);
		$this->questionDAL = new \quiz\model\QuestionDAL($mysqli);
		$this->userDataPk = $userDataPk;
		$this->resultDAL = new \quiz\model\ResultDAL($mysqli);
	}

	/** 
	 * @param  integer $quizId    
	 * @throws If user has done quiz
	 */
	public function toggleQuiz($quizId) {

		if ($this->resultDAL->hasDoneQuiz($quizId, $this->userDataPk)) {
			throw new \Exception();
		}

		$this->quizCredentials = $this->quizDAL->getStudentQuiz($quizId, \quiz\model\PublishType::STUDENTS);
		$this->questions = $this->questionDAL->getQuestionsForQuiz($quizId);
		$this->saveQuiz($this->quizCredentials);
	}

	/** 
	 * @return \quiz\model\QuizCredentials
	 */
	public function getQuizCredentials() {

		return $this->quizCredentials;
	}

	/** 
	 * @return array of \quiz\model\QuestionCredentials
	 */
	public function getQuestions() {

		return $this->questions;
	}

	/** 
	 * @param  \quiz\model\QuizCredentials $quizCredentials   
	 */
	private function saveQuiz(\quiz\model\QuizCredentials $quizCredentials) {

		$_SESSION[self::$QuizLocation] = $quizCredentials;		
	}

	/** 
	 * @return \quiz\model\QuizCredentials $quizCredentials  
	 * @throws If quizCredentials not saved in session
	 */
	private function getSavedQuiz() {

		if (isset($_SESSION[self::$QuizLocation])) {

			return $_SESSION[self::$QuizLocation];
		}
		throw new \Exception();
	}

	/** 
	 * @param  array integers $guesses 
	 * @return \quiz\model\ResultCredentials          
	 */
	public function getResult($guesses) {

		$this->quizCredentials = $this->getSavedQuiz();
		$quizPk = $this->quizCredentials->getPk();
		$questions = $this->questionDAL->getQuestionsForQuiz($quizPk);

		$guessCredentials = array();
		$this->questions = array();
		foreach ($questions as $question) {

			$correctAnswer = $question->guessIsCorrect($guesses);
			if (!$correctAnswer) {
				$this->questions[] = $question;
			}

			$guessCredentials[] = new \quiz\model\GuessCredentials(NULL, NULL, $question->getPk(), $correctAnswer);
		}
		return new \quiz\model\ResultCredentials(NULL, $quizPk, $this->userDataPk, $guessCredentials);
	}

	/** 
	 * @param  quizmodelResultCredentials $results 
	 * @throws student has submitted quiz                             
	 */
	public function saveResult(\quiz\model\ResultCredentials $results) {

		if ($this->resultDAL->hasDoneQuiz($results->getQuizFk(), $this->userDataPk)) {
			throw new \Exception();
		}
		$this->resultDAL->saveResult($results);
	}
}