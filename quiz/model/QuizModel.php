<?php

namespace quiz\model;

require_once("./quiz/model/ResultStatus.php");
require_once("./quiz/model/ResultStatusDAL.php");
require_once("./quiz/model/PublishType.php");

class QuizModel {

	/** 
	 * @var string
	 */
	private static $EditIdLocation = "QuizModel::EditId";

	/** 
	 * @var \quiz\model\QuizDAL
	 */
	private $quizDAL;

	/** 
	 * @var integer
	 */
	private $userDataPk; 

	/** 
	 * @var \quiz\model\QuestionDAL
	 */
	private $questionDAL;

	/** 
	 * @var \quiz\model\ResultDAL
	 */
	private $resultDAL;

	/** 
	 * @var quiz\model\ResultStatusDAL
	 */
	private $resultStatusDAL;

	/** 
	 * @var \quiz\model\GroupDAL
	 */
	private $groupDAL;

	/** 
	 * @var array of \quiz\model\QuizCredentials
	 */
	private $publishedQuizzes;

	/** 
	 * @var \quiz\model\GroupFollowDAL
	 */
	private $groupFollowDAL;

	/** 
	 * @param \mysqli $mysqli     
	 * @param integer $userDataPk 
	 */
	public function __construct(\mysqli $mysqli, $userDataPk) {

		$this->quizDAL = new \quiz\model\QuizDAL($mysqli); 
		$this->groupFollowDAL = new \quiz\model\GroupFollowDAL($mysqli);
		$this->questionDAL = new \quiz\model\QuestionDAL($mysqli);
		$this->resultDAL = new \quiz\model\ResultDAL($mysqli);
		$this->resultStatusDAL = new \quiz\model\ResultStatusDAL($mysqli);
		$this->groupDAL = new \quiz\model\GroupDAL($mysqli);
		$this->userDataPk = $userDataPk;

	}

	/** 
	 * @return array of \quiz\model\QuizCredentials
	 */
	public function getQuizzes() {

		$myQuizzes = $this->quizDAL->getMyQuizzes($this->userDataPk);
		$unPublishedQuizzes = array();
		$this->publishedQuizzes = array();
		foreach ($myQuizzes as $quiz) {
			if ($quiz->getPublishType() == \quiz\model\PublishType::CREATOR) {
				$unPublishedQuizzes[] = $quiz;
			} else if ($quiz->getPublishType() == \quiz\model\PublishType::STUDENTS) {
				$this->publishedQuizzes[]= $quiz;
			}
		}
		return $unPublishedQuizzes;
	}

	/** 
	 * @return array of \quiz\model\QuizCredentials
	 */
	public function getPublishedQuizzes() {

		return $this->publishedQuizzes;
	}

	/** 
	 * @param integer $quizId 
	 * @throws If quiz not valid to be publish
	 */
	public function publishQuiz($quizId) {

		$quiz = $this->quizDAL->getQuiz($quizId, $this->userDataPk, \quiz\model\PublishType::CREATOR);

		$endTime = $quiz->getEndDateObject();
		if ($endTime->dateIsFuture()) {
			
			$questions = $this->questionDAL->getQuestionsForQuiz($quizId);
			if (empty($questions)) {
				throw new \Exception();
			}			

			if (!$this->groupDAL->groupExists($quiz->getGroupNameFk())) {
				throw new \Exception();
			}


			$quiz->publish();
			$publishId = $this->quizDAL->saveNewQuiz($quiz);

			$this->questionDAL->copyQuestions($quiz->getPk(), $publishId);

			$resultStatus = new \quiz\model\ResultStatus(NULL, $publishId, false);
			$this->resultStatusDAL->saveStatus($resultStatus);
		} else {
			throw new \Exception();
		}
	}

	/** 
	 * @param integer $quizId 
	 */
	public function removePublishedQuiz($quizId) {

		$quiz = $this->quizDAL->getQuiz($quizId, $this->userDataPk, \quiz\model\PublishType::STUDENTS);
		$quiz->remove(); 

		$this->quizDAL->updateQuizCredentials($quiz);
	}

	/** 
	 * @return array of \quiz\model\QuizCredentials
	 */
	public function getStudentQuizzes() {

		$followedGroups = $this->groupFollowDAL->getMyGroups($this->userDataPk);
		$completedQuizzes = $this->resultDAL->getCompletedQuizzes($this->userDataPk);

		$quizFks = array();
		foreach ($completedQuizzes as $result) {

			$quizFks[] = $result->getQuizFk();
		}
		return $this->quizDAL->getQuizzesFromGroups($followedGroups, $quizFks);
	}

	/** 
	 * @param integer $quizId 
	 */
	public function saveEditId($quizId) {

		try {
			$this->quizDAL->getQuiz($quizId, $this->userDataPk, \quiz\model\PublishType::CREATOR);
		} catch (\Exception $exception) {
			$this->getEditId();
		}

		if (!empty($quizId)) {

			$_SESSION[self::$EditIdLocation] = $quizId;	
		}
	}

	/** 
	 * @return integer
	 * @throws If id wasn't saved in session
	 */
	public function getEditId() {

		if (isset($_SESSION[self::$EditIdLocation])) {

			return $_SESSION[self::$EditIdLocation];
		}
		throw new \Exception();
	}


}