<?php

namespace quiz\model;

class OwnResultModel {

	/** 
	 * @var \quiz\model\ResultDAL
	 */
	private $resultDAL;

	/** 
	 * @var \quiz\model\QuizDAL
	 */
	private $quizDAL;

	/** 
	 * @var integer
	 */
	private $userDataPk;

	/** 
	 * @param \mysqli $mysqli     
	 * @param  integer $userDataPk 
	 */
	public function __construct(\mysqli $mysqli, $userDataPk) {

		$this->resultDAL = new \quiz\model\ResultDAL($mysqli);
		$this->quizDAL = new \quiz\model\QuizDAL($mysqli);
		$this->userDataPk = $userDataPk;
	}

	/** 
	 * @return array of \quiz\model\ResultCredentials
	 */
	public function getResults() {

		return $this->resultDAL->getCompletedResults($this->userDataPk);
	}

	/** 
	 * @param  array of \quiz\model\ResultCredentials $results 
	 * @return \quiz\model\QuizCredentials
	 */
	public function getQuizzes($results) {

		$resultQuizFks = array();
		foreach ($results as $result) {

			$resultQuizFks[] = $result->getQuizFk();
		}

		return $this->quizDAL->getQuizzesFromResults($resultQuizFks);
	}

}