<?php

namespace quiz\model;

class QuesitonCompilation {

	/** 
	 * @var \quiz\model\QuestionCredentials
	 */
	private $question;

	/** 
	 * @var integer
	 */
	private $correctGuesses;

	/** 
	 * @var integer
	 */
	private $guesses;

	/** 
	 * @param quizmodelQuestionCredentials $question       
	 * @param integer $correctGuesses 
	 * @param integer $guesses        
	 */
	public function __construct(\quiz\model\QuestionCredentials $question, $correctGuesses, $guesses) {

		$this->question = $question;
		$this->correctGuesses = $correctGuesses;
		$this->guesses = $guesses;
	}

	/** 
	 * @return \quiz\model\QuestionCredentials
	 */
	public function getQuestion() {
		return $this->question;
	}

	/** 
	 * @return integer
	 */
	public function getAverageProcent() {
		return $this->correctGuesses / $this->guesses * 100;
	}

}