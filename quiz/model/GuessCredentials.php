<?php

namespace quiz\model;

class GuessCredentials {

	/** 
	 * @var integer
	 */
	public $pk;

	/** 
	 * @var integer
	 */
	public $resultFk;

	/** 
	 * @var integer
	 */	
	public $questionFk;

	/** 
	 * @var boolean
	 */
	private $correctAnswer;

	/** 
	 * @param integer $pk            
	 * @param integer $resultFk      
	 * @param integer $questionFk    
	 * @param boolean $correctAnswer 
	 */
	public function __construct($pk = 0, $resultFk, $questionFk, $correctAnswer) {

		$this->pk = $pk;
		$this->resultFk = $resultFk;
		$this->questionFk = $questionFk;
		$this->correctAnswer = $correctAnswer;
	}

	/** 
	 * @return boolean 
	 */
	public function isCorrect() {

		return $this->correctAnswer;
	}
}