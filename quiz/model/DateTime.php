<?php

namespace quiz\model;

class DateTime {

	/** 
	 * @var string
	 */
	private static $dateFormat = "Y-m-d H:i:s";

	/** 
	 * @var string Y-m-d H:i:s Format
	 */
	private $dateTime;

	/** 
	 * @param string $newDateTime 
	 * @throws If string not valid datetime format
	 */
	public function __construct($newDateTime) {

		$dateTime =  \DateTime::createFromFormat(self::$dateFormat, $newDateTime);

		if ($dateTime && $dateTime->format(self::$dateFormat) == $newDateTime) {

			$this->dateTime = $newDateTime;
		} else {
			throw new \common\model\DateTimeException();
		}	
	}

	/** 
	 * @param  string $year 
	 * @param  string $month 
	 * @param  string $day  
	 * @param  string $hour 
	 * @param  string $minute
	 * @return \quiz\model\DateTime
	 */
	public static function createDateTime($year, $month, $day, $hour, $minute) {

		$monthZero = $month < 10 ? "0" : "";
		$dayZero = $day < 10 ? "0" : "";
		$hourZero = $hour < 10 ? "0" : "";
		$minuteZero = $minute < 10 ? "0" : "";

		return new \quiz\model\DateTime("$year-$monthZero$month-$dayZero$day $hourZero$hour:$minuteZero$minute:00");
	}

	/** 
	 * @return string
	 */
	public function getDateTime() {
		return $this->dateTime;
	}

	/** 
	 * @return boolean
	 */
	public function dateIsFuture() {

		$now = \quiz\model\DateTime::createDateTime(date('Y'), date('n'), date('j'), date('G'), intval(date('i')));

		return strtotime($now->getDateTime()) < strtotime($this->dateTime);
	}

	/** 
	 * @return \DateTime
	 */
	public function getDateObject() {

		return \DateTime::createFromFormat(self::$dateFormat, $this->dateTime);
	}

}