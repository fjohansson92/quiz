<?php 

namespace quiz\model;

class GroupFollowCred {

	/** 
	 * @var integer
	 */
	public $followPk;

	/** 
	 * @var integer
	 */
	public $userDataFk;

	/** 
	 * @var integer
	 */
	public $groupFk;

	/** 
	 * @param integer $followPk   
	 * @param integer  $userDataFk 
	 * @param integer  $groupFk    
	 */
	public function __construct($followPk = 0, $userDataFk, $groupFk) {

		$this->followPk = $followPk;
		$this->userDataFk = $userDataFk;
		$this->groupFk = $groupFk;
	}
}