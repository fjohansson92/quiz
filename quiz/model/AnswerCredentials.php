<?php

namespace quiz\model;

class AnswerCredentials {

	/** 
	 * @var integer
	 */
	private static $answerMaxLength = 150;

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var integer
	 */
	private $questionFk;

	/** 
	 * @var string
	 */
	private $answer;

	/** 
	 * @var boolean
	 */
	private $isCorrect;

	/** 
	 * @param integer $pk 
	 * @param integer $questionFk
	 * @param string  $answer
	 * @param boolean  $isCorrect
	 * @throws If answer was invalid
	 */
	public function __construct($pk = 0, $questionFk = 0 ,$answer, $isCorrect) {

		if (empty($answer)) {
			throw new \Exception();
		} else if (strlen($answer) > self::$answerMaxLength) {
			throw new \common\model\AnswerLengthException(self::$answerMaxLength);
		}

		$this->pk = $pk;
		$this->questionFk = $questionFk;
		$this->answer = $answer;
		$this->isCorrect = $isCorrect;
	}

	/** 
	 * @return integer
	 */
	public function getPk() {
		return $this->pk;
	}

	/** 
	 * @return integer
	 */
	public function getQuestionFk() {
		return $this->questionFk;
	}

	/** 
	 * @return string
	 */
	public function getAnswer() {
		return $this->answer;
	}

	/** 
	 * @return string
	 */
	public function getIsCorrect() {
		return $this->isCorrect;
	}

}