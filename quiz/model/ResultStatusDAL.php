<?php

namespace quiz\model;

class ResultStatusDAL {

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;
		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  \quiz\model\ResultStatus $resultStatus 
	 */
	public function saveStatus(\quiz\model\ResultStatus $resultStatus) {

		$sql = "INSERT INTO resultstatus (quizFk, published)  VALUES (?, ?)";
		$values = array($resultStatus->getQuizFk(), $resultStatus->getPublished());

		$this->database->insert($sql, $values);
	}

	/** 
	 * @return array of \quiz\model\ResultStatus
	 */
	public function getAllStatuses() {

		$sql = "SELECT pk, quizFk, published FROM resultstatus";

		$values = array();
		$result = $this->database->select($sql, $values);

		$statuses = array();
		while ($row = $result->fetch_assoc()) {   

	        $statuses[] = new \quiz\model\ResultStatus($row["pk"], $row["quizFk"], $row["published"]);
	    }
	    return $statuses;
	}

	/** 
	 * @param  integer $statusId 
	 * @param  \quiz\model\ResultStatus $newStatus  
	 * @throws If status wasn't found            
	 */
	public function changeStatus($statusId, $newStatus) {

		if ($this->statusExists($statusId)) {
			$sql = "UPDATE resultstatus SET published = ? WHERE pk = ?";

			$values = array($newStatus, $statusId);
			$this->database->insert($sql, $values);
		} else {
			throw new \Exception();	
		}
	}

	/** 
	 * @param  integer $statusId 
	 * @return boolean
	 */
	private function statusExists($statusId) {

		$sql = "SELECT pk FROM resultstatus WHERE pk = ?";

		$values = array($statusId);

		$result = $this->database->select($sql, $values);
		return $row = $result->fetch_assoc(); 
	}
}