<?php

namespace quiz\model;

class QuizDAL {

	/** 
	 * @var string
	 */
	private $mysqli;

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;
		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  QuizCredentials $quizCredentials 
	 * @return integer
	 */
	public function saveNewQuiz(QuizCredentials $quizCredentials) {

		$sql = "INSERT INTO quiz (titel, endDate, groupNameFk, quizCreatorFk, publishType)  VALUES (?, ?, ?, ?, ?)";
		$values = array($quizCredentials->getTitel(),
						$quizCredentials->getEndDate(),
						$quizCredentials->getGroupNameFk(), 
						$quizCredentials->getQuizCreatorFk(),
						$quizCredentials->getPublishType());

		return $this->database->insert($sql, $values);
	}

	/** 
	 * @param  integer $userDataPk 
	 * @return array of \quiz\model\QuizCredentials            
	 */
	public function getMyQuizzes($userDataPk) {

		$sql = "SELECT pk, titel, endDate, groupNameFk, quizCreatorFk, publishType FROM quiz WHERE quizCreatorFk = ?";
		$values = array($userDataPk);

		return $this->sortMultipleQuizzesDb($sql, $values);
	}

	/** 
	 * @param  integer $id          
	 * @param  integer $userDataPk  
	 * @param  \quiz\model\PublishType $publishType 
	 * @return \quiz\model\QuizCredentials
	 */
	public function getQuiz($id, $userDataPk, $publishType) {

		$sql = "SELECT pk, titel, endDate, groupNameFk, quizCreatorFk, publishType FROM quiz WHERE quizCreatorFk = ? AND pk = ? AND publishType = ?";
		$values = array($userDataPk, $id, $publishType);

		return $this->sortQuizDb($sql, $values);
	}

	/** 
	 * @param integer $id          
	 * @param  \quiz\model\PublishType $publishType 
	 * @return \quiz\model\QuizCredentials            
	 */
	public function getStudentQuiz($id, $publishType) {

		$sql = "SELECT pk, titel, endDate, groupNameFk, quizCreatorFk, publishType FROM quiz WHERE pk = ? AND publishType = ?";
		$values = array($id, $publishType);

		return $this->sortQuizDb($sql, $values);
	}

	/** 
	 * @param  \quiz\model\QuizCredentials $quizCredentials   
	 */
	public function updateQuizCredentials(\quiz\model\QuizCredentials $quizCredentials) {

		$sql = "UPDATE quiz SET titel = ?, endDate = ?, groupNameFk = ?, publishType = ? WHERE quizCreatorFk = ? AND pk = ? ";

		$values = array($quizCredentials->getTitel(), 
						$quizCredentials->getEndDate(), 
						$quizCredentials->getGroupNameFk(), 
						$quizCredentials->getPublishType(), 
						$quizCredentials->getQuizCreatorFk(), 
						$quizCredentials->getPk());

		$this->database->insert($sql, $values);
	}

	/** 
	 * @return array of \quiz\model\QuizCredentials
	 */
	public function getPublishedQuizzes() {

		$sql = "SELECT pk, titel, endDate, groupNameFk, quizCreatorFk, publishType FROM quiz WHERE publishType != ?";
		$values = array(\quiz\model\PublishType::CREATOR);

		return $this->sortMultipleQuizzesDb($sql, $values);
	}

	/** 
	 * @param  quiz\model\GroupFollowCred $followedGroups 
	 * @param  quiz\model\ResultCredentials $results        
	 * @return array of \quiz\model\QuizCredentials                
	 */
	public function getQuizzesFromGroups($followedGroups, $results) {

		$sql = "SELECT pk, titel, endDate, groupNameFk, quizCreatorFk, publishType FROM quiz WHERE groupNameFk = ? AND publishType = ?";

		$quizzes = array();

		foreach ($followedGroups as $followedGroup) {

			$values = array($followedGroup->groupFk, \quiz\model\PublishType::STUDENTS);

			$result = $this->database->select($sql, $values);

			while ($row = $result->fetch_assoc()) {   

				if (!in_array($row["pk"], $results)) {
	        		$endDate = new \quiz\model\DateTime($row["endDate"]);
	        		$quizzes[] = new \quiz\model\QuizCredentials($row["pk"], 
	        													 $row["titel"], 
	        													 $endDate, 
	        													 $row["groupNameFk"], 
	        													 $row["quizCreatorFk"], 
	        													 $row["publishType"]);
	        	}
	        }
		}
		return $quizzes;
	}

	/** 
	 * @param  array of integers $resultQuizFks 
	 * @return array of \quiz\model\QuizCredentials              
	 */
	public function getQuizzesFromResults($resultQuizFks) {

		$sql = "SELECT pk, titel, endDate, groupNameFk, quizCreatorFk, publishType FROM quiz WHERE publishType = ?";

		$values = array(\quiz\model\PublishType::STUDENTS);

		$result = $this->database->select($sql, $values);

		$quizzes = array();
		while ($row = $result->fetch_assoc()) {   
			
			if (in_array($row["pk"], $resultQuizFks)) {
	       		$endDate = new \quiz\model\DateTime($row["endDate"]);
	       		$quizzes[] = new \quiz\model\QuizCredentials($row["pk"], $row["titel"], $endDate, $row["groupNameFk"], $row["quizCreatorFk"], $row["publishType"]);
	       	}
	    }
	    return $quizzes; 
	}

	/** 
	 * @param  string $sql   
	 * @param  array $values
	 * @return \quiz\model\QuizCredentials    
	 * @throws If quiz wasn't found
	 */
	private function sortQuizDb($sql, $values) {

		$result = $this->database->select($sql, $values);

		if ($row = $result->fetch_assoc()) {  

			$endDate = new \quiz\model\DateTime($row["endDate"]);
			return new \quiz\model\QuizCredentials($row["pk"], $row["titel"], $endDate, $row["groupNameFk"], $row["quizCreatorFk"], $row["publishType"]);
		}
		throw new \Exception();
	}

	/** 
	 * @param  string $sql   
	 * @param  array $values
	 * @return array of \quiz\model\QuizCredentials    
	 */
	private function sortMultipleQuizzesDb($sql, $values) {

		$result = $this->database->select($sql, $values);

		$quizzes = array();
        while ($row = $result->fetch_assoc()) {   

        	$endDate = new \quiz\model\DateTime($row["endDate"]);
        	$quizzes[] = new \quiz\model\QuizCredentials($row["pk"], $row["titel"], $endDate, $row["groupNameFk"], $row["quizCreatorFk"], $row["publishType"]);
        }	
        return $quizzes;
	}


}
