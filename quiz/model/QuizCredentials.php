<?php

namespace quiz\model;

class QuizCredentials {

	/** 
	 * @var integer
	 */
	private static $titleMaxLength = 50;

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var string
	 */
	private $titel;

	/** 
	 * @var string Y-m-d H:i:s Format
	 */
	private $endDate;

	/** 
	 * @var string
	 */
	private $groupNameFk;

	/** 
	 * @var string
	 */
	private $quizCreatorFk;

	/** 
	 * @var boolean
	 */
	private $publishType;

	/** 
	 * @param integer $id          
	 * @param string $titel       
	 * @param \quiz\model\DateTime $endDate  
	 * @param string $groupName   
	 * @param string $quizCreator 
	 * @param \quiz\model\PublishType $published   
	 * @throws If invalid title or group
	 */
	public function __construct($pk = 0, $titel, \quiz\model\DateTime $endDate, $groupNameFk, $quizCreatorFk, $publishType) {

		if (empty($titel)) {
			throw new \common\model\TitleEmptyException();
		}else if ( strlen($titel) > self::$titleMaxLength) {
			throw new \common\model\TitleLengthException(self::$titleMaxLength);
		} else if (empty($groupNameFk)) {
			throw new \Exception();
		}

		$this->pk = $pk;
		$this->titel = $titel;
		$this->endDate = $endDate;
		$this->groupNameFk = $groupNameFk;
		$this->quizCreatorFk = $quizCreatorFk;
		$this->publishType = $publishType;
	}

	/** 
	 * @return integer
	 */
	public function getPk() {
		return $this->pk;
	}

	/** 
	 * @param integer $pk 
	 */
	public function setPk($pk) {
			$this->pk = $pk;
	}

	/** 
	 * @return string
	 */
	public function getTitel() {

		return $this->titel;
	}

	/** 
	 * @return string Y-m-d H:i:s Format
	 */
	public function getEndDate() {

		return $this->endDate->getDateTime();
	}

	/** 
	 * @return \quiz\model\DateTime
	 */
	public function getEndDateObject() {

		return $this->endDate;
	}

	/** 
	 * @return string
	 */
	public function getGroupNameFk() {

		return $this->groupNameFk;
	}

	/** 
	 * @return string
	 */
	public function getQuizCreatorFk() {

		return $this->quizCreatorFk;
	}

	/** 
	 * @return \quiz\model\PublishType
	 */
	public function getPublishType() {

		return $this->publishType;
	}

	public function publish() {
		$this->publishType = \quiz\model\PublishType::STUDENTS;
	}

	public function remove() {
		$this->publishType = \quiz\model\PublishType::REMOVED;
	}
}