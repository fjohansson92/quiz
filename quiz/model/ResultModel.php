<?php

namespace quiz\model;

require_once("./quiz/model/ResultCompilation.php");
require_once("./quiz/model/QuestionCompilation.php");

class ResultModel {

	/** 
	 * @var array of \quiz\model\ResultCompilation
	 */
	private $privateResults;

	/** 
	 * @var array of \quiz\model\ResultCompilation
	 */
	private $openResults;

	/** 
	 * @var \quiz\model\ResultDAL
	 */
	private $resultDAL;

	/** 
	 * @var \quiz\model\ResultStatusDAL
	 */
	private $resultStatusDAL;

	/** 
	 * @var \quiz\model\QuizDAL
	 */
	private $quizDAL;

	/** 
	 * @var \user\model\Users
	 */
	private $userDAL;

	/** 
	 * @var \quiz\model\QuestionDAL
	 */
	private $questionDAL;

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->resultStatusDAL = new \quiz\model\ResultStatusDAL($mysqli);
		$this->resultDAL = new \quiz\model\ResultDAL($mysqli);
		$this->quizDAL = new \quiz\model\QuizDAL($mysqli);
		$this->userDAL = new \user\model\Users($mysqli);
		$this->questionDAL = new \quiz\model\QuestionDAL($mysqli);
	}

	/** 
	 * @param  integer $quizId 
	 * @return \quiz\model\ResultCompilation        
	 */
	public function getTeacherCompilation($quizId) {

		return $this->getResultCompilation($quizId);
	}

	/** 
	 * @param  integer $quizId 
	 * @return \quiz\model\ResultCompilation
	 * @throws If result not available
	 */
	public function getStudentCompilation($quizId) {

		$resultCompilation = $this->getResultCompilation($quizId);
		$resultStatuses = $this->resultStatusDAL->getAllStatuses(); 

		foreach ($resultStatuses as $resultStatus) {

			if ($resultStatus->getQuizFk() == $resultCompilation->getQuizId() && $resultStatus->getPublished()) {
				return $resultCompilation;
			}
		}

		throw new \Exception();
	}

	/** 
	 * @param  integer $quizId 
	 * @return \quiz\model\ResultCompilation
	 * @throws If quiz wasn't found
	 */
	private function getResultCompilation($quizId) {

		$results = $this->resultDAL->getAllResults();
		$quizResults = $this->getResults($results, $quizId);
		$quizzes = $this->quizDAL->getPublishedQuizzes();
		if (empty($quizResults)) {
			throw new \Exception();
		}

		$quizToShow;
		foreach ($quizzes as $quiz) {
			if ($quiz->getPk() == $quizId) {
				$quizToShow = $quiz;
			}
		}

		$userDataPk = $quizToShow->getQuizCreatorFk();
		$userData = $this->userDAL->getFullName($userDataPk);

		return new \quiz\model\ResultCompilation($quizToShow, $quizResults, $userData, NULL);
	}

	public function sortResults() {

		$quizzes = $this->quizDAL->getPublishedQuizzes();
		$results = $this->resultDAL->getAllResults();
		$resultStatuses = $this->resultStatusDAL->getAllStatuses(); 

		foreach ($quizzes as $quiz) {

			$quizResults = $this->getResults($results, $quiz->getPk());

			if (!empty($quizResults)) {

				foreach ($resultStatuses as $resultStatus) {

					if ($quiz->getPk() == $resultStatus->getQuizFk()) {

						$userDataPk = $quiz->getQuizCreatorFk();
						$userData = $this->userDAL->getFullName($userDataPk);

						if ($resultStatus->getPublished()) {
							$this->openResults[] = new \quiz\model\ResultCompilation($quiz, $quizResults, $userData, $resultStatus->pk); 
						} else {
							$this->privateResults[] = new \quiz\model\ResultCompilation($quiz, $quizResults, $userData, $resultStatus->pk);
						}
					}
				}
			}
		}
	}	

	/** 
	 * @param  array of \quiz\model\ResultCredentials $results 
	 * @param  integer $quizId  
	 * @return \quiz\model\ResultCredentials          
	 */
	private function getResults($results, $quizId) {

		$quizResults = array();
		foreach ($results as $result) {

			if ($result->getQuizFk() == $quizId) {

				$quizResults[] = $result; 
			}
		}
		return $quizResults;
	}

	/** 
	 * @param  \quiz\model\ResultCompilation $resultCompilation 
	 * @return array of \quiz\model\QuesitonCompilation
	 */
	public function getQuestionCompilations(\quiz\model\ResultCompilation $resultCompilation) {

		$quizCredentials = $resultCompilation->getQuizCredentials();
		$quizId = $quizCredentials->getPk();
		$questions = $this->questionDAL->getQuestionsForQuiz($quizId);		
		$results = $resultCompilation->getResults();


		$quesitonCompilation = array();
		foreach ($questions as $question) {

			$correctAnswers = 0;
			$answers = 0;
			foreach ($results as $result) {

				foreach ($result->getGuessCredentials() as $guessCredential) {

					if ($guessCredential->questionFk == $question->getPk()) {
						$answers++;
						if ($guessCredential->isCorrect()) {
							$correctAnswers++;
						}
					}
				}	
			}
			if ($answers > 0) {
				$quesitonCompilation[] = new \quiz\model\QuesitonCompilation($question, $correctAnswers, $answers);
			}
		}
		return $quesitonCompilation;
	}

	/** 
	 * @return array of \quiz\model\ResultCompilation
	 */
	public function getPrivateResultsComp() {

		return $this->privateResults;
	}

	/** 
	 * @return array of \quiz\model\ResultCompilation
	 */
	public function getOpenResultsComp() {

		return $this->openResults;
	}

	/** 
	 * @param integer
	 */
	public function publishResult($statusId) {

		$this->resultStatusDAL->changeStatus($statusId, true);
	}

	/** 
	 * @param integer
	 */
	public function unPublishResult($statusId) {

		$newpk = NULL;
		$newpk = $this->resultStatusDAL->changeStatus($statusId, false);
	}

}