<?php

namespace quiz\model;

class MessageDAL {

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @var string
	 */
	private static $tableName = "messages";

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {

		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  array of \user\model\UserCredentials
	 * @param  integer $messageLimit  
	 * @return array of \quiz\model\MessageCredentials           
	 */
	public function getMessages($teachers, $messageLimit) {

		$sql = "SELECT pk, userDataFk, title, postText FROM " . self::$tableName . " ORDER BY pk DESC LIMIT ?";

		$values = array($messageLimit);

		$result = $this->database->select($sql, $values);

		$messages = array();
		while ($row = $result->fetch_assoc()) {   

			foreach ($teachers as $teacher) {
				if ($teacher->getPk() == $row["userDataFk"]) {
					
					$messages[] = new \quiz\model\MessageCredentials($row["pk"], $teacher, $row["title"], $row["postText"]);
				}
			}
        }	
        return $messages;
	}

	/** 
	 * @param \quiz\model\MessageCredentials $message 
	 */
	public function saveMessage(\quiz\model\MessageCredentials $message) {

		$sql = "INSERT INTO " . self::$tableName . " (userDataFk, title, postText)  VALUES (?, ?, ?)";

		$values = array($message->getUserDataFk(), $message->getTitle(), $message->getText());
		
		$questionPk = $this->database->insert($sql, $values);
	}
}