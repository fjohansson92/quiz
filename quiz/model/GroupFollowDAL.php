<?php

namespace quiz\model;

class GroupFollowDAL {

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {

		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  integer $userDataPk 
	 * @return array of \quiz\model\GroupFollowCred            
	 */
	public function getMyGroups($userDataPk) {

		$sql = "SELECT pk, userNameFk, groupFk FROM groupfollow WHERE userNameFk = ?";

		$values = array($userDataPk);

		$result = $this->database->select($sql, $values);

		$groupFollowCreds = array();
		while ($row = $result->fetch_assoc()) {   

        	$groupFollowCreds[] = new \quiz\model\GroupFollowCred($row["pk"], $row["userNameFk"], $row["groupFk"]);
        }	
		return $groupFollowCreds;
	}

	/** 
	 * @param  array of \quiz\model\GroupFollowCred $groupFollowCreds
	 * @param  integer $userDataPk                      
	 */
	public function saveFollowedGroups($groupFollowCreds, $userDataPk) {

		$deleteSql = "DELETE FROM groupfollow WHERE userNameFk = ?";
		$insertSql = "INSERT INTO groupfollow (userNameFk, groupFk)  VALUES (?, ?)";

		$deleteValues = array($userDataPk);
		$this->database->select($deleteSql, $deleteValues);		

		foreach ($groupFollowCreds as $groupFollowCred) {

			$insertValues = array($groupFollowCred->userDataFk, $groupFollowCred->groupFk);
			$this->database->select($insertSql, $insertValues);
		} 
	}
}