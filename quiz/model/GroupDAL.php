<?php

namespace quiz\model;

class GroupDAL {

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->database = new \common\model\Database($mysqli);
		$this->users = new \user\model\Users($mysqli);
	}

	/** 
	 * @param  integer $userDataPk 
	 * @return array of \quiz\model\GroupCredentials            
	 */
	public function getTeacherGroups($userDataPk) {
		$sql = "SELECT pk, groupName, userDataFk FROM groups WHERE userDataFk = ? ORDER BY pk DESC";
		$values = array($userDataPk);
		return $this->getGroups($sql, $values);
	}

	/** 
	 * @param  integer $userDataPk 
	 * @return array of \quiz\model\GroupCredentials            
	 */
	public function getOtherTeacherGroups($userDataPk) {
		$sql = "SELECT pk, groupName, userDataFk FROM groups WHERE userDataFk != ?";
		$values = array($userDataPk);
		return $this->getGroups($sql, $values);
	} 

	/** 
	 * @return array of \quiz\model\GroupCredentials           
	 */
	public function getAllGroups() {
		$sql = "SELECT pk, groupName, userDataFk FROM groups";
		return $this->getGroups($sql, array());
	}

	/** 
	 * @param  string $groupName 
	 * @return array of \quiz\model\GroupCredentials            
	 */
	public function getGroupFromName($groupName) {
		$sql = "SELECT pk, groupName, userDataFk FROM groups WHERE groupName = ?";
		$values = array($groupName);
		return $this->getGroups($sql, $values);     
	}

	/** 
	 * @param  integer $groupPk 
	 * @return boolean          
	 */
	public function groupExists($groupPk) {
		$sql = "SELECT pk, groupName, userDataFk FROM groups WHERE pk = ?";
		$values = array($groupPk);
		$groups = $this->getGroups($sql, $values);
		return !empty($groups);    
	}

	/** 
	 * @param  string $sql   
	 * @param  array $values
	 * @return array of \quiz\model\GroupCredentials        
	 */
	private function getGroups($sql, $values) {

		$result = $this->database->select($sql, $values);

		$groups = array();

        while ($row = $result->fetch_assoc()) {   

        	$teacher = $this->users->getFullName($row["userDataFk"]);
        	
        	$groups[] = new \quiz\model\GroupCredentials($row["pk"], $row["groupName"], $teacher);
        }	
		return $groups;
	}

	/** 
	 * @param  array of \quiz\model\GroupCredentials  $groups     
	 * @param  integer $userDataPk             
	 */
	public function removeGroups($groups, $userDataPk) {

		foreach($groups as $groupPk) {

			$sql = "DELETE FROM groups WHERE userDataFk = ? AND pk = ?";

			$values = array($userDataPk, $groupPk);
			$this->database->insert($sql, $values);
		}
	}

	/** 
	 * @param \quiz\model\GroupCredentials  $newGroup 
	 */
	public function addGroup($newGroup) {

		$sql = "INSERT INTO groups (groupName, userDataFk)  VALUES (?, ?)";

		$teacher = $newGroup->getTeacher();
		$values = array($newGroup->getGroupName(), $teacher->getPk());

		$this->database->insert($sql, $values); 
	}

}