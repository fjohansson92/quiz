<?php

namespace quiz\model;

require_once("./quiz/model/GroupDAL.php");
require_once("./quiz/model/GroupFollowDAL.php");

class GroupModel {

	/** 
	 * @var \quiz\model\GroupDAL
	 */
	private $groupDAL;

	/** 
	 * @var integer
	 */
	private $userDataPk;

	/** 
	 * @var array of \quiz\model\GroupCredentials
	 */
	private $notFollowed;

	/** 
	 * @var \user\model\UserCredentials
	 */
	private $userCredentials;

	/** 
	 * @var \user\model\GroupFollowDAL
	 */
	private $groupFollowDAL;
	
	/** 
	 * @param \mysqli $mysqli          
	 * @param \user\model\UserCredentials $userCredentials 
	 */
	public function __construct(\mysqli $mysqli, \user\model\UserCredentials $userCredentials) {

		$this->groupDAL = new \quiz\model\GroupDAL($mysqli);
		$this->groupFollowDAL = new \quiz\model\GroupFollowDAL($mysqli);
		$this->userDataPk = $userDataPk = $userCredentials->getPk();
		$this->userCredentials = $userCredentials;
	}

	/** 
	 * @return array of \quiz\model\GroupCredentials 
	 */
	public function getMyGroups() {

		return $this->groupDAL->getTeacherGroups($this->userDataPk);
	}

	/** 
	 * @return array of \quiz\model\GroupCredentials 
	 */
	public function getOtherGroups() {

		return $this->groupDAL->getOtherTeacherGroups($this->userDataPk);
	}

	/** 
	 * @return array of \quiz\model\GroupCredentials 
	 */
	public function getAllGroups() {

		return $this->groupDAL->getAllGroups();
	}

	/** 
	 * @param  array of \quiz\model\GroupCredentials 
	 * @throws No groups selected to be removed
	 */
	public function removeGroups($groups) {

		if (empty($groups)) {
			throw new \Exception();
		}
		$this->groupDAL->removeGroups($groups, $this->userDataPk);
	}

	/** 
	 * @param string $groupName     
	 * @param \quiz\model\GroupObserver $groupObserver 
	 * @throws If group exists
	 */
	public function addGroup($groupName, \quiz\model\GroupObserver $groupObserver) {

		$group = $this->groupDAL->getGroupFromName($groupName); 
		if (!empty($group)) {
			$groupObserver->uniqNameFail();
			throw new \Exception();
		}	

		$newGroup = new \quiz\model\GroupCredentials(NULL, $groupName, $this->userCredentials);

		$this->groupDAL->addGroup($newGroup);
	}

	/** 
	 * @return array of \quiz\model\GroupCredentials
	 */
	public function getFollowedGroups() {

		$groupFollowCreds = $this->groupFollowDAL->getMyGroups($this->userDataPk);
		$allGroups = $this->groupDAL->getAllGroups();

		$followed = array();
		$this->notFollowed = array();
		foreach ($allGroups as $group) {
			$isFollowing = false;
			foreach ($groupFollowCreds as $followedGroup) {
				if ($group->getPk() == $followedGroup->groupFk) {
					$isFollowing = true;
				}
			}
			if (!$isFollowing) {
				$this->notFollowed[] = $group;
			} else {
				$followed[] = $group;
			}
		}
		return $followed;
	}

	/** 
	 * @return array of \quiz\model\GroupCredentials
	 */
	public function getNotFollowedGroups() {

		return $this->notFollowed;
	}

	/** 
	 * @param array of \quiz\model\GroupCredentials $groups         
	 */
	public function saveFollowedGroups($groups) {

		$groupFollowCreds = array();
		foreach ($groups as $groupPk) {

			$groupFollowCreds[] = new \quiz\model\GroupFollowCred(NULL, $this->userDataPk, $groupPk);
		}
		$this->groupFollowDAL->saveFollowedGroups($groupFollowCreds, $this->userDataPk);
	}
}