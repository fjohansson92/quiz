<?php

namespace quiz\model;

class QuestionCredentials {

	/** 
	 * @var integer
	 */
	private static $questionMaxLength = 255;

	/** 
	 * @var integer
	 */
	private static $textMaxLength = 255;

	/** 
	 * @var integer
	 */
	private static $answerMinLength = 2;

	/** 
	 * @var integer
	 */
	private $pk;

	/** 
	 * @var integer
	 */
	private $quizFk;

	/** 
	 * @var string
	 */
	private $question;

	/** 
	 * @var string
	 */
	private $description;

	/** 
	 * @var array of \quiz\model\AnswerCredentials
	 */
	private $answers;

	/** 
	 * @param integer $pk  
	 * @param integer $quizFk   
	 * @param string $question  
	 * @param string  $description 
	 * @param array of \quiz\model\AnswerCredentials $answers  
	 * @throws if question or description is invalid or not enough answers
	 */
	public function __construct($pk = 0, $quizFk = 0, $question, $description, $answers) {

		if (empty($question)){
			throw new \common\model\QuestionEmptyException();
		} else if ( strlen($question) > self::$questionMaxLength) {
			throw new \common\model\QuestionLengthException(self::$questionMaxLength);
		} else if (strlen($description) > self::$textMaxLength) {
			throw new \common\model\TextLengthException(self::$textMaxLength);
		} else if (empty($answers) || count($answers) < self::$answerMinLength) {
			throw new \common\model\AnswerCountException(self::$answerMinLength);
		}

		$this->pk = $pk;
		$this->quizFk = $quizFk;
		$this->question = $question;
		$this->description = $description;
		$this->answers = $answers;
	}

	/** 
	 * @return integer
	 */
	public function getPk() {
		return $this->pk;
	}

	/** 
	 * @param integer $pk 
	 */
	public function setPk($pk) {
		$this->pk = $pk;
	}

	/** 
	 * @return integer
	 */
	public function getQuizFk() {
		return $this->quizFk;
	}

	/** 
	 * @param integer $pk 
	 */
	public function setQuizFk($quizFk) {

		$this->quizFk = $quizFk;
	}

	/** 
	 * @return string 
	 */
	public function getQuestion() {

		return $this->question;
	}

	/** 
	 * @return string 
	 */
	public function getDescription() {

		return $this->description;
	}

	/** 
	 * @return array of \quiz\model\AnswerCredentials
	 */
	public function getAnswers() {

		return $this->answers;
	}

	/** 
	 * @param  array of integers $guesses
	 * @return boolean
	 */
	public function guessIsCorrect($guesses) {
		
		foreach ($this->answers as $answer) {

			if ($answer->getIsCorrect()) {
				if (!in_array($answer->getPk(), $guesses)) {
					return false;
				}
			} else {
				if (in_array($answer->getPk(), $guesses)) {
					return false;
				}
			}
		}
		return true;
	}
}