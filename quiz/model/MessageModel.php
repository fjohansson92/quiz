<?php

namespace quiz\model;

require_once("./quiz/model/MessageDAL.php");
require_once("./quiz/model/MessageCredentials.php");

class MessageModel {

	/** 
	 * @var integer
	 */
	private static $messages = 5;

	/** 
	 * @var \quiz\model\MessageDAL
	 */
	private $messageDAL;

	/** 
	 * @var \user\model\Users
	 */
	private $users;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {

		$this->messageDAL = new \quiz\model\MessageDAL($mysqli);
		$this->users = new \user\model\Users($mysqli);
	}

	/** 
	 * @return array of \quiz\model\MessageCredentials
	 */
	public function getMessages() {

		$teachers = $this->users->getEveryUserFromType(\user\model\UserType::TEACHER);
		$messages = $this->messageDAL->getMessages($teachers, self::$messages);
		return $messages;
	}

	/** 
	 * @param  \quiz\model\MessageCredentials $message
	 */
	public function saveMessage(\quiz\model\MessageCredentials $message) {

		$this->messageDAL->saveMessage($message);
	}
}