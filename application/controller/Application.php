<?php

namespace application\controller;

require_once("./common/view/Page.php");
require_once("./guest/controller/GuestController.php");
require_once("./common/model/Database.php");
require_once("./user/controller/UserController.php");
require_once("./user/model/UserCredentials.php");

class Application {

	/** 
	 * @var \guest\controller\GuestCongroller 
	 */
	private $guestController;

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;
		$this->guestController = new \guest\controller\GuestController($this->mysqli);
	}

	/**
	 * @return HTML 
	 */ 
	public function startApplication() {

		if ($this->guestController->isUser()) {

			$user = $this->guestController->getUser();
			$userController = new \user\controller\UserController($this->mysqli, $user);
			return $userController->startUserApplication();
		} else {
			
			return $this->guestController->startGuestApplication();
		}
	}
}