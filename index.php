<?php

require_once("./common/view/PageView.php");
require_once("./application/controller/Application.php");

session_start();

$mysqli = new mysqli("127.0.0.1", "root", "", "login");


$pageView = new \common\view\PageView();
$application = new \application\controller\Application($mysqli);

$page = $application->startApplication();

echo $pageView->getHTMLPage($page);

$mysqli->close();
