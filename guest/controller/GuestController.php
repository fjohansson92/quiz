<?php

namespace guest\controller;

require_once("./login/view/LoginView.php");
require_once("./login/controller/LoginController.php");
require_once("./guest/view/Navigation.php");
require_once("./guest/view/GuestView.php");
require_once("./login/controller/RegisterController.php");
require_once("./login/view/RegisterView.php");

class GuestController {

	/** 
	 * @var \login\controller\LoginController
	 */
	private $loginController;

	/** 
	 * @var \guest\view\Navigation
	 */
	private $navigation;

	/** 
	 * @var \guest\view\GuestView
	 */
	private $guestView;

	/** 
	 * @var \login\view\LoginView
	 */
	private $loginView;

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @var \quiz\view\Navigation
	 */
	private $quizNavigation;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {

		$this->mysqli = $mysqli;

		$this->navigation = new \guest\view\Navigation();
		$this->guestView = new \guest\view\GuestView($this->navigation);
		$this->quizNavigation = new \quiz\view\Navigation();

		$this->loginView = new \login\view\LoginView();
		$this->loginController = new \login\controller\LoginController($this->mysqli, $this->loginView);
	}

	/** 
	 * @return boolean 
	 */
	public function isUser() {

		$this->loginController->doToggleLogin();
		return $this->loginController->isLoggedIn();
	}

	/**
	 * @return \login\model\ClientInfo
	 */
	public function getUser() {

		return $this->loginController->getUser();
	}

	/**
	 * @return \common\view\Page
	 */
	public function startGuestApplication() {

		if($this->navigation->userGoesToRegister()) {

			$registerView = new \login\view\RegisterView();
			$registerController = new \login\controller\RegisterController($this->mysqli, $registerView, $this->navigation);
			$registerController->doToggleRegister();
			
			return $this->guestView->getRegisterPage($registerView);
		} else if($this->navigation->userChecksResults() || $this->quizNavigation->checksQuizResult()) {

			$resultsController = new \quiz\controller\ResultController($this->mysqli, $this->quizNavigation);
			$content = $resultsController->doStudentResults();
			return $this->guestView->getResultPage($content);
		} else {

			return $this->guestView->getLoggedOutPage($this->loginView);
		}	
	}


}