<?php

namespace guest\view;

class GuestView {

	/** 
	 * @var \guest\view\Navigation
	 */
	private $navigation;

	/** 
	 * @param Navigation $navigation
	 */
	public function __construct(Navigation $navigation) {

		$this->navigation = $navigation;
	}

	/** 
	 * @param  login\view\LoginView $loginView 
	 * @return \common\view\Page
	 */
	public function getLoggedOutPage(\login\view\LoginView $loginView) {

		$active = $this->navigation->startPage();
		$html = $this->navigation->getMenu($active);
		$html .= $loginView->getForm();

		return new \common\view\Page("Utloggad", $html);
	}

	/** 
	 * @param  login\view\RegisterView $registerView 
	 * @return \common\view\Page
	 */
	public function getRegisterPage(\login\view\RegisterView $registerView) {

		$active = $this->navigation->registerPage();
		$html = $this->navigation->getMenu($active);
		$html .= $registerView->getForm();

		return new \common\view\Page("Registrering", $html);
	}

	/** 
	 * @param  string $content 
	 * @return \common\view\Page
	 */
	public function getResultPage($content) {

		$active = $this->navigation->resultsPage();
		$html = $this->navigation->getMenu($active);
		$html .= $content;

		return new \common\view\Page("Öppna resultat", $html);
	}
}