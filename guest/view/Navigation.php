<?php

namespace guest\view;

require_once("user/view/Navigation.php");

class Navigation extends \user\view\Navigation{

	/** 
	 * @var string
	 */
	private static $Start = "start";

	/** 
	 * @var string
	 */
	private static $Register = "register";

	/** 
	 * @var string
	 */
	private static $Results = "results";


	/** 
	 * @return boolean
	 */
	public function userGoesToRegister() {
		return isset($_GET[self::$Register]);
	}

	/** 
	 * @return boolean
	 */
	public function userChecksResults() {
		return isset($_GET[self::$Results]);
	}

	/** 
	 * @return string
	 */
	public function startPage() {
		return self::$Start;
	}

	/** 
	 * @return string
	 */
	public function resultsPage() {
		return self::$Results;
	}

	/** 
	 * @return string
	 */
	public function registerPage() {
		return self::$Register;
	}

	/** 
	 * @param  string $active
	 * @return string HTML menu
	 */
	public function getMenu($active) {

		$links = array();
		$links[] = self::$Start;
		$links[] = self::$Results;
		$links[] = self::$Register;

		$menu = "";
		$span = "";
		foreach ($links as $link) {
			$span .=  "<span class='icon-bar'></span>";
			$menu .= "<li";
			$menu .= $active == $link ? " class='active'>" : ">";

			switch ($link) {

				case self::$Start :

					$menu .= "<a href='?" . self::$Start . "'>Start</a>";
					break;
				case self::$Results :

					$menu .= "<a href='?" . self::$Results . "'>Resultat</a>";
					break;
				case self::$Register :

					$menu .= "<a href='?" . self::$Register . "'>Registrering</a>";
					break;	
			}
			$menu .= "</li>";
		}	

		return $this->getHeader($menu, $span);
	}	

}