<?php

namespace login\view;

require_once("./login/model/RegisterObserver.php");
require_once("./common/model/FNameEmptyException.php");
require_once("./common/model/FNameLengthException.php");
require_once("./common/model/LNameEmptyException.php");
require_once("./common/model/LNameLengthException.php");

class RegisterView implements \login\model\RegisterObserver {

	/** 
	 * @var string
	 */
	private static $Register = "register";	

	/** 
	 * @var string
	 */	
	private static $UserName = "RegisterView::UserName";

	/** 
	 * @var string
	 */	
	private static $Password = "RegisterView::Password";

	/** 
	 * @var string
	 */	
	private static $RepeatedPassword = "RegisterView::RepeatedPassword";

	/** 
	 * @var string
	 */	
	private static $FName = "RegisterView::fName";

	/** 
	 * @var string
	 */	
	private static $LName = "RegisterView::lName";

	/** 
	 * @var string
	 */	
	private $message = "";

	/** 
	 * @return string HMTL
	 */
	public function getForm() {

		$userName = $this->getUserName();
		$fName = $this->getFName();
		$lName = $this->getLName();

		$html = "
		<div class='row shadow' ><div class='col-md-8 col-md-offset-2' >
			<form action='?" . self::$Register . "' data-validate='parsley' method='post' enctype='multipart/form-data' role='form' class='form-horizontal'>
				<fieldset>
					<div class='error col-md-offset-2'>$this->message</div>
					<legend>Registrera ny användare - Skriv in användarnamn och lösenord</legend>";
		$html .= $this->getFormGroups($userName, $fName, $lName);
 		$html .= "<div class='col-md-6 col-md-offset-2' >
						<input type='submit' name=''  value='Registrera' id='submit' type='button' class='btn btn-primary pull-right'/>
					<div>
				</fieldset>
			</form>
			</div></div>";
		return $html;
	}

	/** 
	 * @param  string $userName 
	 * @param  string $fName    
	 * @param  string $lName    
	 * @return string           
	 */
	private function getFormGroups($userName, $fName, $lName) {

		return "<div class='form-group'>
			<label for='UserNameID' class='col-lg-4 control-label'>Användarnamn :</label>
			<div class='col-lg-4'>
				<input type='text' name='" . self::$UserName . "' id='UserNameID' value='$userName' class='form-control' data-validation-minlength='0' maxlength='16'
					data-required-message='Du måste fylla i ett användarnamn' data-required='true' data-rangelength='[3,16]' maxlength='16' 
					data-rangelength-message='Det här värdet måste vara mellan 3 och 16 tecken långt.' data-trigger='focusout' />
			</div>
		 </div>
		<div class='form-group'>	
			<label for='PasswordID' class='col-lg-4 control-label'>Lösenord  :</label>
			<div class='col-lg-4'>
				<input type='password' size='20' name='" . self::$Password . "' id='PasswordID' value='' class='form-control' maxlength='16'
					data-validation-minlength='0' data-required-message='Du måste fylla i ett lösenord' maxlength='16'
					data-required='true' data-rangelength='[6,16]' data-rangelength-message='Det här värdet måste vara mellan 6 och 16 tecken långt.' data-trigger='focusout'/>
			</div>
		 </div>
		<div class='form-group'>
			<label for='repeatedPasswordID' class='col-lg-4 control-label'>Upprepa Lösenord  :</label>
			<div class='col-lg-4'>
				<input type='password' size='20' name='" . self::$RepeatedPassword . "' id='repeatedPasswordID' value='' class='form-control' 
					data-required='true' data-validation-minlength='0' data-required-message='Du måste upprepa lösenordet' maxlength='16' data-trigger='focusout'/>
			</div>
		 </div>
		<div class='form-group'>
			<label for='fName' class='col-lg-4 control-label'>Förnamn : </label>
			<div class='col-lg-4'>
				<input type='text' size='20' name='" . self::$FName ."' id='FNamee' value='$fName' class='form-control'
					data-required='true' data-validation-minlength='0' data-required-message='Du måste ange ett förnamn' maxlength='30' data-trigger='focusout'/> 
			</div>
		</div>
		<div class='form-group'>
			<label for='lName' class='col-lg-4 control-label'>Efternamn : </label>
			<div class='col-lg-4'>
				<input type='text' size='30' name='" . self::$LName ."' id='lName' value='$lName' class='form-control'
					data-required='true' data-validation-minlength='0' data-required-message='Du måste ange ett efternamn' maxlength='30' data-trigger='focusout'/> 
			</div>
		 </div> ";
	}
	
	/** 
	 * @return boolean 
	 */
	public function regesteringUser(){
		return isset($_POST[self::$UserName]);
	}

	/** 
	 * @return \login\model\LoginCredentials
	 * @throws If new user is in invalid format.
	 */
	public function getLoginCredentials(){

		$userName;
		try {
			$userName = new \login\model\UserName($this->getUserName());
		} catch (\common\model\UserNameShortException $exception) {

			$minLength = $exception->getLength();
			$this->message = "<p>Användarnamnet måste vara minst $minLength tecken</p>";
			throw new \Exception();
		} catch (\common\model\UserNameLengthException $exception) {

			$maxLength = $exception->getLength();
			$this->message = "<p>Användarnamnet får max vara $maxLength tecken</p>";
			throw new \Exception();
		}

		$password = new \login\model\ClientPassword($this->getPassword());
		try {

			$loginCredentials = \login\model\LoginCredentials::loginCredentialsWithoutTemp($userName, $password);

			$repeatedPassword = $this->getRepeatedPassword();
			$passInput = $this->getPassword();
			if($passInput != $repeatedPassword) {

				$this->message = "<p>Lösenorden matchar inte</p>";
				throw new \Exception();
			}
			return $loginCredentials;
		} catch (\common\model\PasswordShortException $exception) {

	 		$minLength = $exception->getLength();
			$this->message = "<p>Lösenordet måste vara minst $minLength tecken</p>";
	 	} catch (\common\model\PasswordLengthException $exception) {

	 		$maxLength = $exception->getLength();
			$this->message = "<p>Lösenordet får max vara $maxLength tecken</p>";
	 	}
	 	throw new \Exception();
	}	

	/** 
	 * @return string 
	 */
	private function getUserName() {
		if (isset($_POST[self::$UserName])) {	
			return $this->sanitize($_POST[self::$UserName]);
		} else {
			return "";
		}	
	}

	/** 
	 * @return string 
	 */
	private function getPassword() {
		if (isset($_POST[self::$Password])) {
			return $this->sanitize($_POST[self::$Password]);
		} else {
			return "";
		}	
	}

	/** 
	 * @return string 
	 */
	private function getRepeatedPassword() {
		if (isset($_POST[self::$RepeatedPassword])) {
			return $this->sanitize($_POST[self::$RepeatedPassword]);
		} else {
			return "";
		}	
	}	

	/** 
	 * @return \user\model\UserCredentials 
	 * @throws If name or lastname is in invalid format.
	 */
	public function getUserCredentials() {

		try {
			$userName = $this->getUserName();
			$fName = $this->getFName();
			$lName = $this->getLName();

			return \user\model\UserCredentials::createStudent(NULL, $userName, $fName, $lName);
		} catch (\common\model\FNameEmptyException $exception) {
			$this->message = "Du måste ange ett förnamn";
		} catch (\common\model\FNameLengthException $exception) {
			$maxLength = $exception->getLength();
			$this->message = "<p>Förnamn får max vara $maxLength tecken</p>";
		} catch (\common\model\LNameEmptyException $exception) {
			$this->message = "Du måste ange ett efternamn";
		} catch (\common\model\LNameLengthException $exception) {
			$maxLength = $exception->getLength();
			$this->message = "<p>Efternamnet får max vara $maxLength tecken</p>";
		}
		throw new \Exception();
	}

	/** 
	 * @return string 
	 */
	private function getFName() {
		if (isset($_POST[self::$FName])){	
			return $this->sanitize($_POST[self::$FName]);
		}else
			return "";
	}

	/** 
	 * @return string 
	 */
	private function getLName() {
		if (isset($_POST[self::$LName])){	
			return $this->sanitize($_POST[self::$LName]);
		}else
			return "";
	}	

	public function userExists() {
		$this->message = "<p>Användarnamnet är upptaget</p>";
	}

	/** 
	 * @return string 
	 */	
	private function sanitize($input) {
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}
}