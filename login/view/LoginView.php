<?php

namespace login\view;

require_once("./common/model/LengthException.php");
require_once("./common/model/UserNameShortException.php");
require_once("./common/model/UserNameLengthException.php");
require_once("./common/model/PasswordShortException.php");
require_once("./common/model/PasswordLengthException.php");

class LoginView {

	/**
	 * Location for $_POST where the username exists.
	 * @var string  
	 */
	private static $UserName = "LoginView::UserName";

	/**
	 * Location for $_POST where the password exists.
	 * @var string  
	 */
	private static $Password = "LoginView::Password";

	/**
	 * Location for $_POST where the value for if the user 
	 * should be saved exists.
	 * @var string
	 */  
	private static $SaveUser = "LoginView::SaveUser";

		/**
	 * Location for cookie where the username exists.
	 * @var string
	 */
	private static $UserCookieLocation = "LoginView::UserName";

	/**
	 * Location for cookie where the password exists.
	 * @var string
	 */	
	private static $PassCookieLocation = "LoginView::Password";

	/**
	 * @var String
	 */
	private static $Login = "login";

	/** 
	 * @var string
	 */
	private static $Logout = "logout";

	/** 
	 * @return boolean
	 */
	public function loggingIn() {

		return isset($_GET[self::$Login]) || $this->hasLoginCookies();
	}

	/** 
	 * @return boolean
	 */
	public function loggingOut() {

		return isset($_GET[self::$Logout]);
	}

	/** 
	 * @var string
	 */
	private $message = "";

	/** 
	 * @return string HTML
	 */
	public function getForm() {

		$userName = $this->getUserName();
		$checked = $this->saveUser() ? "checked='checked'" : "";

		$html = "<div class='row'><div class='col-md-8 col-md-offset-2 shadow' >
				<form class='form-horizontal' role='form' method='post' action='?" . self::$Login . 
					"' enctype='multipart/form-data'  id='loginForm' data-validate='parsley'>
					<fieldset>
						<legend>
							Login - Skriv in användarnamn och lösenord
						</legend>
						<div class='col-lg-offset-2'>
							<div class='error'>$this->message</div>
						</div>";
		$html .= $this->getFormGroups($userName, $checked);
		$html .= "</fieldset>
				</form>
				</div></div>";		
		return $html;	
	}	

	/** 
	 * @param  string $userName 
	 * @param  string $checked  
	 * @return string           
	 */
	private function getFormGroups($userName, $checked) {

		return "<div class='form-group'>
					<label for='inputUserName' class='col-lg-4 control-label'>Användarnamn:</label>
					<div class='col-lg-4'>
						<input type='text' name='". self::$UserName . 
							"' id='inputUserName'  size='16' value='$userName' maxlength='16' type='text' class='form-control' placeholder='Användarnamn'
							data-validation-minlength='0' data-required-message='Du måste fylla i ett användarnamn' maxlength='16' data-trigger='focusout' data-required='true'/>
					</div>
				</div>
				<div class='form-group'>
					<label for='inputPassword' class='col-lg-4 control-label' >Lösenord:</label>
					<div class='col-lg-4'>
						<input type='password' name='". self::$Password .
							"' size='16' maxlength='16'  type='password' class='form-control' placeholder='Lösenord' id='inputPassword'
							data-validation-minlength='0' data-required-message='Du måste fylla i ett lösenord' maxlength='16' data-trigger='focusout' data-required='true'/>
					</div>
				</div>
				<div class='form-group'>
				   	<label class='col-lg-4 control-label' >Håll mig inloggad</label>
				    <div class='col-lg-4'>
				     	<div class='checkbox'>
								<input type='checkbox' name='". self::$SaveUser .
									"' id='SaveUserID' value='1' $checked/>
				      	</div>
				    </div>
				</div>
				<div class='form-group'>
				    <div class='col-lg-offset-6 col-lg-2'>
				      <button type='submit' value='Logga in' type='button' class='btn btn-primary pull-right'>Logga in</button>
				    </div>
				  </div>";
	}


	/** 
	 * @return \login\model\LoginCredentials
	 * @throws If invalid LoginCredentials 
	 */
	public function getLoginCredentials() {

		if($this->hasLoginCookies()) {
			return \login\model\LoginCredentials::loginCredentialsWithoutPassword(new \login\model\UserName($this->getUserName()),
																				  new \login\model\TemporaryPassword($this->getPassword()));
		}

		$userName;
		$password;
		try {
			$userName = $this->getUserName();
			$password = $this->getPassword();

			return \login\model\LoginCredentials::loginCredentialsWithoutTemp(new \login\model\UserName($userName), 
																			  new \login\model\ClientPassword($password));
		} catch (\Exception $e) {
			
			if (empty($userName)) {
				$this->message .= "<p>Användarnamn saknas</p>";
			} else if (empty($password)) {
				$this->message .= "<p>Lösenord saknas</p>";
			}
		}
		throw new \Exception();
	}

	/** 
	 * @return string 
	 */
	private function getUserName() {

		if (isset($_COOKIE[self::$UserCookieLocation])) {
			return $this->sanitize($_COOKIE[self::$UserCookieLocation]);
		}
		if (isset($_POST[self::$UserName])){	
			return $this->sanitize($_POST[self::$UserName]);
		}else {
			return "";
		}	
	}

	/** 
	 * @return string 
	 */
	private function getPassword() {

		if (isset($_COOKIE[self::$PassCookieLocation])) {
			return $this->sanitize($_COOKIE[self::$PassCookieLocation]);
		}
		if (isset($_POST[self::$Password]))
			return $this->sanitize($_POST[self::$Password]);
		else
			return "";
	}

	/** 
	 * @return boolean 
	 */
	public function saveUser() {

		return isset($_POST[self::$SaveUser]);
	}


	/** 
	 * @return string 
	 */	
	private function sanitize($input) {
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}

	/** 
	 * @param  \login\model\LoginCredentials $loginCredentials 
	 */
	public function loginSuccess(\login\model\LoginCredentials $loginCredentials) {

		if($this->saveUser() || $this->hasLoginCookies()) {
			setcookie(self::$UserCookieLocation, $loginCredentials->getUserName(), $loginCredentials->getEndTime());
			setcookie(self::$PassCookieLocation, $loginCredentials->getTemporaryPassword(), $loginCredentials->getEndTime());
		}
	}


	public function loginFail() {
	
	
		if ($this->hasLoginCookies()) {
			$this->message = "<p>Felaktig information i cookie</p>";
		} else if (empty($this->message)){
			$this->message = "<p>Felaktigt användarnamn och/eller lösenord</p>";
		}
		$this->removeCookies();
	}

	/** 
	 * @return boolean 
	 */
	public function hasLoginCookies() {

		return isset($_COOKIE[self::$UserCookieLocation]) && isset($_COOKIE[self::$PassCookieLocation]);
	}

	public function removeCookies() {

		unset($_COOKIE[self::$UserCookieLocation]);
		unset($_COOKIE[self::$PassCookieLocation]);

		$time = time() - 1;
		setcookie(self::$UserCookieLocation, "", $time);
		setcookie(self::$PassCookieLocation, "", $time);
	}
}