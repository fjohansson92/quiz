<?php

namespace login\model;

class RegisterModel {

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @var login\model\RegisterModel
	 */
	private $registerObserver;

	/** 
	 * @param \mysqli $mysqli 
	 * @param \login\model\RegisterObserver $registerObserver 
	 */
	public function __construct(\mysqli $mysqli, \login\model\RegisterObserver $registerObserver) {

		$this->mysqli = $mysqli;
		$this->registerObserver = $registerObserver;
	}

	/** 
	 * @param  \login\model\LoginCredentials $loginCredentials 
	 * @param  \user\model\UserCredentials   $userCredentials  
	 * @throws If username is being used
	 */
	public function doRegister(\login\model\LoginCredentials $loginCredentials, \user\model\UserCredentials $userCredentials) {

		$clientInfo = new \login\model\ClientInfo($loginCredentials);
		$users = new \login\model\Users($this->mysqli);

		try{
			$users->saveNewUser($clientInfo, $userCredentials);
		} catch(\Exception $exception) {
			$this->registerObserver->userExists();
			throw new \Exception();
		}	
	}
}