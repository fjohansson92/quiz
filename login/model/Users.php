<?php

namespace login\model;

class Users {

	/** 
	 * @var \common\model\Database
	 */
	private $database;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {
		$this->database = new \common\model\Database($mysqli);
	}

	/** 
	 * @param  \login\model\LoginCredentials $loginCredentials 
	 * @return \login\model\ClientInfo
	 * @throws If username couldn't be found in database
	 */
	public function getUser(\login\model\LoginCredentials $loginCredentials) {

		try{
			return $this->getUserFromCookie($loginCredentials->getUserName(), $loginCredentials->getTemporaryPassword());
		} catch(\Exception $exception) {}
		try{
			$userName = $loginCredentials->getUserName();
			return $this->getUserFromUserName($userName);
		} catch(\Exception $exception) {}
		throw new \Exception();
	}

	/** 
	 * @param  \login\model\ClientInfo $clientInfo 
	 */
	public function saveClientInfo(\login\model\ClientInfo $clientInfo) {

		$sql = "INSERT INTO users (userName, password, tempPassword, userAgent, time, ip) VALUES (?, ?, ?, ?, ?, ?)";
	    
	    $loginCredentials = $clientInfo->getLoginCredentials();
	    $values = array($loginCredentials->getUserName(),
	                    $loginCredentials->getEncryptedPassword(),
	                    $loginCredentials->getTemporaryPassword(),
	                    $clientInfo->userAgent,
	                    $loginCredentials->getEndTime(),
	                    $clientInfo->ip);

		$this->database->insert($sql, $values);
	}

	/** 
	 * @param \login\model\ClientInfo $clientInfo     
	 * @param \user\model\UserCredentials $userCredentials 
	 * @throws Username is being used
	 */
	public function saveNewUser(\login\model\ClientInfo $clientInfo, \user\model\UserCredentials $userCredentials) {

		$loginCredentials = $clientInfo->getLoginCredentials();

		$existingUser;
		try{
			
			$userName = $loginCredentials->getUserName();
			$existingUser = $this->getUserFromUserName($userName); 
		} catch(\Exception $exception) {}
		if(isset($existingUser)) {
			throw new \Exception();
		}

		$firstSql = "INSERT INTO users (userName, password, tempPassword, userAgent, time, ip) VALUES (?, ?, ?, ?, ?, ?);";
		$firstValues = array($loginCredentials->getUserName(),
                                          $loginCredentials->getEncryptedPassword(),
                                          $loginCredentials->getTemporaryPassword(),
                                          $clientInfo->userAgent,
                                          $loginCredentials->getEndTime(),
                                          $clientInfo->ip);

		$secondSql = "INSERT INTO userdata (userName, fName, lName, userType) VALUES (?, ?, ?, ?);";
		$secondValues = array($userCredentials->getUserName(),
                                        $userCredentials->getFName(),
                                        $userCredentials->getLName(),
                                        $userCredentials->getUserType());


		$this->database->insertTwoTables($firstSql, $secondSql, $firstValues, $secondValues);
	}

	/** 
	 * @param string $userName 
	 * @return \login\model\ClientInfo
	 * @throws If use couldn't be found
	 */
	private function getUserFromUserName($userName) {

		$sql = "SELECT userName, password, tempPassword, userAgent, time, ip FROM users WHERE userName = ?";
		$values = array($userName);

		return $this->sortClientFromDb($sql, $values);
	}

	/** 
	 * @param  string $userName 
	 * @param  string $password 
	 * @return \login\model\ClientInfo
	 * @throws If use couldn't be found
	 */
	private function getUserFromCookie($userName, $password) {

		$sql = "SELECT userName, password, tempPassword, userAgent, time, ip FROM users WHERE userName = ? AND tempPassword = ?";
		$values = array($userName, $password);

		return $this->sortClientFromDb($sql, $values);
	}

	/** 
	 * @param  string $sql    
	 * @param  array $values 
	 * @return  \login\model\ClientInfo       
	 */
	private function sortClientFromDb($sql, $values) {

		$result = $this->database->select($sql, $values);

		if ($row = $result->fetch_assoc()) {   

			$savedloginCredentials = new \login\model\LoginCredentials(new \login\model\UserName($row["userName"]), 
									 new \login\model\ClientPassword($row["password"]), 
									 new \login\model\TemporaryPassword($row["tempPassword"]), 
									 $row["time"]);

			$user = new \login\model\ClientInfo($savedloginCredentials);
			$user->userAgent = $row["userAgent"];
			$user->ip = $row["ip"];

			return $user;
		}				
		throw new \Exception();
	}
}