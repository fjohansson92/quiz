<?php

namespace login\model;

interface RegisterObserver {
	public function userExists();
}