<?php

namespace login\model;

class LoginCredentials {

	/** 
	 * @var \login\model\UserName
	 */
	private $userName;

	/** 
	 * @var \login\model\ClientPassword
	 */
	private $password;

	/** 
	 * @var integer
	 */
	private $endTime;

	/** 
	 * @var \login\model\TemporaryPassword
	 */
	private $temporaryPassword;

	/** 
	 * @var boolean
	 */
	private $cookieLogin = false;

	/** 
	 * @param \login\model\UserName $userName     
	 * @param \login\model\ClientPassword $password    
	 * @param \login\model\TemporaryPassword $temporaryPassword
	 * @param integer $time    
	 */
	public function __construct(\login\model\UserName $userName, 
								\login\model\ClientPassword $password, 
								\login\model\TemporaryPassword $temporaryPassword, 
								$time) {

		$this->userName = $userName;
		$this->password = $password;
		$this->temporaryPassword = $temporaryPassword;
		$this->endTime = $time;
	}

	/** 
	 * @param  \login\model\UserName $userName
	 * @param  \login\model\ClientPassword $password
	 * @return \login\model\LoginCredentials
	 */
	public static function loginCredentialsWithoutTemp(\login\model\UserName $userName, 
													   \login\model\ClientPassword $password) {

		$password->validatePassword();

		$password->encryptPassword();
		$tempString = $password->generateNewPassword();
		$newTemp = new \login\model\TemporaryPassword($tempString);
		return new \login\model\LoginCredentials($userName, $password, $newTemp, time());
	}

	/** 
	 * @param  \login\model\UserName $userName 
	 * @param  \login\model\TemporaryPassword $temporary 
	 * @return \login\model\LoginCredentials
	 */
	public static function loginCredentialsWithoutPassword(\login\model\UserName $userName, 
														   \login\model\TemporaryPassword $temporary) {

		$password = new \login\model\ClientPassword($temporary->getPassword());
		return new \login\model\LoginCredentials($userName, $password, $temporary, time());
	}

	/** 
	 * Test if same user
	 * @param  LoginCredentials $loginCredentials 
	 * @return boolean
	 */
	public function isSameUser(LoginCredentials $loginCredentials) {

		$sameUserName = $this->userName == $loginCredentials->userName;
		$samePassword = $this->password == $loginCredentials->password;

		if ($sameUserName && $samePassword) {
			$this->endTime += 1000;
			return true;
		}

		$samePassword = $this->temporaryPassword == $loginCredentials->temporaryPassword;

		if ($sameUserName && $samePassword && $this->endTime < $loginCredentials->getEndTime()) {
			$this->endTime += 1000;
			$this->cookieLogin = true;
			$this->password = $loginCredentials->password;
			$this->temporaryPassword = new \login\model\TemporaryPassword($this->password->generateNewPassword());
			return true;
		} 
		return false;		
	}

	/** 
	 * @return string
	 */
	public function getUserName() {
		return $this->userName->getUserName();
	}

	/** 
	 * @return string
	 */	
	public function getTemporaryPassword() {

		return $this->temporaryPassword->getPassword();
	}

	/** 
	 * @return string
	 */
	public function getEncryptedPassword() {

		return $this->password->getPassword();
	}

	/** 
	 * @return integer
	 */
	public function getEndTime() {

		return $this->endTime;
	} 

	/** 
	 * @return boolean
	 */
	public function cookieLogin() {

		return $this->cookieLogin; 
	}


}