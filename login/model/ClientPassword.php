<?php

namespace login\model;

class ClientPassword{

	/** 
	 * @var integer
	 */
	private static $passwordMinLength = 6;

	/** 
	 * @var integer
	 */
	private static $passwordMaxLength = 16;

	/** 
	 * @var string
	 */
	private $password;

	/** 
	 * @param string $password 
	 */
	public function __construct($password) {

		$this->password = $password;
	}

	/** 
	 * @return string
	 */
	public function generateNewPassword() {

		return md5(mcrypt_create_iv(32));
	}

	/** 
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	public function encryptPassword() {

		$salt = "mysaltystringthatiuseformypassword";
		$this->password = md5($salt . $this->password);
	}

	/** 
	 * @return boolean
	 * @throws If password invalid format.
	 */
	public function validatePassword() {

		if(strlen($this->password) < self::$passwordMinLength) {

			throw new \common\model\PasswordShortException(self::$passwordMinLength);
		} else if (strlen($this->password) > self::$passwordMaxLength) {

			throw new \common\model\PasswordLengthException(self::$passwordMaxLength);
		}
	}

}