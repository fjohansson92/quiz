<?php

namespace login\model;

require_once("./login/model/Users.php");
require_once("./login/model/ClientInfo.php");
require_once("./login/model/UserName.php");
require_once("./login/model/ClientPassword.php");
require_once("./login/model/TemporaryPassword.php");

class LoginModel {

	/** 
	 * @var string
	 */
	private static $userSessionLocation = "LoginModel::savedUser";

	/** 
	 * @var \mysqli
	 */
	private $mysqli;

	/** 
	 * @param \mysqli $mysqli 
	 */
	public function __construct(\mysqli $mysqli) {
		$this->mysqli = $mysqli;
	}

	/** 
	 * @return boolean 
	 */
	public function isLoggedIn() {

		if (isset($_SESSION[self::$userSessionLocation])) {

			return $_SESSION[self::$userSessionLocation]->isSameUser();
		}
		return false;
	}

	/** 
	 * @param \login\model\LoginCredentials $loginCredentials 
	 * @param $saveUser         
	 * @return \login\model\LoginCredentials        
	 * @throws If not valid user.  
	 */
	public function doLogin(\login\model\LoginCredentials $loginCredentials, $saveUser) {

		$savedUsers = new \login\model\Users($this->mysqli);
		$savedClientInfo = $savedUsers->getUser($loginCredentials);
		$savedLoginCredentials = $savedClientInfo->getLoginCredentials();

		if(!$loginCredentials->isSameUser($savedLoginCredentials)) {
			throw new \Exception();
		}	

		$clientInfo = new \login\model\ClientInfo($loginCredentials);

		if($loginCredentials->cookieLogin()) {
			if(!$clientInfo->isSameSavedUser($savedClientInfo)) {
				throw new \Exception();
			}
		}

		if ($saveUser || $loginCredentials->cookieLogin()) {
			$savedUsers->saveClientInfo($clientInfo);
		}
		$this->saveUser($clientInfo);
		return $clientInfo->getLoginCredentials();			
	}

	/** 
	 * @param  ClientInfo $clientInfo
	 */
	private function saveUser(ClientInfo $clientInfo) {

		$_SESSION[self::$userSessionLocation] = $clientInfo;
	}

	public function doLogout() {

		session_unset(self::$userSessionLocation);
	}

	/** 
	 * @return ClientInfo
	 * @throws If session user wasn't found
	 */
	public function getSessionUser() {

		if (isset($_SESSION[self::$userSessionLocation])) {

			return $_SESSION[self::$userSessionLocation];
		}
		throw new \Exception();
	}

}
