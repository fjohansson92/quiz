<?php

namespace login\model;

class ClientInfo {

	/** 
	 * @var \login\model\LoginCredentials
	 */
	private $loginCredentials;

	/** 
	 * @var string
	 */
	public $userAgent;

	/** 
	 * @var string
	 */
	public $ip;

	/** 
	 * @param LoginCredentials $loginCredentials 
	 */
	public function __construct(LoginCredentials $loginCredentials) {

		$this->loginCredentials = $loginCredentials;
		$this->userAgent = $this->getUserAgent();
		$this->ip = $this->getIp();
	}

	/** 
	 * @return boolean 
	 */
	public function isSameUser() {

		return $this->userAgent == $this->getUserAgent() &&
			   $this->ip == $this->getIp();
	}

	/** 
	 * @param  ClientInfo $savedClientInfo 
	 * @return boolean                     
	 */
	public function isSameSavedUser(ClientInfo $savedClientInfo) {

		return $this->userAgent == $savedClientInfo->userAgent &&
			   $this->ip == $savedClientInfo->ip;
	}

	/** 
	 * @return \login\model\LoginCredentials
	 */
	public function getLoginCredentials() {

		return $this->loginCredentials;
	}

	/** 
	 * @return string
	 */
	private function getUserAgent() {

		return $_SERVER['HTTP_USER_AGENT'];
	}

	/** 
	 * @return string
	 */
	private function getIp() {

		return $_SERVER["REMOTE_ADDR"];
	}

}