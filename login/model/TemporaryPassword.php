<?php

namespace login\model;

class TemporaryPassword{

	/** 
	 * @var string
	 */
	private $temporaryPassword;

	/** 
	 * @param string $cookiePassword 
	 */
	public function __construct($cookiePassword) {

		$this->temporaryPassword = $cookiePassword;
	}

	/** 
	 * @return string
	 */
	public function getPassword() {
		return $this->temporaryPassword;
	}

}