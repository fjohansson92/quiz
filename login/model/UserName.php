<?php

namespace login\model;

class UserName {

	/** 
	 * @var integer
	 */
	private static $userNameMinLength = 3;

	/** 
	 * @var integer
	 */
	private static $userNameMaxLenght = 16;

	/** 
	 * @var string
	 */
	private $userName;

	/** 
	 * @param string $userName
	 * @throws If userName not correct format
	 */
	public function __construct($userName) {

		if(strlen($userName) < self::$userNameMinLength) {

			throw new \common\model\UserNameShortException(self::$userNameMinLength);
		} else if (strlen($userName) > self::$userNameMaxLenght) {

			throw new \common\model\UserNameLengthException(self::$userNameMaxLenght);
		}

		$this->userName = $userName;
	} 

	/** 
	 * @return string
	 */
	public function getUserName() {

		return $this->userName;
	}

}