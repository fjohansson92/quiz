<?php

namespace login\controller;

require_once("./login/model/RegisterModel.php");

class RegisterController {

	/** 
	 * @var \login\view\RegisterView
	 */
	private $registerView;

	/** 
	 * @var \guest\view\Navigation
	 */
	private $navigationView;

	/** 
	 * @param \mysqli $mysqli      
	 * @param \login\view\RegisterView $registerView 
	 * @param \guest\view\Navigation   $navigationView 
	 */
	public function __construct(\mysqli $mysqli, 
								\login\view\RegisterView $registerView, 
								\guest\view\Navigation $navigationView) {

		$this->registerView = $registerView;
		$this->navigationView = $navigationView;
		$this->registerModel = new \login\model\RegisterModel($mysqli, $this->registerView);
	}

	public function doToggleRegister() {

		if($this->registerView->regesteringUser()){
			try{

				$loginCredentials = $this->registerView->getLoginCredentials();
				$userCredentials = $this->registerView->getUserCredentials();
				$this->registerModel->doRegister($loginCredentials, $userCredentials);
				$this->navigationView->reloadToStartPage();
			}catch(\Exception $e){}
		}
	}
}