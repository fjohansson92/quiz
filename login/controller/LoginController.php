<?php

namespace login\controller;

require_once("./login/model/LoginModel.php");
require_once("./login/model/LoginCredentials.php");

class LoginController {

	/** 
	 * @var \login\model\LoginModel
	 */
	private $loginModel;

	/** 
	 * @var \login\view\LoginView
	 */
	private $loginView;

	/** 
	 * @param \mysqli $mysqli
	 * @param \login\view\LoginView $loginView
	 */
	public function __construct(\mysqli $mysqli, \login\view\LoginView $loginView) {

		$this->loginView = $loginView;
		$this->loginModel = new \login\model\LoginModel($mysqli);
	}

	public function doToggleLogin() {

		if ($this->loginModel->isLoggedIn()) {
			if ($this->loginView->loggingOut()) {
				
				$this->loginView->removeCookies();
				$this->loginModel->doLogout();
			}
		} else {
			if ($this->loginView->loggingIn()){
				try{	

					$loginCredentials = $this->loginView->getLoginCredentials();
					$saveUser = $this->loginView->saveUser();
					$newloginCredentials = $this->loginModel->doLogin($loginCredentials, $saveUser);
					$this->loginView->loginSuccess($newloginCredentials);
				} catch (\Exception $e){
					$this->loginView->loginFail();
				}	
			} 
		}
	}

	/** 
	 * @return boolean 
	 */
	public function isLoggedIn() {

		return $this->loginModel->isLoggedIn();
	}

	/** 
	 * @return \login\model\ClientInfo
	 */
	public function getUser() {

		return $this->loginModel->getSessionUser();
	}
}